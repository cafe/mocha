function cadelmon(channel)
% cadelmon('channel')
% Deletes monitor callback on EPICS channel

% translate pv name to handle
if ischar(channel)
    pv = mcaisopen(channel);
    if pv == 0
        return;
    end
elseif isnumeric(channel)
    pv = channel;
else
    error ('first argument must be channel name or handle');
end

% delete monitor
mcaclearmon(pv);

% if no monitor left then stop monitoring
if isempty(mcamon) && mcamontimer
    mcamontimer('stop')
end
