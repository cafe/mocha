function value = cainfo(pvname)
% cainfo('pvname') returns handle,element count, native type, state, ...
%
 

mcaNoConnection    ={'unknown',  'disconnected','Disconnected due to server or network problem'};
mcaConnection      ={'known',    'connected','Normal connection'};
mcaClosedConnection={'unknown',  'disconnected','Permanently disconnected (cleared)'};
 
 
value=struct('Handle',{}, 'PVName',{}, 'ElementCount',{},'NativeType',{},'State',{},'MCAMessage',{},'Host',{},'Units',{});
chInfo=mocha ('getChannelInfo', pvname);
pvCtrl=mocha ('getCtrlCache', pvname);
value(1).Handle= mocha('getHandleFromPV', pvname);
value(1).PVName= mocha('getPVFromHandle', value(1).Handle);
value(1).ElementCount=chInfo.nelem;
nativeType=strsplit(chInfo.dataType,'_');
value(1).NativeType=nativeType{2};

if (chInfo.connectFlag==1)
  			value(1).State=mcaConnection{2}; 
				value(1).MCAMessage=mcaConnection{3};
else 
  			value(1).State=mcaNoConnection{2};
				value(1).MCAMessage=mcaNoConnection{3};
end
	

%value(1).MCAMessage=mocha('statusInfo', mocha('getStatus', pvname));
value(1).Host =chInfo.hostName;
value(1).Units=pvCtrl.units;

%value(1)=setfield(value(1),'Units',pvCtrl.units)

