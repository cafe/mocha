function camon(channel,callback,userarg)
% camon('channel', 'callback') 
% camon('channel', 'callback', userarg) 
% camon('channel', @callback) 
% camon('channel', @callback, userarg) 
%
% Installs a callback function (given as string or handle)
% on an EPICS channel.
% Whenever the channel updates, the callback is called
% with a structure as defined below and an optional
% numeric, string, or matrix user argument
%
% callback argument structure elements:
%   val:      value (scalar, vector or string)
%   sevr:     severity as a number
%   sevr_str: severity as a string
%   stat:     status as a number
%   stat_str: status as a string
%   time:     timestamp as a date vector
%
% A monitor can be deleted with
% cadelmon ('channel')


% initialize monitoring the first time
if ~mcamontimer
    mcamontimer('start');
end

% translate channel name to handle
if ischar(channel)
    pv = mcacheckopen(channel);
    if pv == 0
        error('EPICS channel %s not found', channel);
    end
elseif isnumeric(channel)
    pv = channel;
else
    error ('usage: camon(''channel'',@callback,userarg)');
end

% callback can be string or function handle -- we need string
if nargin>1
    if isa(callback,'function_handle')
        callback = func2str(callback);
    elseif ~ischar(callback)
        error ('usage: camon(''channel'',@callback,userarg)');
    end
end

% install monitor
if nargin==1
    mcamon(pv);
    return
elseif nargin==2
    cb=sprintf('%s(caget(%d,1))',callback,pv);
else
    cb=sprintf('%s(caget(%d,1),''%s'')',callback,pv,mat2str(userarg));
end
disp(pv)
disp(cb)
mcamon(pv, cb);
