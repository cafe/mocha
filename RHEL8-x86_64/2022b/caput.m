function sts = caput(pvname, value)
%   sts=caput('pvname',value) sets the value of EPICS PV 'pvname'.

%
channel=pvname;

if ischar(channel)    
    pv = mocha('open',channel);

                if mocha('isConnected', channel) == false
                          mocha('openNowAndWait', 1.5)
                end

    if mocha('isConnected', channel) == false
        error('EPICS channel %s not found', channel);
    end
elseif isnumeric(channel)
    
		if mocha('isConnected', channel) == false
        error('EPICS channel with handle %d not connected', channel);
    end
		pv = channel;
		
else
    error ('First argument must be channel name or handle');
end


sts= mocha('set', pv, value);

