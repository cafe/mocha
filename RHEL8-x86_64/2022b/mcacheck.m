function varargout = mcacheck(varargin)
%MCACHECK     - Same as MCASTATE
%   See also MCAOPEN

if nargin > 0  
    %varargout{1} = mca(13,varargin{:});
		for k = 1:nargin	
		  if mocha('isConnected', varargin{k}) == true	
				isConnected(k) = uint16(1);
			else
				isConnected(k) = uint16(0);	
			end	
		end	
		varargout{1} = isConnected;
else
		[h,pv,state] = mocha('getHandleStates'); 
    %[varargout{1}, varargout{2}] = mca(12);
		varargout{1} = h;
		varargout{2} = state;
end
