function mcaexit()
%MCAEXIT      - Shut down channel access...
%
% Disconnects all PVs.
mocha('close') %mca(999)
