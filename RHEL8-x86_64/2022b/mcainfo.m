function varargout = mcainfo(varargin);
%MCAINFO      - get connection status and other information about a PV 
% INFO = MCAINFO(PV) returns information on a single PV
%   PV can be a string PV name or an integer handle
%   Returns a 1-by-1 structure with fields:
%
%       Handle
%       PVName
%       ElementCount:
%       NativeType { STRING | INT |  FLOAT | ENUM | CHAR | DOUBLE }
%       State { connected | disconnected }
%       MCAMessage
%       Host
%
% [HANDLES, INFO] = MCAINFO with no argument returns information on
%   all open channels in a structure array.
%       HANDLES is a numeric array of handles
%       INFO is a structure array of inoformation on the PV in the
%            same format as above
%
% Note: A channel may become disconnected
% due to a server or network problem. This will be reflected in 
% MCAMessage field. Any attempts to read, write or monitor this channel
% will return an error. CA library will periodically attempt to reestablish
% connection without any action required from the user.
% When the connection is closed by the user with MCACLOSE,
% that PV handle becomes invalid, and attempts to call MCAINFO
% will result in an error. 
%   
% See also: MCAOPEN MCACLOSE MCASTATE
value=struct('Handle',{}, 'PVName',{}, 'ElementCount',{},'NativeType',{},'State',{},'MCAMessage',{},'Host',{},'Units',{});


mcaNoConnection    ={'unknown',  'disconnected','Disconnected due to server or network problem'};
mcaConnection      ={'known',    'connected','Normal connection'};
mcaClosedConnection={'unknown',  'disconnected','Permanently disconnected (cleared)'};

if nargin>0
    if ischar(varargin{1})
        %[handles, names] = mca(3);
        %matchfound = find(strcmp(varargin{1},names));
        %if isempty(matchfound)
        %    error(['No open channels found for a PV: ',varargin{1}]);
        %end
        
        %h = handles(matchfound);
				h(1)=mocha('getHandleFromPV',varargin{1});
				if (h(1)==0)
					error(['No open channels found for a PV: ',varargin{1}]);
				end
    elseif isnumeric(varargin{1})
        h(1)=(varargin{1});
    else 
        error('Argument must be a string PV Name or an integer handle');
    end
    %varargout{1} = mca(11,h);
else % Return info on all channels
    %[varargout{1} varargout{2}] = mca(10);
		[h,pv]=mocha('getHandles');
end		
		
for k=1:length(h)
			chInfo=mocha ('getChannelInfo', h(k));
			pvCtrl=mocha ('getCtrlCache', h(k));
			value(k).Handle=h(k);
			value(k).PVName= mocha('getPVFromHandle', h(k));
			value(k).ElementCount=chInfo.nelem;
			nativeType=strsplit(chInfo.dataType,'_');
			value(k).NativeType=nativeType{2};
			if (chInfo.connectFlag==1)
  			value(k).State=mcaConnection{2}; 
				value(k).MCAMessage=mcaConnection{3};
			else 
  			value(k).State=mcaNoConnection{2};
				value(k).MCAMessage=mcaNoConnection{3};
			end
			%value(k).MCAMessage=mocha('statusInfo', mocha('getStatus', h(k)));
			value(k).Host =chInfo.hostName;
			value(k).Units=pvCtrl.units;
end
if (length(h)==1)
	varargout{1} = value;
else
	varargout{1} = h;
	varargout{2} = value;
end
