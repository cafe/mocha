function varargout = mcaopen(varargin);
%MCAOPEN      - open a Channel Access connection to an EPICS Process Variable
%
%   H = MCAOPEN(PVNAME);
%       If successful H is a unique nonzero integer handle associated with this PV.
%       Returned handle is 0 if a connection could not be established
%       
%   [H1, ... ,Hn] = MCAOPEN(PVNAME1, ... ,PVNAMEn);
%       Is equivalent to but more efficient than multiple single-argument calls
%           H1 = MCAOPEN(PVNAME1);
%           ...
%           Hn = MCAOPEN(PVNAMEn);
%        
%   HANDLES  = MCAOPEN(NAMES) is convenient when working with long lists of PV names
%       HANDLES is a numeric array of assigned handles
%       NAMES is a cell array of strings with PV names
%   
%   [HANDLES, NAMES] = MCAOPEN with no arguments returns a list of PV Names for all open connections. 
%       HANDLES is a numeric array of handles
%       NAMES is a cell array of strings with PV names
%
%   Note:
%   When done, one should probably use MCACLOSE on the handle.
%   When you use the same channel again "later", you might keep it open.
%   See MCACHECKOPEN for a lazy person's bookkeeping helper.
%
%   See also: MCACHECKOPEN, MCAISOPEN, MCACLOSE

if nargin>1  &&  nargin ~= nargout
    error('Number of outputs must match the number of inputs')
end


if nargin==0
    [varargout{1} varargout{2}] = mocha('getHandles'); %mca(3); 
		
elseif iscellstr(varargin) && nargin ==1		
		varargout{1} = mocha('open', varargin{1} );
		
elseif iscellstr(varargin{1}) && nargin==1
    if length(varargin{1}) > 1
		 	mocha ('openPrepare');
			varargout{1} = mocha('open', varargin{1} );
			mocha ('openNow')
		else			
    	varargout{1} = mocha('open', varargin{1} ); % mca(2,varargin{1});
		end		
		
elseif iscellstr(varargin) 
	  mocha('openPrepare'); 
    %[varargout{1:nargin}] = 	
		h1=mocha('openArray',{varargin{:}})  %mca(1,varargin{:}); 
		for i = 1:length(h1)
			[varargout{i}] = h1(i);	
		end		
		mocha('openNow');	
else
    error('All arguments must be strings')
end

    
