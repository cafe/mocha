#! /bin/bash

# User to set the default MATLAB version here
# This default is ONLY activated 
# (1) in the absence of the relevant input argument, and
# (2) if matlab is not already pre-loaded on your system
MATLAB_DEFAULT=2017b

# cafe-matlab.sh script to enable use of the mocha mex-file
# The script executes the command: module load cafe-matlab/<matlab-version> 
# which will pre-pend the $MATLABPATH environment variable with the location 
# of the matching mocha mex-file
# Jan Chrin, 5 April 2017
#
# Usage: cafe-matlab.sh -v <matlab version> -d <default matlab version> -s <true/false>
# where the input key value pairs are **optional** 
# Examples of usage:
# (1) Use mocha with MATLAB version 2017b but do not start matlab [-s false is default] 
# cafe-matlab.sh -v 2017b  
# (2) Use mocha with MATLAB version 2017b and start matlab
# cafe-matlab.sh -v 2017b -s true 
# (3) Use mocha with MATLAB version given by MATLAB_DEFAULT in script 
# cafe-matlab.sh -v default -s true
# cafe-matlab.sh --usedefault  [equivalent -v default -s true]
# (4) Use mocha with preloaded matlab (else if none, then that given by -d option else MATLAB_DEFAULT ) 
# cafe-matlab.sh -d 2017b 
# cafe-matlab.sh --preloaded [equivalent to -v preloaded -s true]


# If no instruction to the matlab version is given by the user then the procedure is:
# (1) If MATLAB is already loaded ($MATLAB has automatically been set), 
# then the location of the corresponding mex-file will be pre-pended to $MATLABPATH 
# (2) if MATLAB is **not** already loaded, then the script will use a default MATLAB 
# version, as given by the -d option else that given by $MATLAB_DEFAULT in the above.
# and the location of the corresponding mex-file will be pre-pended to $MATLABPATH

PRINT_INFO=false

# Reset (required)
MATLAB_START=false
MATLAB_REQUESTED=

MATLAB_V=$MATLAB_VERSION

#For MATLAB version 2015a and earlier
if [ ! $MATLAB_VERSION ] 
then
	if [ ${MATLAB} ]
	then
		MATLAB_EXT=${MATLAB##*/}
		#use IFS to split string into arrays with . being the delimeter
		IFS='.' read -ra NAMES <<< $MATLAB_EXT
		MATLAB_V=${NAMES[0]}
		#echo $MATLAB_EXT
	fi
fi


# Loop round input arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -v|-V|--version|--Version)
		c=$2		
    MATLAB_REQUESTED=${c##*/}
		if [ $PRINT_INFO = true ]
		then
		  echo MATLAB VERSION REQUESTED  = "${MATLAB_REQUESTED}"
		fi
    shift 
    ;;
    -s|-S|--start|--Start)
    MATLAB_START="$2"
		if [ $PRINT_INFO = true ]
		then
		  echo MATLAB START = "${MATLAB_START}"
		fi
    shift 
    ;; 
		-d|-D|--default|--Default)
    MATLAB_DEFAULT="$2"
		if [ $PRINT_INFO = true ]
		then
		  echo MATLAB DEFAULT = "${MATLAB_DEFAULT}"
		fi
    shift 
    ;; 
		--preloaded)
		MATLAB_REQUESTED="preloaded"
    MATLAB_START=false
		if [ $PRINT_INFO = true ]
		then
		  echo MATLAB VERSION REQUESTED  = "${MATLAB_REQUESTED}"
		  echo MATLAB START = "${MATLAB_START}"
		fi	
    ;;		
    --usedefault)
		MATLAB_REQUESTED="default"
    MATLAB_START=false
		if [ $PRINT_INFO = true ]
		then
		  echo MATLAB VERSION REQUESTED  = "${MATLAB_REQUESTED}"
		  echo MATLAB START = "${MATLAB_START}"
		fi		
    ;;
		-h|-H|--help|--Help)
		echo '*********************************************************'
		echo Usage: cafe-matlab.sh 
		echo '-v <matlabVersion>  If omitted, uses pre-loaded matlab version, else default version' 
		echo '-d <matlabVersion>  Override default version to be used if matlab module not already loaded'
		echo '-s <true/false>     Whether or not to start matlab; default is false.'
		echo '                    Option is valid for matlab versions 2020a and earlier '
		echo '--show              Shows matlab version currently loaded and the script default version'
		echo '*********************************************************'	
		echo 'e.g. cafe-matlab.sh -v 2019a -s false % use matlab/2019a'
		echo 'e.g. cafe-matlab.sh -d 2019a -s false % Use pre-loaded matlab module else matlab/2019a'
		echo 'e.g. cafe-matlab.sh % Use pre-loaded matlab module, else that given in script'
		return  2> /dev/null || exit 
		;;
		--show)
		echo '*********************************************************'
		if [ $MATLAB_V ] 
		then
		 echo MATLAB VERSION CURRENTLY LOADED IS ${MATLAB_V}
		else
		 echo 'MATLAB MODULE NOT CURRENTLY LOADED'
		fi
	  echo THE SCRIPT DEFAULT IS $MATLAB_DEFAULT BUT WILL ONLY BE ACTIVATED IF MATLAB
		echo 'IS NOT ALREADY LOADED OR OTHERWISE SPECIFIED BY THE -v OPTION'
		echo '$MATLAB_DEFAULT CAN BE OVERRIDDEN USING THE -d OPTION'		
		echo '*********************************************************'
		return  2> /dev/null || exit 
		;;
    *)
		echo Unknown input key: "$key" 
		echo Usage: 'cafe-matlab -v <matlab version> -s <true/false>'
    echo where '-s true'  will start matlab - default is false
		echo Executing script with default options  
            # unknown option
    ;;
esac
shift #
done



if [ $MATLAB_REQUESTED ]
then
   
  if [ $MATLAB_REQUESTED == 'default' ]
	then
		MATLAB_REQUESTED=$MATLAB_DEFAULT
	fi	
	if [ $MATLAB_REQUESTED == 'preloaded' ]
	then
		MATLAB_REQUESTED= #leave empty
	fi
	
fi	


#echo $MATLAB_V
#echo $MATLAB_REQUESTED
#echo $MATLAB_DEFAULT

module use Cafe

#if -v is given then load MATLAB_REQUESTED
#if -v not given or if -v preloaded, then load MATLAB_V
#else load MATLAB_DEFAULT

if [ $MATLAB_REQUESTED ]
then
  
  if test -f "/opt/psi/Cafe/modulefiles/cafe-matlab/${MATLAB_REQUESTED}"
  then 
	  
		#unload removes $MATLAB_VERSION
		module unload matlab
	        module unload cafe-matlab
	        module load cafe-matlab/${MATLAB_REQUESTED}
		if [ $PRINT_INFO = true ]
		then
		  echo "Using requested version: matlab/$MATLAB_REQUESTED"
		fi	
	else
		echo "Unknown requested version: matlab/$MATLAB_REQUESTED"
		echo "'module avail cafe-matlab' gives the possibilities:"
		module avail cafe-matlab
		MATLAB_START=false
	fi	
elif  [ $MATLAB_V ] 
then
	if test -f "/opt/psi/Cafe/modulefiles/cafe-matlab/${MATLAB_V}"
	then
		#unload removes $MATLAB_VERSION
		module unload matlab
	        module unload cafe-matlab
	        module load cafe-matlab/${MATLAB_V}
		if [ $PRINT_INFO = true ]
		then
		  echo "Using matlab/$MATLAB_V"
		fi	
	else
	        module unload matlab
	        module unload cafe-matlab
	        module load cafe-matlab/$MATLAB_DEFAULT
		if [ $PRINT_INFO = true ]
		then
		  echo "Using matlab/$MATLAB_DEFAULT"
		fi	
	fi	
else
  if test -f "/opt/psi/Cafe/modulefiles/cafe-matlab/${MATLAB_DEFAULT}"
	then
                module unload matlab
		module unload cafe-matlab
                module load cafe-matlab/$MATLAB_DEFAULT
		if [ $PRINT_INFO = true ]
		then
	  	echo "Using matlab/$MATLAB_DEFAULT"	
		fi
	else
		echo "The default requested version: matlab/$MATLAB_DEFAULT within cafe-matlab.sh is invalid!"
		echo "'module avail cafe-matlab' gives the possibilities:"
		module avail cafe-matlab
	fi	
fi 

#safe versions
mvarray=("2015a" "2016a" "2016b" "2017a" "2017b" "2018a" "2019a" "2019b" "2020a")
if [[ ! " ${mvarray[*]} " =~ " ${MATLAB_REQUESTED} " ]]; then
    #echo "Version is not in array"
    #ld-preload for 2020b onwards on redhat7
    if [[ -z "${LD_PRELOAD}" ]]; then
	export LD_PRELOAD=$EPICS/base-7.0.6/lib/$EPICS_HOST_ARCH/libCom.so 
    else
	LD_PRELOAD_TEMP=$LD_PRELOAD
	export LD_PRELOAD=$EPICS/base-7.0.6/lib/$EPICS_HOST_ARCH/libCom.so:$LD_PRELOAD
    fi
    MATLAB_START=true
fi


if [ $MATLAB_START = true ]
then
matlab &
fi

## Or manually acticate cafe-matlab:
## module unload matlab
## module unload cafe-matlab
## module load cafe-matlab/2016b

## or module switch cafe-matlab/2016b cafe-matlab/2015a

if [[ ! " ${mvarray[*]} " =~ " ${MATLAB_REQUESTED} " ]]; then
    if [[ -z "$LD_PRELOAD_TEMP" ]]; then
	unset LD_PRELOAD
    else
	export LD_PRELOAD=$LD_PRELOAD_TEMP
	unset LD_PRELOAD_TEMP
    fi
    module unload cafe-matlab
    module unload matlab
fi
