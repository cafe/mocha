


struct mochaccino
{
long          id;
std::string name;

	mochaccino(long _id, std::string _name):id(_id),name(_name){};
	
	friend std::ostream& operator<<(std::ostream& os, const mochaccino& m)
	{
                //os<<m.id << " " << m.name << std::endl;
                os << m.name << std::endl;
		return os;
	}
};

struct id{};
struct name{};

typedef multi_index_container<
	mochaccino,
		indexed_by<
	                ordered_unique<
			  tag<name>, BOOST_MULTI_INDEX_MEMBER(mochaccino, std::string , name)>,
			ordered_non_unique<
			  tag<id>,   BOOST_MULTI_INDEX_MEMBER(mochaccino, long, id )> >
 > mochaccino_set;


template<typename Tag,typename MultiIndexContainer>
void print_out_by(
 const MultiIndexContainer& s,
 Tag* =0 
)
{
	
// obtain a reference to the index tagged by Tag 

  const typename boost::multi_index::index<MultiIndexContainer,Tag>::type& i=
    get<Tag>(s);

  typedef typename MultiIndexContainer::value_type value_type;

// dump the elements of the index to cout 
  std::cout << "--------------" << std::endl;
  std::cout << " METHODS LIST " << std::endl;
  std::cout << "--------------" << std::endl;
  std::copy(i.begin(),i.end(),std::ostream_iterator<value_type>(std::cout));
  std::cout << "--------------------" << std::endl;
}

typedef mochaccino_set::index<id>::type   mochaccino_set_by_id;
typedef mochaccino_set::index<name>::type mochaccino_set_by_name;





