/* Header for the wrapper.
        Contains the function signature of (wrapper) machinettaFunction such that we can use this header
        in the MEX-file to call the function in the (wrapper) machinetta library.
*/
#include "mex.h"


#define MACCHINETTA_API extern

MACCHINETTA_API void macchinettaFunction(int nlhs,mxArray *[],int nrhs, const mxArray *prhs[]);
