#include <cafe.h>
//include <hashConduit.h>
#include "macchinetta.h"
#include "containerMochaccino.h"

namespace NS_MOCHA
{

using boost::multi_index_container;
using namespace boost::multi_index;
using namespace std;

const unsigned short METHOD_NAME_MAX_SIZE=80;
const unsigned short PVNAME_MAX_SIZE=80;
const unsigned short DBR_STRING_MAX_SIZE=40;

const unsigned short MONITOR_ACTION_MAX_SIZE=255;
const unsigned short ME_MESSAGE_MAX_SIZE=255;

CAFE cafe;
CAFEGlobalAlarmCondition cgac;
CAFEGlobalAlarmSeverity cgas;


bool isCAInitialized=false;
bool collectionsLoaded=false;
bool groupsLoaded=false;

bool reportError=false;

bool withExceptions=false;

size_t neWithinAllDataCells=0;

size_t neWithinAllCells=0;
size_t  iWithinAllCells=0;
dbr_string_t * inputStringCell;
pv_string_t * inputPVStringCell;

//Declarations
mxArray *mData;
mxArray *xData;
mxArray *yData;
mxArray *zData;

mxLogical *logicalData;


int32_T * longStatus;
int64_T * longlongStatus;
int16_T * shortAlarmStatus;
int16_T * shortAlarmSeverity;

epicsTimeStamp ets1;
uint32_T * d4;
uint32_T * d9;
uint32_T * fHandle;

uint8_T  * ucharArray;
uint16_T * ushortArray;
uint32_T * ulongArray;
uint64_T * ulonglongArray;
int8_T  * charArray;
int16_T * shortArray;
int32_T * longArray;
int64_T * longlongArray;
float  * floatArray;
double * doubleArray;

bool    * inputBool;
int8_T  * inputChar;
float   * inputFloat;
int16_T * inputShort;
double  * inputDouble;
int32_T * inputLong;
int64_T * inputLongLong;

uint8_T  * inputUChar;
uint16_T * inputUShort;
uint32_T * inputULong;
uint64_T * inputULongLong;

uint32_T * handleULong;
uint64_T * handleULongLong;

uint32_T * handleULongReadback;

dbr_string_t * stringArray;


//Ctrl
uint16_T * precision;
uint16_T * noEnumStrings;
double * udl;
double * ldl;
double * ual;
double * lal;
double * uwl;
double * lwl;
double * ucl;
double * lcl;

int32_T status=ECA_NORMAL;;

int32_T pvLength;
int32_T messLength;
int32_T MELength;


char  acceleratorBase[PVNAME_MAX_SIZE];

char  groupName[PVNAME_MAX_SIZE];   //
char  pv[PVNAME_MAX_SIZE];   //
char  mess[METHOD_NAME_MAX_SIZE];  // open get set close
char  monAction[MONITOR_ACTION_MAX_SIZE];  // open get set close
char  MEmess[METHOD_NAME_MAX_SIZE];  // open get set close

mwSignedIndex nelem;

double * dd;

uint32_T handle=0;
uint32_T ghandle=0;

uint32_T monitorID=0;

int16_T nfieldsInfo=11;
int16_T nfieldsME=5;
int16_T nfields=5;
int16_T nfieldsExtra=11;
int16_T nfieldsCtrl=21;

const char * fnamesInfo[11] = {"handle","pv","channelID","connectFlag","hostName","dataType",
                               "className","accessRead", "accessWrite","nelem","connectionState"
                              };


const char * fnamesME[5] = {"pv","handle","status","statusText","statusInfo"};
const char * fnames[5]     = {"val","status","alarmStatus","alarmSeverity","ts"};
const char * fnamesExtra[11]     = {"handle","pv","val","status","status_str","alarmStatus","alarmStatus_str",
                                    "alarmSeverity","alarmSeverity_str","ts","epicsTS"
                                   };
const char * fnamesCtrl[21]= {"handle","pv","val","status","status_str","alarmStatus","alarmStatus_str",
                              "alarmSeverity","alarmSeverity_str",
                              "precision","units","noEnumStrings","enumStrings",
                              "upperDisplayLimit","lowerDisplayLimit","upperAlarmLimit",  "lowerAlarmLimit",
                              "upperWarningLimit","lowerWarningLimit","upperControlLimit","lowerControlLimit"
                             };

mxArray *fout0;
mxArray *fout1;
mxArray *fout2;
mxArray *fout3;
mxArray *fout4;
mxArray *fout5;
mxArray *fout6;
mxArray *fout7;
mxArray *fout8;
mxArray *fout9;
mxArray *fout10;
mxArray *fout11;
mxArray *fout12;
mxArray *fout13;
mxArray *fout14;
mxArray *fout15;
mxArray *fout16;
mxArray *fout17;
mxArray *fout18;
mxArray *fout1a;
mxArray *fout2a;
mxArray *fout3a;

unsigned  int nc=0,nr=0,nn=0;

mochaccino_set ms;
mochaccino_set mdt;

enum mdtIndex   {MOCHA_NATIVE=0, MOCHA_CHAR, MOCHA_UCHAR, MOCHA_SHORT, MOCHA_USHORT,
                 MOCHA_INT, MOCHA_UINT, MOCHA_LONG, MOCHA_ULONG,
                 MOCHA_FLOAT, MOCHA_DOUBLE, MOCHA_STRING
                };
//map Strings to Integers

enum mochaIndex {SHOW=1, OPEN, OPEN_ARRAY, OPEN_WAIT, GET_OPEN_WAIT_TIME,
                 OPEN_WAIT_WITH_TIME,OPEN_WAIT_TIME_TO_DEFAULT,
                 OPEN_NOWAIT, OPEN_NOW, OPEN_NOW_AND_WAIT, OPEN_GROUP,
                 CLOSE, CLOSE_ARRAY, CLOSE_GROUP, CA_POLL, MEX_UNLOCK,
		 MOCHA_VERSION, EPICS_V_STRING, CA_V_STRING,
                 WITH_EXCEPTIONS, HAVE_EXCEPTIONS, SEND_NOW,
                 GET=100, GET_INT8, GET_INT16, GET_INT32, GET_INT64, GET_UINT8,
                 GET_UINT16, GET_UINT32, GET_UINT64, GET_FLOAT, GET_DOUBLE,
                 GET_STRING, GET_CACHE, GET_ASYN, GET_SCALAR_ARRAY,
                 GET_SCALAR_ARRAY_CACHE, GET_ARRAY_ASYN,
                 GET_STRUCT_ARRAY, GET_STRUCT_ARRAY_CACHE,
                 GET_STRUCT_EXTENDED_ARRAY, GET_STRUCT_EXTENDED_ARRAY_CACHE,
                 GET_CELL_ARRAY, GET_CELL_ARRAY_CACHE,
                 GET_WF_AS_STRING, GET_WF_AS_STRING_CACHE,
                 GET_GROUP, GET_GROUP_CACHE,  GET_GROUP_EXTENDED, GET_GROUP_EXTENDED_CACHE,
                 GET_STRUCT, GET_STRUCT_CACHE, GET_STRUCT_EXTENDED, GET_STRUCT_EXTENDED_CACHE,
                 GET_ENUM_STRINGS, GET_CTRL_STRUCT, GET_CTRL_CACHE, GET_ME_STRUCT, GET_CHANNEL_INFO,
                 FETCH_INDEX_OF_GROUP_MEMBER, FETCH_INDEX_OF_COLLECTION_MEMBER,
                 GET_STATUS=200, GET_STATUS_TEXT, GET_STATUS_INFO, IS_STATUS_TIMEOUT,
                 GET_ALARM, GET_ALARM_AS_STRING,
                 STATUS_AS_STRING, ALARM_STATUS_AS_STRING, ALARM_SEVERITY_AS_STRING,
                 STATUS_LIST, ALARM_STATUS_LIST, ALARM_SEVERITY_LIST,
                 GET_TIMESTAMP,  GET_EPICS_TIMESTAMP, GET_PULSEID_FROM_TIMESTAMP,
                 GET_PULSEID_FROM_TIMESTAMP_GROUP,
                 GET_READ_ACCESS, GET_WRITE_ACCESS,
                 INIT_GET_SYNC_WITH_CALLBACK_DEFAULT,INIT_GET_SYNC_WITHOUT_CALLBACK,
                 SET=300, SET_AND_GET, SET_AND_MATCH, SET_SCALAR_ARRAY, SET_STRUCT_ARRAY, SET_GROUP, SET_PUT_PREPARE,
                 SET_PUT_WAIT, SET_PUT_NOWAIT,
                 GET_TIMEOUT, SET_TIMEOUT, SET_TIMEOUT_DEFAULT,
                 SET_TIMEOUT_PUT, SET_TIMEOUT_GET, SET_SELF_GOVERNING_TIMEOUT,
                 GET_SG_TIMEOUT, SET_SG_TIMEOUT, SET_SG_TIMEOUT_DEFAULT,
                 SET_SG_TIMEOUT_PUT, SET_SG_TIMEOUT_GET, SET_SG_SELF_GOVERNING_TIMEOUT,
                 SET_NELEM, SET_NELEM_CACHE, SET_OFFSET,
                 GET_NELEM_CLIENT, GET_NELEM_NATIVE, GET_NELEM_REQUEST,
                 GET_NELEM, GET_NELEM_CACHE, GET_OFFSET,
                 MONITOR=400, MONITOR_STOP, MONITOR_FLUSH_EVENT, 
                 GET_MONITOR_HANDLES, GET_MONITOR_HANDLES_AND_ACTIONS, GET_NO_MONITORS,
                 IS_CONNECTED=500, ALL_CONNECTED,
                 PRINT_STATUS=600, PRINT_MONITORS, PRINT_HANDLE, PRINT_HANDLES,
                 PRINT_DISCONNECTED_HANDLES,GET_HANDLES,GET_HANDLE_STATES,
                 GET_HANDLES_WITHIN_GROUP,
                 GET_DISCONNECTED_HANDLES, GET_CONNECTED_HANDLES,
                 HANDLE_FROM_PV, PV_FROM_HANDLE, LIST_COLLECTIONS, LIST_COLLECTION_MEMBERS,
                 LIST_GROUPS, LIST_GROUP_MEMBERS, DEFINE_GROUP,
                 LOAD_XML_COLLECTIONS=900, LOAD_XML_GROUPS, LOAD_SF_GROUPS, GET_CHANNEL_LIST,
                 TEST=999
                };

void msInsert()
{

    ms.insert(mochaccino(SHOW, 	 "show"));
    ms.insert(mochaccino(OPEN, 	 "open"));
    ms.insert(mochaccino(OPEN_ARRAY, 	  "openarray"));
    ms.insert(mochaccino(CLOSE_ARRAY, 	"closearray"));
    ms.insert(mochaccino(OPEN_WAIT,     "openwait"));
    ms.insert(mochaccino(GET_OPEN_WAIT_TIME, "getopenwaittime"));
    ms.insert(mochaccino(OPEN_NOWAIT,   "opennowait"));
    ms.insert(mochaccino(OPEN_NOWAIT,   "openprepare"));
    ms.insert(mochaccino(OPEN_NOW,      "opennow"));
    ms.insert(mochaccino(OPEN_NOW_AND_WAIT,      "opennowandwait"));
    ms.insert(mochaccino(OPEN_WAIT_WITH_TIME,    "setopentimeout"));
    ms.insert(mochaccino(OPEN_WAIT_TIME_TO_DEFAULT, "setopentimeouttodefault"));
    ms.insert(mochaccino(OPEN_WAIT_WITH_TIME,    "setopenwaittime"));
    ms.insert(mochaccino(OPEN_WAIT_TIME_TO_DEFAULT, "setopenwaittimetodefault"));

    ms.insert(mochaccino(OPEN_GROUP,    "opengroup"));
    ms.insert(mochaccino(CLOSE,	 "close"));

    ms.insert(mochaccino(CLOSE_GROUP,   "closegroup"));

    ms.insert(mochaccino(CA_POLL,         "capoll"));
    ms.insert(mochaccino(MEX_UNLOCK,      "mexunlock"));
    ms.insert(mochaccino(MOCHA_VERSION,   "version"));
    ms.insert(mochaccino(EPICS_V_STRING,   "epicsversion"));
    ms.insert(mochaccino(CA_V_STRING,      "caversion"));
    ms.insert(mochaccino(WITH_EXCEPTIONS,   "withexceptions"));
    ms.insert(mochaccino(WITH_EXCEPTIONS,   "setexceptions"));
    ms.insert(mochaccino(HAVE_EXCEPTIONS,   "haveexceptions"));
    ms.insert(mochaccino(SEND_NOW,      "sendnow"));
    ms.insert(mochaccino(SEND_NOW,      "caflush"));
    ms.insert(mochaccino(GET_ASYN,      "getasyn"));
    ms.insert(mochaccino(GET_ME_STRUCT, "getmestruct"));

    ms.insert(mochaccino(GET_WF_AS_STRING,        "getcharasstring"));
    ms.insert(mochaccino(GET_WF_AS_STRING_CACHE,  "getcharasstringcache"));
    ms.insert(mochaccino(GET_WF_AS_STRING,        "getwfasstring"));
    ms.insert(mochaccino(GET_WF_AS_STRING_CACHE,  "getwfasstringcache"));

    ms.insert(mochaccino(GET_SCALAR_ARRAY,      "getscalararray"));
    ms.insert(mochaccino(GET_SCALAR_ARRAY_CACHE,"getscalararraycache"));

    ms.insert(mochaccino(GET_CELL_ARRAY,        "getcellarray"));
    ms.insert(mochaccino(GET_CELL_ARRAY_CACHE,  "getcellarraycache"));

    ms.insert(mochaccino(GET_ARRAY_ASYN,        "getarrayasyn"));

    //ms.insert(mochaccino(GET_STRUCT_ARRAY,      "getstructarray"));
    //ms.insert(mochaccino(GET_STRUCT_ARRAY_CACHE,"getstructarraycache"));
    ms.insert(mochaccino(GET_STRUCT_ARRAY,      "getpvarray"));
    ms.insert(mochaccino(GET_STRUCT_ARRAY_CACHE,"getpvarraycache"));


    ms.insert(mochaccino(GET_STRUCT_EXTENDED_ARRAY,      "getstructarray"));
    ms.insert(mochaccino(GET_STRUCT_EXTENDED_ARRAY_CACHE,"getstructarraycache"));
    //ms.insert(mochaccino(GET_STRUCT_EXTENDED_ARRAY,      "getpvxarray"));
    //ms.insert(mochaccino(GET_STRUCT_EXTENDED_ARRAY_CACHE,"getpvxarraycache"));

    ms.insert(mochaccino(INIT_GET_SYNC_WITH_CALLBACK_DEFAULT, "usesyncgetwithdefaultcallback"));
    ms.insert(mochaccino(INIT_GET_SYNC_WITHOUT_CALLBACK,      "usesyncgetwithoutcallback"));

    ms.insert(mochaccino(GET,        "get"));
    ms.insert(mochaccino(GET_CACHE,  "getcache"));
    ms.insert(mochaccino(GET_UINT8,  "getuint8"));
    ms.insert(mochaccino(GET_UINT16, "getuint16"));
    ms.insert(mochaccino(GET_UINT32, "getuint32"));
    ms.insert(mochaccino(GET_UINT64, "getuint64"));
    ms.insert(mochaccino(GET_INT8,   "getint8"));
    ms.insert(mochaccino(GET_INT16,  "getint16"));
    ms.insert(mochaccino(GET_INT32,  "getint32"));
    ms.insert(mochaccino(GET_INT64,  "getint64"));
    ms.insert(mochaccino(GET_FLOAT,  "getfloat"));
    ms.insert(mochaccino(GET_FLOAT,  "getsingle"));
    ms.insert(mochaccino(GET_DOUBLE, "getdouble"));
    ms.insert(mochaccino(GET_STRING, "getstring"));

    //ms.insert(mochaccino(GET_STRUCT,        "getstruct"));
    //ms.insert(mochaccino(GET_STRUCT_CACHE,  "getstructcache"));
    ms.insert(mochaccino(GET_STRUCT,        "getpv"));
    ms.insert(mochaccino(GET_STRUCT_CACHE,  "getpvcache"));

    ms.insert(mochaccino(GET_STRUCT_EXTENDED,        "getstruct"));
    ms.insert(mochaccino(GET_STRUCT_EXTENDED_CACHE,  "getstructcache"));
    //ms.insert(mochaccino(GET_STRUCT_EXTENDED,        "getpvx"));
    //ms.insert(mochaccino(GET_STRUCT_EXTENDED_CACHE,  "getpvxcache"));

    ms.insert(mochaccino(GET_GROUP,         "getgroup"));
    ms.insert(mochaccino(GET_GROUP_CACHE,   "getgroupcache"));
    ms.insert(mochaccino(GET_GROUP,         "getpvgroup"));
    ms.insert(mochaccino(GET_GROUP_CACHE,   "getpvgroupcache"));
    ms.insert(mochaccino(GET_GROUP_EXTENDED,         "getstructgroup"));
    ms.insert(mochaccino(GET_GROUP_EXTENDED_CACHE,   "getstructgroupcache"));


    ms.insert(mochaccino(GET_ENUM_STRINGS,   "getenumstrings"));
    ms.insert(mochaccino(GET_CTRL_STRUCT,   "getctrl"));
    ms.insert(mochaccino(GET_CTRL_CACHE,    "getctrlcache"));

    ms.insert(mochaccino(FETCH_INDEX_OF_GROUP_MEMBER, "idxgrpmem"));
    ms.insert(mochaccino(FETCH_INDEX_OF_COLLECTION_MEMBER, "idxcolmem"));
    ms.insert(mochaccino(LIST_GROUPS,           "listgroups"));
    ms.insert(mochaccino(LIST_GROUP_MEMBERS,    "listgroupmembers"));
    ms.insert(mochaccino(LIST_COLLECTIONS,           "listcollections"));
    ms.insert(mochaccino(LIST_COLLECTION_MEMBERS,    "listcollectionmembers"));
    ms.insert(mochaccino(STATUS_AS_STRING,            "statusasstring"));
    ms.insert(mochaccino(ALARM_STATUS_AS_STRING,      "alarmstatusasstring"));
    ms.insert(mochaccino(ALARM_SEVERITY_AS_STRING,    "alarmseverityasstring"));

    ms.insert(mochaccino(STATUS_LIST,             "statuslist"));
    ms.insert(mochaccino(ALARM_STATUS_LIST,             "alarmstatuslist"));
    ms.insert(mochaccino(ALARM_SEVERITY_LIST,           "alarmseveritylist"));

    ms.insert(mochaccino(GET_STATUS,            "getstatus"));
    ms.insert(mochaccino(GET_STATUS_TEXT,       "statusastext"));
    ms.insert(mochaccino(GET_STATUS_INFO,       "statusinfo"));
    ms.insert(mochaccino(IS_STATUS_TIMEOUT,     "istimeout"));
    ms.insert(mochaccino(GET_ALARM,             "getalarm"));
    ms.insert(mochaccino(GET_ALARM_AS_STRING,             "getalarmasstring"));
    ms.insert(mochaccino(GET_TIMESTAMP,         "gettimestamp"));
    ms.insert(mochaccino(GET_EPICS_TIMESTAMP,         "getepicstimestamp"));
    ms.insert(mochaccino(GET_EPICS_TIMESTAMP,         "getets"));
    //ms.insert(mochaccino(GET_PULSEID_FROM_TIMESTAMP, "getpulseidfromts"));
    //ms.insert(mochaccino(GET_PULSEID_FROM_TIMESTAMP_GROUP,"getgrouppulseidfromts"));

    ms.insert(mochaccino(GET_READ_ACCESS,   "getreadaccess"));
    ms.insert(mochaccino(GET_WRITE_ACCESS,  "getwriteaccess"));
    ms.insert(mochaccino(GET_CHANNEL_INFO,  "getchannelinfo"));
    ms.insert(mochaccino(GET_CHANNEL_INFO,  "getinfo"));

    ms.insert(mochaccino(SET,               "set"));
    ms.insert(mochaccino(SET_AND_GET,               "setandget"));
    ms.insert(mochaccino(SET_AND_MATCH,               "setandmatch"));
    ms.insert(mochaccino(SET_SCALAR_ARRAY,  "setscalararray"));
    ms.insert(mochaccino(SET_STRUCT_ARRAY,  "setstructarray"));
    ms.insert(mochaccino(SET_STRUCT_ARRAY,  "setpvarray"));
    ms.insert(mochaccino(SET_STRUCT_ARRAY,  "setmany"));


    ms.insert(mochaccino(SET_GROUP,         "setgroup"));
    ms.insert(mochaccino(SET_PUT_PREPARE,       "setputprepare"));
    ms.insert(mochaccino(SET_PUT_WAIT,          "setputwait"));
    ms.insert(mochaccino(SET_PUT_NOWAIT,        "setputnowait"));
    ms.insert(mochaccino(SET_PUT_NOWAIT,       "setputdefault"));

    ms.insert(mochaccino(GET_TIMEOUT,   "gettimeout"));
    ms.insert(mochaccino(SET_TIMEOUT,   "settimeout"));
    ms.insert(mochaccino(SET_TIMEOUT_PUT,    "settimeoutput"));
    ms.insert(mochaccino(SET_TIMEOUT_GET,    "settimeoutget"));
    ms.insert(mochaccino(SET_TIMEOUT_DEFAULT,"settimeoutdefault"));
    ms.insert(mochaccino(SET_SELF_GOVERNING_TIMEOUT,"setselfgoverningtimeout"));

    ms.insert(mochaccino(GET_SG_TIMEOUT,   "getsgtimeout"));
    ms.insert(mochaccino(SET_SG_TIMEOUT,   "setsgtimeout"));
    ms.insert(mochaccino(SET_SG_TIMEOUT_PUT,    "setsgtimeoutput"));
    ms.insert(mochaccino(SET_SG_TIMEOUT_GET,    "setsgtimeoutget"));
    ms.insert(mochaccino(SET_SG_TIMEOUT_DEFAULT,"setsgtimeoutdefault"));
    ms.insert(mochaccino(SET_SG_SELF_GOVERNING_TIMEOUT,"setsgselfgoverningtimeout"));

    ms.insert(mochaccino(SET_NELEM,     "setnelem"));
    ms.insert(mochaccino(SET_NELEM_CACHE,"setnelemcache"));

    ms.insert(mochaccino(SET_OFFSET,    "setoffset"));
    ms.insert(mochaccino(GET_NELEM_CLIENT,     "getnelemclient"));
    ms.insert(mochaccino(GET_NELEM_NATIVE,     "getnelemnative"));
    ms.insert(mochaccino(GET_NELEM_REQUEST,    "getnelemrequest"));
    ms.insert(mochaccino(GET_NELEM,     "getnelem"));
    ms.insert(mochaccino(GET_NELEM_CACHE,"getnelemcache"));

    ms.insert(mochaccino(GET_OFFSET,    "getoffset"));

    ms.insert(mochaccino(MONITOR,       "monitorstart"));
    ms.insert(mochaccino(MONITOR,       "monitor"));
    ms.insert(mochaccino(MONITOR_STOP,  "monitorstop"));
    ms.insert(mochaccino(MONITOR_FLUSH_EVENT,  "monitorflushevent"));
		
    ms.insert(mochaccino(GET_NO_MONITORS,  "getnomonitors"));
    ms.insert(mochaccino(GET_MONITOR_HANDLES,  "getmonitorhandles"));
    ms.insert(mochaccino(GET_MONITOR_HANDLES_AND_ACTIONS,  "getmonitorhandlesandactions"));

    ms.insert(mochaccino(IS_CONNECTED,  "isconnected"));
    ms.insert(mochaccino(ALL_CONNECTED,  "allconnected"));

    ms.insert(mochaccino(PRINT_STATUS,     "printstatus"));
    ms.insert(mochaccino(PRINT_DISCONNECTED_HANDLES,"printdisconnectedhandles"));
    ms.insert(mochaccino(GET_HANDLES,  "gethandles"));
    ms.insert(mochaccino(GET_HANDLE_STATES,  "gethandlestates"));
    ms.insert(mochaccino(GET_HANDLES_WITHIN_GROUP,  "gethandleswithingroup"));

    ms.insert(mochaccino(GET_CONNECTED_HANDLES,  "getconnectedhandles"));
    ms.insert(mochaccino(GET_DISCONNECTED_HANDLES,  "getdisconnectedhandles"));
    ms.insert(mochaccino(PRINT_MONITORS,     "printmonitors"));
    ms.insert(mochaccino(PRINT_HANDLE,     "printhandle"));
    ms.insert(mochaccino(PRINT_HANDLES,    "printhandles"));
    ms.insert(mochaccino(HANDLE_FROM_PV,   "gethandlefrompv"));
    ms.insert(mochaccino(PV_FROM_HANDLE,   "getpvfromhandle"));
    ms.insert(mochaccino(DEFINE_GROUP,     "definegroup"));

    ms.insert(mochaccino(LOAD_XML_COLLECTIONS,"loadxmlcollections"));
    ms.insert(mochaccino(LOAD_XML_GROUPS,     "loadxmlgroups"));
    ms.insert(mochaccino(LOAD_SF_GROUPS,      "loadsfgroups"));
    ms.insert(mochaccino(GET_CHANNEL_LIST,    "getchannellist"));
    ms.insert(mochaccino(TEST, 	 "test"));
}

void mdtInsert()
{
    mdt.insert(mochaccino(MOCHA_NATIVE, "native"));
    mdt.insert(mochaccino(MOCHA_CHAR,   "int8"));
    mdt.insert(mochaccino(MOCHA_UCHAR,  "uint8"));
    mdt.insert(mochaccino(MOCHA_SHORT,  "int16"));
    mdt.insert(mochaccino(MOCHA_USHORT, "uint16"));
    mdt.insert(mochaccino(MOCHA_INT,    "int32"));
    mdt.insert(mochaccino(MOCHA_UINT,   "uint32"));
    mdt.insert(mochaccino(MOCHA_LONG,   "int64"));
    mdt.insert(mochaccino(MOCHA_ULONG,  "uint64"));
    mdt.insert(mochaccino(MOCHA_FLOAT,  "single"));
    mdt.insert(mochaccino(MOCHA_FLOAT,  "float"));
    mdt.insert(mochaccino(MOCHA_DOUBLE, "double"));
    mdt.insert(mochaccino(MOCHA_STRING, "char"));
    mdt.insert(mochaccino(MOCHA_STRING, "string"));
}

void printStatus(int status )
{
    std::string s =  cafe.getCafeStatus().csi.message(status);
    std::string c =  cafe.getCafeStatus().csc.message(status);
    mexPrintf("%s\n",c.c_str());
    mexPrintf("%s\n",s.c_str());
}


void printStringInputError()
{
    mexPrintf("Error in intepreting pv in input argument:\n");
    mexPrintf("PV name does not have a char value \n");
    mexPrintf("MATLAB mx library does not support string/mxSTRING_CLASS as input\n");
    mexPrintf("Please use char/mxCHAR_CLASS as data type instead\n");
	  mexPrintf("i.e., use single quotes instead of double quotes in input argument!\n");
}

std::string prepareExceptionID(int status )
{
    std::string exID="CAFE_";
    exID.append(cafe.getCafeStatus().severity(status));
    exID.append(":");
    exID.append(cafe.getCafeStatus().msgID(status));
    return exID;
}


std::string prepareExceptionText(unsigned int handle, int status )
{
    std::string exText="PV=";
    exText.append(cafe.getPVFromHandle(handle));
    exText.append(" Handle=");
    exText.append(static_cast<std::ostringstream*>( &(std::ostringstream() << handle) )->str());
    exText.append(" statusCode=");
    exText.append(static_cast<std::ostringstream*>( &(std::ostringstream() << status) )->str());
    exText.append(" statusText=");
    exText.append(cafe.getCafeStatus().msgID(status));
    exText.append(" statusInfo=");
    exText.append(cafe.getCafeStatus().info(status));
    return exText;
}



void fillMEStruct(std::string sME)
{

    fout1 = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
    ulongArray = (uint32_T *) mxGetData (fout1);

    fout2 = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
    longStatus = (int32_T *) mxGetData (fout2);

    fout0 = mxCreateCellMatrix(1,1);
    fout3 = mxCreateCellMatrix(1,1);
    fout4 = mxCreateCellMatrix(1,1);

    std::string str2 ="";
    std::string str  ="";

    std::size_t found = sME.find("PV=");

    if (found != std::string::npos)
    {
        str2 = sME.substr(found+3);
        found  = str2.find(" ");
        str = str2.substr(0,found);
        mexPrintf("PV %s\n", str.c_str());
        mxSetCell (fout0, 0, mxCreateString( str.c_str()));
    }

    found= sME.find("Handle=");
    if (found!= std::string::npos)
    {
        str2 = sME.substr(found+7);
        found  = str2.find(" ");
        str = str2.substr(0,found);
        //mexPrintf("handle %d\n", std::atoi(str.c_str()));
        ulongArray[0]= std::atoi(str.c_str());
    }

    found= sME.find("statusCode=");
    if (found!= std::string::npos)
    {

        str2 = sME.substr(found+11);
        found  = str2.find(" ");
        str = str2.substr(0,found);
        //mexPrintf("status %d\n", std::atoi(str.c_str()));
        longStatus[0]= std::atoi(str.c_str());
    }

    found= sME.find("statusText=");
    if (found!= std::string::npos)
    {
        str2 = sME.substr(found+11);
        found  = str2.find(" ");
        str = str2.substr(0,found);
        //mexPrintf("statusText %s\n", str.c_str());
        mxSetCell (fout3, 0, mxCreateString(str.c_str()));
    }

    found= sME.find("statusInfo=");
    if (found!= std::string::npos)
    {
        str2 = sME.substr(found+11);
        //mexPrintf("statusInfo %s\n", str2.c_str());
        mxSetCell (fout4, 0, mxCreateString(str2.c_str()));
    }

    return;
}



bool isValidClass(const mxArray * inputClass)
{
    // Refuse these input mx classes;

    mxClassID mxid =mxGetClassID( inputClass);

    switch(mxid)
    {
    case mxUNKNOWN_CLASS:
        mexPrintf("Undetermined class: %s \n",mxGetClassName(  inputClass) );
        return false;
        break;
    case mxSTRUCT_CLASS:
    case mxFUNCTION_CLASS:
        mexPrintf("The requested class is not yet supported: %s \n",mxGetClassName(  inputClass) );
        return false;
        break;
    case mxVOID_CLASS:
        mexPrintf("This requested class is reserved: %s \n",mxGetClassName(  inputClass) );
        return false;
        break;
    }

    return true;
}


unsigned  int getMdtIndexFromCafeDataType(CAFE_DATATYPE cdt)
{
    unsigned  int mdtIndex;

    switch ( cdt)
    {
    case CAFE_DOUBLE:
        mdtIndex=MOCHA_DOUBLE;
        break;
    case CAFE_FLOAT:
        mdtIndex=MOCHA_FLOAT;
        break;
    case CAFE_SHORT:
        mdtIndex=MOCHA_SHORT;
        break;
    case CAFE_ENUM:
        mdtIndex=MOCHA_USHORT;
        break;
    case CAFE_CHAR:
        mdtIndex=MOCHA_UCHAR;
        break;
    case CAFE_LONG:
        mdtIndex=MOCHA_INT;
        break;
    case CAFE_STRING:
        mdtIndex=MOCHA_STRING;
        break;
    default: //If not connected for instance
        mdtIndex=MOCHA_DOUBLE;
        break;
    }
    return mdtIndex;
}

unsigned  int getMdtIndex(const mxArray * l)
{

    char  messStruct[METHOD_NAME_MAX_SIZE];
    mxArray * mDataStruct;

    unsigned  int mdtIndex=MOCHA_NATIVE;

    mochaccino_set_by_name & name_mdt_index = mdt.get<name> ();
    mochaccino_set_by_name::iterator it_name_mdt;

    short messLengthStruct = METHOD_NAME_MAX_SIZE+1;
    //message
    mDataStruct = (mxArray *)  l;
    mxGetString(mDataStruct, messStruct, messLengthStruct);

    //Not allowed!
    //mxFree(mxGetData(mDataStruct)); // Added line to avoid memory leak
    //mxDestroyArray(mDataStruct);

    //remove leadingTrailing Blanks
    const char * _messStruct=messStruct;
    cafe.getHandleHelper().removeLeadingAndTrailingSpaces(_messStruct,messStruct);

    //tolowercase
    std::string messString = messStruct;

    if(messString.size()==0)
    {
        return mdtIndex;
    }

    std::transform(messString.begin(), messString.end(), messString.begin(), ::tolower);
    strcpy(messStruct,messString.c_str());

    //Is Valid Data Type??
    it_name_mdt = name_mdt_index.find(messStruct);

    if (it_name_mdt != name_mdt_index.end())
    {
        mdtIndex=(*it_name_mdt).id;
        //mexPrintf("MESSAGE INDEX: %d %s ", mdtIndex, messStruct);
    }
    else
    {
        mexPrintf("WARNING: Unknown data type: %s\n", messStruct);
        mexPrintf("WARNING: Possible data types are listed. \n");
        mexPrintf("WARNING: Using native type if known else double\n");
        print_out_by<id>(mdt);
    }

    return mdtIndex;
}



void scanCell(const mxArray * thisCell)
{

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);
    for (mwIndex i=0; i<thisNeWithinObject; i++)
    {
        mxArray * newCell = mxGetCell(thisCell,i);

        if ( !mxIsCell(newCell) )
        {
            if (!isValidClass(newCell)) return; //message printed
            size_t neWithinObject = mxGetNumberOfElements (newCell);

            //This line is important for a string as the Number of Elements
            //is interpreted as the number of characters!!!
            //Hence for ne to be 1
            if (mxGetClassID( newCell)==mxCHAR_CLASS)
            {
                neWithinObject=1;
            }
            neWithinAllCells=neWithinAllCells+neWithinObject;
        }
        else
        {
            scanCell(newCell);
        }
    }
}

unsigned int getNelemFromDataCell(const mxArray * thisCell)
{

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);

    for (mwIndex i=0; i<thisNeWithinObject; i++)
    {
        mxArray * newCell = mxGetCell(thisCell,i);

        if ( !mxIsCell(newCell) )
        {
            neWithinAllDataCells=neWithinAllDataCells+1;

            /*
            if (!isValidClass(newCell)) continue;; //message printed
            neWithinAllDataCells=neWithinAllDataCells+1;

            size_t neWithinObject = mxGetNumberOfElements (newCell);

            //This line is important for a string as the Number of Elements
            //is interpreted as the number of characters!!!
            //Hence for ne to be 1
            if (mxGetClassID( newCell)==mxCHAR_CLASS) { neWithinObject=1; }
            neWithinAllDataCells=neWithinAllDataCells+neWithinObject;
            */

        }
        else
        {
            getNelemFromDataCell(newCell);
        }
    }

    return neWithinAllDataCells;
}


unsigned int getNObjectsFromDataCell(const mxArray * thisCell)
{

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);

    if (thisNeWithinObject==1)
    {
        mxArray * newCell = mxGetCell(thisCell,0);

        if ( !mxIsCell(newCell) && isValidClass(newCell))
        {

            thisNeWithinObject = mxGetNumberOfElements (newCell);

            //This line is important for a string as the Number of Elements
            //is interpreted as the number of characters!!!
            //Hence for ne to be 1
            if (mxGetClassID( newCell)==mxCHAR_CLASS)
            {
                thisNeWithinObject=1;
            }


        }
        else if(isValidClass(newCell))
        {
            getNObjectsFromDataCell(newCell);
        }
    }

    return thisNeWithinObject;
}


char getEscapeSequence(char cNext, bool & recognizedFlag)  {
    char wfChar = cNext;
    recognizedFlag = true;
  
    switch (cNext) {
        case '0':
	    wfChar = '\0'; 
            break;
	case '\'':
	    wfChar = '\''; 
            break;
        case '"':
	    wfChar = '\"'; 
            break;
	case '?':
	    wfChar = '\?'; 
            break;
	case '\\':
	    wfChar = '\\'; 
            break;
        case 'a':
	    wfChar = '\a'; 
            break;
	case 'b':
	    wfChar = '\b'; 
            break;
        case 'f':
	    wfChar = '\f'; 
            break;
	case 'n':
	    wfChar = '\n'; 
            break;
        case 'r':
	    wfChar = '\r'; 
            break;
	case 't':
	    wfChar = '\t'; 
            break;
        case 'v':
	    wfChar = '\v'; 
            break;
	default:
            // Unrecognized escape sequence; print slash 
	    wfChar = '\\';   
            recognizedFlag = false;                
	    break;
    }
    
    return wfChar;
}


/*
unsigned int getMxArrayFromDataCell(const mxArray * thisCell){

    mxArray * mxDataWithinCell;

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);

    for (mwIndex i=0; i<thisNeWithinObject; i++) {
        mxArray * newCell = mxGetCell(thisCell,i);

        if ( !mxIsCell(newCell) ) {
						mxDataWithinCell=newCell;

        }
        else {
            getMxArrayFromDataCell(newCell);
        }
    }

		return mxDataWithinCell;
}
*/


void cellToString(const mxArray * thisCell)
{

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);

    iWithinAllCells=0;

    for (mwIndex i=0; i<thisNeWithinObject; i++)
    {
        mxArray * newCell = mxGetCell(thisCell,i);

        if ( !mxIsCell(newCell) )
        {
            if (!isValidClass(newCell)) return; //message printed
            size_t ne = mxGetNumberOfElements (newCell);

            mxClassID mxid =mxGetClassID( newCell);

            //fill String
            switch(mxid)
            {
            case mxDOUBLE_CLASS:
                inputDouble = (double *) mxGetPr ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%.15f",inputDouble[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxSINGLE_CLASS:
                inputFloat =  (float *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%f",inputFloat[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxLOGICAL_CLASS:
                inputBool =   (bool *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%d",inputBool[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT8_CLASS:
                inputChar =   (int8_T *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%u",inputChar[j]);
                    ++iWithinAllCells;
                }

                break;
            case mxUINT8_CLASS:
                inputUChar =  (unsigned char *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%u",inputUChar[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT16_CLASS:
                inputShort =  (short *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%d",inputShort[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT16_CLASS:
                inputUShort =  (unsigned short *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%d",inputUShort[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT32_CLASS:
                inputLong =   (int32_T *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%d",inputLong[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT32_CLASS:
                inputULong = (uint32_T *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%d",inputULong[j]);
                    ++iWithinAllCells;
                }
                break;

            case mxINT64_CLASS:
                inputLongLong = (int64_T *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%ld",inputLongLong[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT64_CLASS:
                inputULongLong = (uint64_T *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputStringCell[iWithinAllCells],"%ld",inputULongLong[j]);
                    ++iWithinAllCells;
                }

                break;
            case mxCHAR_CLASS:
	        unsigned int neLocal=1;
                
                if (ne > (DBR_STRING_MAX_SIZE-1))  {
		    mexPrintf("Exceeded (with %d) max limit of %d chars. Ignoring this element of cell\n", 
                            ne, (DBR_STRING_MAX_SIZE-1));
                    break;
		}
                             

                dbr_string_t * strBuffer = new dbr_string_t[neLocal];
                mxGetString(newCell, *strBuffer, sizeof(dbr_string_t)*neLocal);
                //mexPrintf("ne %d\n", ne);
                //mexPrintf("strBuffer %d\n", *strBuffer[0]);
                //mexPrintf("strBuffer %c \n", *strBuffer[0]);
                char c =  strBuffer[0][0];  	
                char cNext;
                if (c == '\\' && ne > neLocal) {
		
	          cNext = strBuffer[0][1]; 
		  //mexPrintf("char cNext %c\n", cNext);
                  bool recognizedFlag = true; 
                  
                  char cNew = NS_MOCHA::getEscapeSequence(cNext, recognizedFlag);
                  //Must loop over all ne!
                  for (unsigned int i=0; i < ne; ++i) { 
		    strBuffer[0][i] = cNew;
                  }
                  /* Must loop over all ne
		  switch (cNext) {
		   case '0':
		      strBuffer[0][0] = '\0';                  
                      break;
                   case '\'':
		      strBuffer[0][1]  = '\''; 
                      break;
                   case '"':
		      strBuffer[0][1]  = '\"'; 
                      break;
		   case '?':
		      strBuffer[0][1]  = '\?'; 
                      break;
		   case '\\':
		      strBuffer[0][1]  = '\\'; 
                      break;
                   case 'a':
		      strBuffer[0][1]  = '\a'; 
                      break;
		   case 'b':
		      strBuffer[0][1]  = '\b'; 
                      break;
                   case 'f':
		      strBuffer[0][1]  = '\f'; 
                      break;
		   case 'n':
		     //strBuffer[0][0]  = '\n';
                       for (unsigned int i=0; i < ne; ++i) {
		         strBuffer[0][i]  = '\n';
                       } 		      
                      break;
                   case 'r':
		      strBuffer[0][1]  = '\r'; 
                      break;
		   case 't':
		      //strBuffer[0][0]  = '\t';
		      for (unsigned int i=0; i < ne; ++i) {
		       strBuffer[0][i]  = '\t';
                       }  		    
                      break;
                   case 'v':
		      strBuffer[0][1]  = '\v'; 
                      break;
		   default:
                     // Unrecognized escape sequence; print slAH
		      strBuffer[0][1]  = '\\';                 
		      break;
                   }
		  */
		}
		 

                for (mwIndex j=0; j< neLocal; ++j )
                {		    
		  strcpy(inputStringCell[iWithinAllCells], strBuffer[j]);
                    ++iWithinAllCells;
                }
                delete [] strBuffer;
               
                break;
            }

        }
        else
        {
            cellToString(newCell);
        }

    }
}




void cellToPVString(const mxArray * thisCell)
{

    size_t thisNeWithinObject = mxGetNumberOfElements (thisCell);


    for (mwIndex i=0; i<thisNeWithinObject; i++)
    {
        mxArray * newCell = mxGetCell(thisCell,i);

        if ( !mxIsCell(newCell) )
        {
            if (!isValidClass(newCell)) return; //message printed
            size_t ne = mxGetNumberOfElements (newCell);

            mxClassID mxid =mxGetClassID( newCell);

            //fill String
            switch(mxid)
            {
            case mxDOUBLE_CLASS:
                inputDouble = (double *) mxGetPr ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%.15f",inputDouble[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxSINGLE_CLASS:
                inputFloat =  (float *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%f",inputFloat[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxLOGICAL_CLASS:
                inputBool =   (bool *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%d",inputBool[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT8_CLASS:
                inputChar =   (int8_T *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%u",inputChar[j]);
                    ++iWithinAllCells;
                }

                break;
            case mxUINT8_CLASS:
                inputUChar =  (unsigned char *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%u",inputUChar[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT16_CLASS:
                inputShort =  (short *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%d",inputShort[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT16_CLASS:
                inputUShort =  (unsigned short *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%d",inputUShort[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxINT32_CLASS:
                inputLong =   (int32_T *) mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%d",inputLong[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT32_CLASS:
                inputULong = (uint32_T *)  mxGetData ( newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%d",inputULong[j]);
                    ++iWithinAllCells;
                }
                break;

            case mxINT64_CLASS:
                inputLongLong = (int64_T *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%ld",inputLongLong[j]);
                    ++iWithinAllCells;
                }
                break;
            case mxUINT64_CLASS:
                inputULongLong = (uint64_T *)  mxGetData (newCell);
                for (mwIndex j=0; j< ne; ++j )
                {
                    sprintf(inputPVStringCell[iWithinAllCells],"%ld",inputULongLong[j]);
                    ++iWithinAllCells;
                }

                break;
            case mxCHAR_CLASS:
                ne=1;
                pv_string_t * strBuffer = new pv_string_t[ne];
                mxGetString(newCell, *strBuffer, sizeof(pv_string_t)*ne);

                for (mwIndex j=0; j< ne; ++j )
                {

                    strcpy(inputPVStringCell[iWithinAllCells],strBuffer[j]);

                    ++iWithinAllCells;
                }
                delete [] strBuffer;
                break;
            }

        }
        else
        {

            cellToPVString(newCell);
        }

    }
}





std::vector<unsigned int> openArray(std::vector<std::string> vstr)
{


    std::vector<uint32_T> vuint32;
    vuint32.reserve(vstr.size());

    std::fill(vuint32.begin(), vuint32.end(), 0);

    try
    {
        cafe.open(vstr, vuint32);
    }
    catch(CAFEException_open &e)
    {

        mexPrintf("---------------\n");
        //cout << e.what() << endl;
        mexPrintf("%s\n",   e.what() );
        mexPrintf("---------------\n");

        cafe.printCAFEException_pv(e.pvEx);
        printStatus(e.pvEx.statusCode);
        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    catch(exception &e)
    {

        mexPrintf("---------------\n");
        mexPrintf("%s\n",   e.what() );
        mexPrintf("---------------\n");

        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    return vuint32;
}


unsigned  int open(char _pv[PVNAME_MAX_SIZE])
{

    uint32_T _handle=0;

    try
    {
        cafe.open(_pv, (unsigned  int&) _handle);
    }
    catch(CAFEException_open &e)
    {

        mexPrintf("---------------\n");
        cout << e.what() << endl;
        mexPrintf("---------------\n");

        cafe.printCAFEException_pv(e.pvEx);
        printStatus(e.pvEx.statusCode);
        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    catch(exception &e)
    {

        mexPrintf("---------------\n");
        cout << e.what() << endl;
        mexPrintf("---------------\n");

        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    return _handle;
}


unsigned  int groupOpen(char _pv[PVNAME_MAX_SIZE])
{
    uint32_T _ghandle=0;

    try
    {
        status=cafe.groupOpen((const char *) _pv, (unsigned  int &) _ghandle);
    }
    catch(CAFEException_groupOpen &e)
    {
        mexPrintf("---------------\n");
        cout << e.what() << endl;
        mexPrintf("---------------\n");
        NS_MOCHA::printStatus(e.groupEx.statusCode);
        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    catch(exception &e)
    {
        mexPrintf("---------------\n");
        cout << e.what() << endl;
        mexPrintf("---------------\n");
        mexPrintf("EXITING MATLAB\n");
        exit(1);
    }
    return _ghandle;
}



unsigned  int checkForHandleNoOpen(const mxArray * inputValue)
{

    unsigned  int _handle=0;

    if (mxIsNumeric(inputValue) )
    {
        _handle = (uint32_T) mxGetScalar(inputValue);
				 
    }

    else if (mxIsChar(inputValue) )
    {

        //message pvname
        xData = (mxArray *) inputValue;

        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, pv, pvLength);

        _handle=cafe.getHandleHelper().getHandleFromPV(pv);
        if (_handle==0)
        {
            mexPrintf("PV=%s has not yet been opened by user!\n", pv );
        }

    }
		
	
    else if (mxIsCell(inputValue) )
    {

        mxArray * newCell = mxGetCell(inputValue,0);

        if (mxIsNumeric(newCell) )
        {
            _handle = (uint32_T) mxGetScalar(newCell);
        }

        else if (mxIsChar(newCell) )
        {

            //message pvname
            xData = (mxArray *) newCell;

            pvLength = PVNAME_MAX_SIZE+1;
            mxGetString(xData, pv, pvLength);

            _handle=cafe.getHandleHelper().getHandleFromPV(pv);
            if (_handle==0)
            {
                mexPrintf("PV=%s has not yet been opened by user!\n", pv );
            }
        }
				
				else if (strcmp (mxGetClassName(newCell),"string") ==0 ) {
				    printStringInputError();
    		    return _handle;;
        }
				

    }
		else if (strcmp (mxGetClassName(inputValue),"string") ==0 ) {
			      printStringInputError();   
    		    return _handle;;
    }
		
		
		
    else
    {
        mexPrintf("Error in checkForHandleNoOpen:\n");
        mexPrintf("Handle does not have a numeric value \n");
        mexPrintf("or PV name does not have a char value \n" );
    }
    return _handle;
}



unsigned  int checkForHandle(const mxArray * inputValue)
{

    unsigned  int _handle=0;
		
		

    if (mxIsNumeric(inputValue) )
    {
        _handle = (uint32_T) mxGetScalar(inputValue);

    }

    else if (mxIsChar(inputValue) )
    {

        //message pvname
        xData = (mxArray *) inputValue;

        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, pv, pvLength);

        _handle=cafe.getHandleHelper().getHandleFromPV(pv);

        if (_handle==0)
        {
            _handle=open(pv);
        }
    }
    else if (mxIsCell(inputValue) )
    {

        mxArray * newCell = mxGetCell(inputValue,0);

        if (mxIsNumeric(newCell) )
        {
            _handle = (uint32_T) mxGetScalar(newCell);
        }

        else if (mxIsChar(newCell) )
        {

            //message pvname
            xData = (mxArray *) newCell;

            pvLength = PVNAME_MAX_SIZE+1;
            mxGetString(xData, pv, pvLength);

            _handle=cafe.getHandleHelper().getHandleFromPV(pv);

            if (_handle==0)
            {
                _handle=open(pv);
            }
        }
				 
        else if (strcmp (mxGetClassName(newCell),"string") ==0 ) {
						printStringInputError();
    		    return _handle;;
        }
				
    }
		
		 
    else if (strcmp (mxGetClassName(inputValue),"string") ==0 ) {
			  printStringInputError();
    		return _handle;;
    }
		
    else
    {
        mexPrintf("Error in checkForHandle:\n");
        mexPrintf("Handle does not have a numeric value \n");
        mexPrintf("or PV name does not have a char value \n");
        return _handle;
    }
    if (_handle<1)
    {
        mexPrintf("Handle in input argument not given! \n");
				mexPrintf("Handle should be a positive integer. \n");
				mexPrintf("Alternatively enter the pv name in single quotes\n");
    }
    return _handle;
}


uint32_T * scanNumericForHandle (const mxArray * inputValue)
{

    uint32_T * inputULong;

    if (mxIsNumeric(inputValue) )
    {

        size_t numberOfHandles=mxGetNumberOfElements (inputValue);
        mxArray * foutE = mxCreateNumericMatrix(1,numberOfHandles,mxUINT32_CLASS,  mxREAL);
        inputULong = (uint32_T *)  mxGetData (foutE);

        mxClassID mxid =mxGetClassID(inputValue);

        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr ( inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputDouble[i];
            }
            break;
        case mxSINGLE_CLASS:
            inputFloat =  (float *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputFloat[i];
            }
            break;
        case mxLOGICAL_CLASS:
            inputBool =   (bool *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputBool[i];
            }
            break;
        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputChar[i];
            }
            break;
        case mxUINT8_CLASS:
            inputUChar =  (uint8_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputUChar[i];
            }
            break;
        case mxINT16_CLASS:
            inputShort =  (int16_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputShort[i];
            }
            break;
        case mxUINT16_CLASS:
            inputUShort =  (uint16_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputUShort[i];
            }
            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputLong[i];
            }
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (inputValue);
            //for (size_t i=0; i< numberOfHandles; ++i) {
            //    inputULongLong[i] = (uint64_T) inputULong[i];
            //}
            break;
        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputLongLong[i];
            }
            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputULongLong[i];
            }
            break;
            // Series of Strings go in a CELL
        }
    }
    else
    {
        mexPrintf("scanNumericForHandle was not given a numeric for input!");
        mxArray * foutE = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,  mxREAL);
        inputULong = (uint32_T *)  mxGetData (foutE);
        inputULong[0] = 0;
    }
    return inputULong;
}

uint32_T * checkHandleArray(const mxArray * inputValue, bool openIfNoHandle)
{

    uint32_T * inputULong;
    mxArray * foutE;
    size_t numberOfHandles=0;

    if (mxIsNumeric(inputValue) )
    {

        numberOfHandles=mxGetNumberOfElements (inputValue);

        foutE = mxCreateNumericMatrix(1,std::max((int)numberOfHandles,1),mxUINT32_CLASS,  mxREAL);
        inputULong= (uint32_T *)  mxGetData (foutE);

        mxClassID mxid =mxGetClassID(inputValue);

        //mexPrintf("Class ID %d\n", mxid );
        //mexPrintf(mxGetClassName(inputValue));

        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr ( inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputDouble[i];
            }
            break;
        case mxSINGLE_CLASS:
            inputFloat =  (float *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputFloat[i];
            }
            break;
        case mxLOGICAL_CLASS:
            inputBool =   (bool *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputBool[i];
            }
            break;
        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputChar[i];
            }
            break;
        case mxUINT8_CLASS:
            inputUChar =  (uint8_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputUChar[i];
            }
            break;
        case mxINT16_CLASS:
            inputShort =  (int16_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputShort[i];
            }
            break;
        case mxUINT16_CLASS:
            inputUShort =  (uint16_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputUShort[i];
            }
            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputLong[i];
            }
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (inputValue);

            break;
        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputLongLong[i];
            }
            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (inputValue);
            for (size_t i=0; i< numberOfHandles; ++i)
            {
                inputULong[i] = (uint32_T) inputULongLong[i];
            }
            break;
            // Series of Strings go in a CELL
        }

        //for (size_t i=0; i<numberOfHandles; ++i) {
        // mexPrintf("value=%d, %d of %d // \n" , inputULong[i], i, numberOfHandles);
        //}


    }


    else if (mxIsChar(inputValue) )
    {

        foutE = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,  mxREAL);
        inputULong = (uint32_T *)  mxGetData (foutE);

        if(openIfNoHandle)
        {
            handle=NS_MOCHA::checkForHandle(inputValue);
        }
        else
        {
            handle=NS_MOCHA::checkForHandleNoOpen(inputValue);
        }
        inputULong[0] = (uint32_T) handle;
    }

    else if (mxIsCell(inputValue) )
    {

        //Is it a string?

        neWithinAllCells=0;

        scanCell(inputValue);

        //if(neWithinAllCells==0) neWithinAllCells=1;

        foutE = mxCreateNumericMatrix(1,max(1,(int) neWithinAllCells),mxUINT32_CLASS,  mxREAL);
        inputULong = (uint32_T *)  mxGetData (foutE);

        mxArray * newCell = mxGetCell(inputValue,0);

        if (mxIsNumeric(newCell) )
        {
            size_t ne = mxGetNumberOfElements (newCell);
            //mexPrintf( " %d // %d ",neWithinAllCells, ne);

            if(ne != neWithinAllCells)
            {
                mexPrintf("Error: Can't deal with this cell' \n", inputValue );

                inputULong[0]=0;
                return inputULong;
            }

            inputULong=scanNumericForHandle(newCell);

        }

        else
        {



            inputPVStringCell = new pv_string_t[max(1,(int) neWithinAllCells)];
            iWithinAllCells=0; //Required before call to cellToPVString
            cellToPVString(inputValue);




            bool setFlushFlag=false;


            for (mwSignedIndex i=0; i<neWithinAllCells; ++i)
            {

                //If string is a number it is a handle

                handle=cafe.getHandleHelper().getHandleFromPV(inputPVStringCell[i]);
                if (handle==0)
                {
                    if (openIfNoHandle)
                    {
                        //Set channelOpenPolicy to enable single flush of open channels
                        if (!setFlushFlag)
                        {
                            cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_DESIGNATED_TO_CLIENT);
                            cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);
                            setFlushFlag=true;
                        }

                        handle=open(inputPVStringCell[i]);
                    }
                }

                inputULong[i] = (uint32_T) handle;
            }

            if (setFlushFlag)
            {
                cafe.channelOpenPolicy.flushSendBufferNow();
            }

            delete [] inputPVStringCell;
        }

    }
    else
    {
        mexPrintf("Error in checkHandleArray: \n");
        mexPrintf("Handle does not have a numeric value \n");
        mexPrintf("or PV name does not have a char value \n" );
        foutE = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,  mxREAL);
        inputULong = (uint32_T *)  mxGetData (foutE);
        inputULong[0]=0;
    }



    return inputULong;
}




unsigned  int checkForGroupHandle(const mxArray * inputValue)
{

    unsigned  int _handle=0;

    if (mxIsNumeric(inputValue) )
    {
        _handle = (uint32_T) mxGetScalar(inputValue);
    }

    else if (mxIsChar(inputValue) )
    {

        //message pvname
        xData = (mxArray *) inputValue;

        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, pv, pvLength);

        _handle=cafe.getHandleHelper().getGroupHandleFromGroupName(pv); //pv is group name here

        if (_handle==0)
        {
            _handle=groupOpen(pv);
        }
    }
    else if (mxIsCell(inputValue) )
    {

        mxArray * newCell = mxGetCell(inputValue,0);

        if (mxIsNumeric(newCell) )
        {
            _handle = (uint32_T) mxGetScalar(newCell);
        }

        else if (mxIsChar(newCell) )
        {

            //message pvname
            xData = (mxArray *) newCell;

            pvLength = PVNAME_MAX_SIZE+1;
            mxGetString(xData, pv, pvLength);

            _handle=cafe.getHandleHelper().getHandleFromPV(pv);

            if (_handle==0)
            {
                _handle=groupOpen(pv);
            }
        }

    }
    else
    {
        mexPrintf("Error in checkForGroupHandle: \n");
        mexPrintf("Handle does not have a numeric value \n");
        mexPrintf("or group name does not have a char value \n");
    }
    return _handle;
}

unsigned  int checkForGroupHandleNoOpen(const mxArray * inputValue)
{

    unsigned  int _handle=0;

    if (mxIsNumeric(inputValue) )
    {
        _handle = (uint32_T) mxGetScalar(inputValue);
    }

    else if (mxIsChar(inputValue) )
    {

        //message pvname
        xData = (mxArray *) inputValue;

        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, pv, pvLength);

        _handle=cafe.getHandleHelper().getGroupHandleFromGroupName(pv); //pv is group name here


    }
    else if (mxIsCell(inputValue) )
    {

        mxArray * newCell = mxGetCell(inputValue,0);

        if (mxIsNumeric(newCell) )
        {
            _handle = (uint32_T) mxGetScalar(newCell);
        }

        else if (mxIsChar(newCell) )
        {

            //message pvname
            xData = (mxArray *) newCell;

            pvLength = PVNAME_MAX_SIZE+1;
            mxGetString(xData, pv, pvLength);

            _handle=cafe.getHandleHelper().getHandleFromPV(pv);

        }

    }
    else
    {
        mexPrintf("Error in checkForGroupHandleNoOpen: \n");
        mexPrintf("Error: Group handle does not have a numeric value \n");
        mexPrintf("or group name does not have a char value \n");
    }

    return _handle;
}



mxArray * getValueIfError(unsigned  int mdtIndex)
{

    mxArray * foutE;

    switch ( mdtIndex) //du.getDataType())
    {
    case MOCHA_DOUBLE:

        foutE =   mxCreateDoubleScalar(numeric_limits<double>::quiet_NaN());

        //foutE = mxCreateDoubleMatrix(1,1,mxREAL);
        //doubleArray = (double *) mxGetPr ( plhs[0]);
        //doubleArray[0]=numeric_limits<double>::signaling_NaN();
        break;
    case MOCHA_FLOAT:
        foutE = mxCreateNumericMatrix(1,1,mxSINGLE_CLASS,  mxREAL);
        floatArray = (float *) mxGetData (foutE);
        floatArray[0]=numeric_limits<float>::quiet_NaN();
        break;
    case MOCHA_INT:
        foutE = mxCreateNumericMatrix(1,1,mxINT32_CLASS,  mxREAL);
        longArray = (int32_T *) mxGetData (foutE);
        longArray[0]=numeric_limits<int32_T>::quiet_NaN();
        break;
    case MOCHA_SHORT:
        foutE = mxCreateNumericMatrix(1,1,mxINT16_CLASS,  mxREAL);
        shortArray = (int16_T *)  mxGetData (foutE);
        shortArray[0]=numeric_limits<int16_T>::quiet_NaN();
        break;
    case MOCHA_USHORT:
        foutE = mxCreateNumericMatrix(1,1,mxUINT16_CLASS,  mxREAL);
        ushortArray = (uint16_T *)  mxGetData (foutE);
        ushortArray[0]=numeric_limits<uint16_T>::quiet_NaN();
        break;
    case MOCHA_UCHAR:
        foutE = mxCreateNumericMatrix(1,1,mxUINT8_CLASS,  mxREAL);
        ucharArray = (uint8_T *)  mxGetData (foutE);
        ucharArray[0]=numeric_limits<uint8_T>::quiet_NaN();
        break;
    case MOCHA_STRING:
        foutE = mxCreateCellMatrix(1,1);
        mxSetCell (foutE, 0, mxCreateString( numeric_limits<char *>::quiet_NaN() ));
        break;
    case MOCHA_CHAR:
        foutE = mxCreateNumericMatrix(1,1,mxINT8_CLASS,  mxREAL);
        charArray = (int8_T *)  mxGetData (foutE);
        charArray[0]= numeric_limits<int8_T>::quiet_NaN();
        break;
    case MOCHA_UINT:
        foutE = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,  mxREAL);
        ulongArray = (uint32_T *)  mxGetData (foutE);
        ulongArray[0]= numeric_limits<uint32_T>::quiet_NaN();
        break;
    case MOCHA_LONG:
        foutE = mxCreateNumericMatrix(1,1,mxINT64_CLASS,  mxREAL);
        longlongArray = (int64_T *)  mxGetData (foutE);
        longlongArray[0]= numeric_limits<int64_T>::quiet_NaN();
        break;
    case MOCHA_ULONG:
        foutE = mxCreateNumericMatrix(1,1,mxUINT64_CLASS,  mxREAL);
        ulonglongArray = (uint64_T *)  mxGetData (foutE);
        ulonglongArray[0]= numeric_limits<uint64_T>::quiet_NaN();
        break;
    default:
        foutE =   mxCreateDoubleScalar(numeric_limits<double>::quiet_NaN());
        break;
    }

    return foutE;
}


void fillPVCell(unsigned  int mdtIndex, PVDataHolder dh)
{

    nelem= dh.getNelem();

    switch ( mdtIndex)
    {
    case MOCHA_DOUBLE:
        fout0 = mxCreateDoubleMatrix(1,nelem,mxREAL);
        doubleArray = (double *) mxGetPr (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            doubleArray[i]=dh.getAsDouble(i);
        }
        break;

    case MOCHA_FLOAT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxSINGLE_CLASS,  mxREAL);
        floatArray = (float *) mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            floatArray[i]=dh.getAsFloat(i);
        }
        break;

    case MOCHA_INT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT32_CLASS,  mxREAL);
        longArray = (int32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longArray[i]=dh.getAsLong(i);
        }
        break;

    case MOCHA_SHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
        shortArray = (int16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            shortArray[i]=dh.getAsShort(i);
        }
        break;

    case MOCHA_USHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT16_CLASS,  mxREAL);
        ushortArray = (uint16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ushortArray[i]=(unsigned short) dh.getAsEnum(i);
        }
        break;

    case MOCHA_UCHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT8_CLASS,  mxREAL);
        ucharArray = (uint8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ucharArray[i]= (unsigned short) dh.getAsChar(i);
        }
        break;

    case MOCHA_STRING:
    {
        //cells are best


        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }

        //for (mwSignedIndex i=0; i <nelem; ++i) {
        /////mxSetCell (fout0, i, mxCreateString( *dh.getAsDbr_string_t(i)));
        //mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str()));
        //}
        break;
    }
// Non DBR_TYPES
    case MOCHA_CHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
        charArray = (int8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            charArray[i]= (int8_T) dh.getAsShort(i);
        }
        break;
    case MOCHA_UINT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT32_CLASS,  mxREAL);
        ulongArray = (uint32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulongArray[i]= (uint32_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_LONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT64_CLASS,  mxREAL);
        longlongArray = (int64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longlongArray[i]= (int64_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_ULONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT64_CLASS,  mxREAL);
        ulonglongArray = (uint64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulonglongArray[i]= (uint64_T) dh.getAsDouble(i);
        }
        break;
    default:


        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }

        break;
    } //switch





}


void fillPVStructExtra(unsigned  int handle, PVDataHolder dh)
{


    string s = cafe.getPVFromHandle(handle);
    fout5 =mxCreateString( s.c_str() );
    s =  cgac.asString( dh.getAlarmStatus() );
    fout7 =mxCreateString( s.c_str() );
    s =  cgas.asString( dh.getAlarmSeverity() );
    fout8 =mxCreateString( s.c_str() );
    s =  cafe.getCafeStatus().asString(dh.getStatus());
    fout6 =mxCreateString( s.c_str() );


    fout9 = mxCreateNumericMatrix(1,2,mxUINT32_CLASS,mxREAL);
    d9    = (uint32_T *) mxGetData(fout9);

    fout10=mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
    fHandle    = (uint32_T *) mxGetData(fout10);
    fHandle[0]=handle;


    d9[0]=dh.getEpicsTimeStamp().secPastEpoch;
    d9[1]=dh.getEpicsTimeStamp().nsec;
}


void fillPVStruct(unsigned  int mdtIndex, PVDataHolder dh)
{

    nelem= dh.getNelem();

    fout1 = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
    longStatus = (int32_T *) mxGetData (fout1);

    fout2 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
    shortAlarmStatus = (int16_T *) mxGetData (fout2);

    fout3 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
    shortAlarmSeverity = (int16_T *) mxGetData (fout3);

    fout4 = mxCreateNumericMatrix(1,7,mxUINT32_CLASS,mxREAL);
    d4    = (uint32_T *) mxGetData(fout4);

    switch ( mdtIndex)
    {
    case MOCHA_DOUBLE:
        fout0 = mxCreateDoubleMatrix(1,nelem,mxREAL);
        doubleArray = (double *) mxGetPr (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            doubleArray[i]=dh.getAsDouble(i);
        }
        break;

    case MOCHA_FLOAT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxSINGLE_CLASS,  mxREAL);
        floatArray = (float *) mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            floatArray[i]=dh.getAsFloat(i);
        }
        break;

    case MOCHA_INT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT32_CLASS,  mxREAL);
        longArray = (int32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longArray[i]=dh.getAsLong(i);
        }
        break;

    case MOCHA_SHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
        shortArray = (int16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            shortArray[i]=dh.getAsShort(i);
        }
        break;

    case MOCHA_USHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT16_CLASS,  mxREAL);
        ushortArray = (uint16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ushortArray[i]=(unsigned short) dh.getAsEnum(i);
        }
        break;

    case MOCHA_UCHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT8_CLASS,  mxREAL);
        ucharArray = (uint8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ucharArray[i]= (unsigned short) dh.getAsChar(i);
        }
        break;

    case MOCHA_STRING:
    {
        //cells are best


        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }

        //for (mwSignedIndex i=0; i <nelem; ++i) {
        /////mxSetCell (fout0, i, mxCreateString( *dh.getAsDbr_string_t(i)));
        //mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str()));
        //}
        break;
    }
// Non DBR_TYPES
    case MOCHA_CHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
        charArray = (int8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            charArray[i]= (int8_T) dh.getAsShort(i);
        }
        break;
    case MOCHA_UINT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT32_CLASS,  mxREAL);
        ulongArray = (uint32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulongArray[i]= (uint32_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_LONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT64_CLASS,  mxREAL);
        longlongArray = (int64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longlongArray[i]= (int64_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_ULONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT64_CLASS,  mxREAL);
        ulonglongArray = (uint64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulonglongArray[i]= (uint64_T) dh.getAsDouble(i);
        }
        break;
    default:

        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }

        //for (mwSignedIndex i=0; i <nelem; ++i) {
        ////mxSetCell (fout0, i, mxCreateString( *dh.getAsDbr_string_t(i)));
        //mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str()));
        //}
        break;
    } //switch


    ets1.secPastEpoch=dh.getEpicsTimeStamp().secPastEpoch;
    ets1.nsec=dh.getEpicsTimeStamp().nsec;

    epicsTime time(ets1);

    local_tm_nano_sec local = (local_tm_nano_sec) time;

    d4[0] = local.ansi_tm.tm_year + 1900;
    d4[1] = local.ansi_tm.tm_mon  + 1;
    d4[2] = local.ansi_tm.tm_mday;
    d4[3] = local.ansi_tm.tm_hour;
    d4[4] = local.ansi_tm.tm_min;
    d4[5] = local.ansi_tm.tm_sec;
    d4[6] = (uint32_T) local.nSec;

    longStatus[0] = dh.getStatus();
    shortAlarmStatus[0] = dh.getAlarmStatus();
    shortAlarmSeverity[0] = dh.getAlarmSeverity();

}


void fillCtrlStruct(unsigned  int mdtIndex, PVCtrlHolder dh)
{
    nelem= dh.getNelem();
    switch ( mdtIndex)
    {
    case MOCHA_DOUBLE:
        fout0 = mxCreateDoubleMatrix(1,nelem,mxREAL);
        doubleArray = (double *) mxGetPr (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            doubleArray[i]=dh.getAsDouble(i);
        }
        break;
    case MOCHA_FLOAT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxSINGLE_CLASS,  mxREAL);
        floatArray = (float *) mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            floatArray[i]=dh.getAsFloat(i);
        }
        break;
    case MOCHA_INT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT32_CLASS,  mxREAL);
        longArray = (int32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longArray[i]=dh.getAsLong(i);
        }
        break;
    case MOCHA_SHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
        shortArray = (int16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            shortArray[i]=dh.getAsShort(i);
        }
        break;
    case MOCHA_USHORT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT16_CLASS,  mxREAL);
        ushortArray = (uint16_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ushortArray[i]=(unsigned short) dh.getAsEnum(i);
        }
        break;
    case MOCHA_UCHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT8_CLASS,  mxREAL);
        ucharArray = (uint8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ucharArray[i]= (unsigned short) dh.getAsChar(i);
        }
        break;
    case MOCHA_STRING:
    {

        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            //cells are best
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }


        //for (mwSignedIndex i=0; i <nelem; ++i) {
        ////mxSetCell (fout0, i, mxCreateString( *dh.getAsDbr_string_t(i)));
        //mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str()));
        //}
        break;
    }
// Non DBR_TYPES
    case MOCHA_CHAR:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
        charArray = (int8_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            charArray[i]= (int8_T) dh.getAsShort(i);
        }
        break;
    case MOCHA_UINT:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT32_CLASS,  mxREAL);
        ulongArray = (uint32_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulongArray[i]= (uint32_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_LONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxINT64_CLASS,  mxREAL);
        longlongArray = (int64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            longlongArray[i]= (int64_T) dh.getAsDouble(i);
        }
        break;
    case MOCHA_ULONG:
        fout0 = mxCreateNumericMatrix(1,nelem,mxUINT64_CLASS,  mxREAL);
        ulonglongArray = (uint64_T *)  mxGetData (fout0);
        for (mwSignedIndex i=0; i< nelem; ++i)
        {
            ulonglongArray[i]= (uint64_T) dh.getAsDouble(i);
        }
        break;
    default:

        if (nelem==1)
        {
            fout0=mxCreateString(dh.getAsString(0).c_str() );
        }
        else
        {
            //cells are best
            fout0 = mxCreateCellMatrix(1,nelem);
            for (mwSignedIndex i=0; i <nelem; ++i)
            {
                mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str() ));
            }
        }

        //for (mwSignedIndex i=0; i <nelem; ++i) {
        ////mxSetCell (fout0, i, mxCreateString( *dh.getAsDbr_string_t(i)));
        //mxSetCell (fout0, i, mxCreateString( dh.getAsString(i).c_str()));
        //}
        break;
    } //switch


    longStatus[0]=dh.getStatus();
    shortAlarmStatus[0]=dh.getAlarmStatus();
    shortAlarmSeverity[0]=dh.getAlarmSeverity();

    //status_str, alarmStatus_str, alarmSeverity_str
    string s =  cgac.asString( dh.getAlarmStatus() );
    fout2a =mxCreateString( s.c_str() );
    s =  cgas.asString( dh.getAlarmSeverity() );
    fout3a =mxCreateString( s.c_str() );
    s =  cafe.getCafeStatus().asString(dh.getStatus());
    fout1a =mxCreateString( s.c_str() );


    precision[0]=dh.getPrecision();
    noEnumStrings[0]=dh.getNoEnumStrings();
    fout7 = mxCreateCellMatrix(1,noEnumStrings[0]);
    for (mwSignedIndex i=0; i <noEnumStrings[0]; ++i)
        mxSetCell (fout7, i, mxCreateString((const char *) dh.getEnumString(i) ) );
    udl[0]=dh.getUpperDispLimit_AsDouble();
    ldl[0]=dh.getLowerDispLimit_AsDouble();
    ual[0]=dh.getUpperAlarmLimit_AsDouble();
    lal[0]=dh.getLowerAlarmLimit_AsDouble();
    uwl[0]=dh.getUpperWarningLimit_AsDouble();
    lwl[0]=dh.getLowerWarningLimit_AsDouble();
    ucl[0]=dh.getUpperCtrlLimit_AsDouble();
    lcl[0]=dh.getLowerCtrlLimit_AsDouble();
}


} //NS_MOCHA

