///
/// \file   macchinettaSwitch.cpp
///
/// \author Jan Chrin, PSI
/// \date   May 2014
///

#include "macchinettaHelper.h"

//#ifdef HAVE_PYTHON_H
//#include <dlfcn.h>
//#include <Python.h>
//#endif

extern void _main();
using namespace NS_MOCHA;

void macchinettaFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    if (nrhs <1)
    {
        mexPrintf("Input argument not entered. \n");
        mexPrintf("The command: mocha \'show\' will display the methods available. \n");
        return;
    }

    if (mxIsCell(prhs[0]))
    {
        mexPrintf("mocha message argument (1st input arg) must be a char and not a cell. \n");
        return;
    }

    int32_T mochaIndex=-1;

    if ( mxIsNumeric(prhs[0]) && !mxIsChar(prhs[0]) )
    {
        mochaIndex= (int) mxGetScalar( prhs[0] );
        if (mochaIndex !=MONITOR_FLUSH_EVENT)
        {
            mexPrintf("We prefer not to expose the numerical index value directly. \n");
            mexPrintf("Please use the method name (MATLAB char) as input to the first argument. \n");
            mexPrintf("The command: mocha \'show\' will display the methods available. \n");

            //mexPrintf("1st input argument given has Class Name: %s\n", mxGetClassName(prhs[0]));
            //mexPrintf("1st input argument given has Class ID:   %d\n", mxGetClassID(prhs[0]));
            //mexPrintf("1st input argument to mocha must be of type char/mxCHAR_CLASS\n");

            return;
        }
    }
           
    
    if (strcmp (mxGetClassName(prhs[0]),"string") ==0 ) {


    		mexPrintf ("MATLAB mx library does not support string/mxSTRING_CLASS as input\n");
    		mexPrintf ("Please use char/mxCHAR_CLASS as data type instead\n");
		mexPrintf("i.e., use single quotes instead of double quotes in input argument!\n");
    		return;
   }
    //message

    //Trick to handle mocha(402) input from monitor Action

    messLength = METHOD_NAME_MAX_SIZE+1;
    if (mxIsChar(prhs[0]) )
    {
        mData = (mxArray *) prhs[0];
        mxGetString(mData, mess, messLength);
    }
		else if (!strcmp(mxGetClassName(prhs[0]), "string") )
		{			
		
       mexPrintf("1st input argument given has Class Name: %s\n", mxGetClassName(prhs[0]));
       mexPrintf("1st input argument given has Class ID:   %d\n", mxGetClassID(prhs[0]));
       mexPrintf("1st input argument to mocha must be of type char/mxCHAR_CLASS\n");
       mexPrintf("Please use single quotes instead of double quotes in input argument!\n");
				
		   return;
		}
    else if (mochaIndex ==MONITOR_FLUSH_EVENT)
    {
        strcpy(mess,"monitorflushevent");
    }
	  else
		{	    
        mexPrintf("1st input argument given has Class Name: %s\n", mxGetClassName(prhs[0]));
        mexPrintf("1st input argument given has Class ID:   %d\n", mxGetClassID(prhs[0]));
				mexPrintf("1st input argument to mocha must be of type char/mxCHAR_CLASS\n");
		    return;
		}

   

    char messOriginal[METHOD_NAME_MAX_SIZE+1];
    strcpy(messOriginal,mess);

    //mexPrintf("message mess:%s\n", mess);
    //mexPrintf("message orig:%s\n", messOriginal);

    //remove leadingTrailing Blanks
    const char * _mess=mess;
    cafe.getHandleHelper().removeLeadingAndTrailingSpaces(_mess,mess);

    std::string messString = mess;

    std::transform(messString.begin(), messString.end(), messString.begin(), ::tolower);
    strcpy(mess,messString.c_str());


    if(!isCAInitialized  && strcmp(mess,"monitorflushevent")!=0 && strcmp(mess,"close")!=0)
    {
        NS_MOCHA::msInsert();
        NS_MOCHA::mdtInsert();

        // print_out_by<id>(ms);
    }
    else if (!isCAInitialized  && strcmp(mess,"close")==0)
    {
        mexPrintf("The CAFEteria was never opened! \n");
        return;
    }

    if(!isCAInitialized && strcmp(mess,"close")!=0)
    {


        if (!mexIsLocked())
        {
            mexLock();
        }

        //Remove PyCafe component should cafe have been compiled with pyCafe
        //This flag  controls import_PyCafe for when PyCafe_api.h is used
        //If PyCafe.h is not used then you must link to non-Py cafe C++ librarry


        //if ( cafe.setPyCafe(true) ) {
        //	mexPrintf("Py_Initialize \n");
        //	Py_Initialize();
        //}

#ifdef HAVE_PYTHON_H
        //cafe.setPyCafe(true);
        //dlopen("libpython2.6.so", RTLD_GLOBAL|RTLD_LAZY);
        //dlopen("libpython3.5m.so", RTLD_GLOBAL|RTLD_LAZY);
        mexPrintf("HAVE_PYTHON_H \n");
        //Py_Initialize();
        //mexPrintf("Py_Initialize from MATLAB \n");
#endif

        cafe.init();

        //#ifdef HAVE_PYTHON_H
        //  cafe.loadSFGroups("VA");
        //#endif

        isCAInitialized=true;

        if (strcmp(mess,"monitorflushevent")==0 )
        {
            mexPrintf("The CAFEteria is now open due to an outstanding monitorFlushEvent action (probably from a timer) \n");
        }
        // else {
        //mexPrintf("The CAFEteria is now open \n");
        //	}
    }


    mochaccino_set_by_name & name_index = ms.get<name> ();
    mochaccino_set_by_name::iterator it_name;


    //mexPrintf("message %s\n", mess);
    //mexPrintf("message %s\n", messString.c_str());
	

    it_name = name_index.find(mess);

    if ( (it_name) != name_index.end() )
    {
        mochaIndex=(*it_name).id;
    }

    int32_T  mdtIndex=MOCHA_NATIVE;
    int32_T  mochaIndexOriginal=mochaIndex;

    bool cacheFlag=false;
    bool sgTimeoutFlag=false;
    bool extendedPV=false;

    size_t numberOfHandlesGetAsyn=0;
    size_t nh=0;
    size_t nv=0;

    //Wrapper methods
    switch(mochaIndex)
    {
    case GET_INT8:
        mochaIndex=GET;
        mdtIndex=MOCHA_CHAR;
        break;
    case GET_INT16:
        mochaIndex=GET;
        mdtIndex=MOCHA_SHORT;
        break;
    case GET_INT32:
        mochaIndex=GET;
        mdtIndex=MOCHA_INT;
        break;
    case GET_INT64:
        mochaIndex=GET;
        mdtIndex=MOCHA_LONG;
        break;
    case GET_UINT8:
        mochaIndex=GET;
        mdtIndex=MOCHA_UCHAR;
        break;
    case GET_UINT16:
        mochaIndex=GET;
        mdtIndex=MOCHA_USHORT;
        break;
    case GET_UINT32:
        mochaIndex=GET;
        mdtIndex=MOCHA_UINT;
        break;
    case GET_UINT64:
        mochaIndex=GET;
        mdtIndex=MOCHA_ULONG;
        break;
    case GET_FLOAT:
        mochaIndex=GET;
        mdtIndex=MOCHA_FLOAT;
        break;
    case GET_DOUBLE:
        mochaIndex=GET;
        mdtIndex=MOCHA_DOUBLE;
        break;
    case GET_STRING:
        mochaIndex=GET;
        mdtIndex=MOCHA_STRING;
        break;
    case GET_CELL_ARRAY_CACHE:
        mochaIndex=GET_CELL_ARRAY;
        cacheFlag=true;
        break;
    case GET_SCALAR_ARRAY_CACHE:
        mochaIndex=GET_SCALAR_ARRAY;
        cacheFlag=true;
        break;
    case GET_STRUCT_ARRAY_CACHE:
        mochaIndex=GET_STRUCT_ARRAY;
        cacheFlag=true;
        break;
    case GET_STRUCT_CACHE:
        mochaIndex=GET_STRUCT;
        cacheFlag=true;
        break;
    case GET_GROUP_CACHE:
        mochaIndex=GET_GROUP;
        cacheFlag=true;
        break;


    case GET_STRUCT_EXTENDED:
        mochaIndex=GET_STRUCT;
        extendedPV=true;
        break;

    case GET_STRUCT_EXTENDED_ARRAY:
        mochaIndex=GET_STRUCT_ARRAY;
        extendedPV=true;
        break;

    case GET_GROUP_EXTENDED:
        mochaIndex=GET_GROUP;
        extendedPV=true;
        break;

    case GET_STRUCT_EXTENDED_ARRAY_CACHE:
        mochaIndex=GET_STRUCT_ARRAY;
        cacheFlag=true;
        extendedPV=true;
        break;
    case GET_STRUCT_EXTENDED_CACHE:
        mochaIndex=GET_STRUCT;
        cacheFlag=true;
        extendedPV=true;
        break;
    case GET_GROUP_EXTENDED_CACHE:
        mochaIndex=GET_GROUP;
        cacheFlag=true;
        extendedPV=true;
        break;

    case GET_WF_AS_STRING_CACHE:
        mochaIndex=GET_WF_AS_STRING;
        cacheFlag=true;
        break;

    case GET_CACHE:
        mochaIndex=GET;
        cacheFlag=true;
        break;
    case GET_CTRL_CACHE:
        mochaIndex=GET_CTRL_STRUCT;
        cacheFlag=true;
        break;
    case GET_NELEM_CACHE:
        mochaIndex=GET_NELEM_REQUEST;
        cacheFlag=true;
        break;
    case GET_ASYN:
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }
        if (mxIsChar(prhs[1]) )
        {
            numberOfHandlesGetAsyn=1;
        }
        else
        {
            numberOfHandlesGetAsyn=mxGetNumberOfElements (prhs[1]);
        }
        if (numberOfHandlesGetAsyn >1)
        {
            mochaIndex=GET_ARRAY_ASYN;
        }
        break;

    case SET:

        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'set', handle/pv, data \n", nrhs);
            return;
        }

        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }
        if (mxIsChar(prhs[2]))
        {
            nv=1;
        }
        else
        {
            nv=mxGetNumberOfElements (prhs[2]);
        }

        ///mexPrintf("nh=%d, nvalues=%d, nargin=%d\n", nh,nv, nrhs);
        if (nh>1 && nh==nv && nrhs==3)
        {
            mochaIndex=SET_SCALAR_ARRAY;
            //mexPrintf("SET_SCALAR_ARRAY\n");
        }
        else if (nh>1 && nh==(nrhs-2))
        {
            mochaIndex=SET_STRUCT_ARRAY;
            //mexPrintf("SET_STRUCT_ARRAY\n");
        }
        else if (nh>1)
        {
            mochaIndex=SET_STRUCT_ARRAY;
            mexPrintf("Inbalance between number of handles and input data\n");
            //mexPrintf("SET_STRUCT_ARRAY\n");
        }

        break;

    case SET_NELEM_CACHE:
        mochaIndex=SET_NELEM;
        cacheFlag=true;
        break;

    case OPEN:
        if (nrhs>1)
        {
            if( mxIsCell(prhs[1]) )
            {
                mochaIndex=OPEN_ARRAY;
            }
        }
        break;

    case CLOSE:
        if (nrhs>1)
        {
            if( mxGetNumberOfElements (prhs[1]) >1 )
            {
                mochaIndex=CLOSE_ARRAY;
            }
        }
        break;

    case OPEN_WAIT_WITH_TIME:
        if (nrhs<2)
        {
            mochaIndex=OPEN_WAIT_TIME_TO_DEFAULT;
            //cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_CHANNEL_CREATION);
            //cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);
        }
        break;

    default:
        break;
    }


    //Test again for GET
    //Wrapper methods
    switch(mochaIndex)
    {
    case GET:

        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }

        ///mexPrintf("nh=%d, nargin=%d\n", nh, nrhs);
        if (nrhs>3)
        {
            mexPrintf("Usage: get [pv] <datatype>(optional)\n");
            return;
        }
        if (nh>1)
        {
            mochaIndex=GET_SCALAR_ARRAY;
            //mexPrintf("GET_SCALAR_ARRAY\n");
            if(nrhs==3 &&mxIsChar(prhs[nrhs-1]))
            {
                mdtIndex=NS_MOCHA::getMdtIndex(prhs[nrhs-1]);
            }
            //else {
            //mexPrintf("3rd input argument giving the <datatype> must be a string\n");
            //}

            //mexPrintf("GET_SCALAR_ARRAY\n");
        }
        break;

    case GET_STRUCT:
        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }

        ///mexPrintf("nh=%d, nargin=%d\n", nh, nrhs);
        if (nrhs>3)
        {
            mexPrintf("Usage: get [pv] <datatype>(optional)\n");
            return;
        }
        if (nh>1)
        {
            mochaIndex=GET_STRUCT_ARRAY;
            //mexPrintf("GET_STRUCT_ARRAY\n");
            if(nrhs==3 &&mxIsChar(prhs[nrhs-1]))
            {
                mdtIndex=NS_MOCHA::getMdtIndex(prhs[nrhs-1]);
            }
            //else {
            //mexPrintf("3rd input argument giving the <datatype> must be a string\n");
            //}
        }

        break;
    }

    // The Big Switch
    switch (mochaIndex)
    {

    case TEST:
        mexPrintf("mocha mex file is active\n");
        break;

    case EPICS_V_STRING:
        plhs[0]=mxCreateString((char *) (cafe._epics_version()).c_str());
        break;
 
    case CA_V_STRING:
        plhs[0]=mxCreateString((char *) (cafe._ca_version()).c_str());
        break;

    case MOCHA_VERSION:
        plhs[0]=mxCreateString((char *) "mocha-1.21.0 : 12 July 2024; Contact: Felix Armborst");
        break;

    case SHOW:
        //print_out_by<id>(ms);
        for (it_name=name_index.begin(); it_name != name_index.end(); ++it_name)
        {
            mexPrintf("%s\n", ((*it_name).name).c_str());
        }
        break;

    case CA_POLL:
        cafe._ca_poll();
        break;

    case MEX_UNLOCK:

        if (!mexIsLocked())
        {
            mexUnlock();
        }
        break;

    case SET_PUT_PREPARE:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument(s) %d. Input: setPutPrepare, handle(s) or handleArray \n", nrhs);
            return;
        }

        size_t numberOfHandles=0;

        for (size_t j=1; j<nrhs; ++j)
        {

            numberOfHandles=mxGetNumberOfElements (prhs[j]);
            neWithinAllCells=0; // filled by checkHandleArray

            //What is dataType?
            handleULong = checkHandleArray(prhs[j], true); //do/do not open handle

            if (handleULong==NULL)
            {
                mexPrintf("SET_PUT_PREPARE: Ignoring NULL value in input %d of %d\n", (j+1), nrhs);
                continue;
            }

            if (mxIsChar(prhs[j]) )
            {
                numberOfHandles=1;
                //mexPrintf("char numberOfHandles=%d \n",numberOfHandles);
            }
            else if (mxIsCell(prhs[j]) )
            {
                numberOfHandles=neWithinAllCells;
                //mexPrintf("cell numberOfHandles=%d \n",numberOfHandles);
            }
            if(handleULong[0]==0)
            {
                mexPrintf("SET_PUT_PREPARE: Ignoring input argument %d of %d \n", (j+1),nrhs);
                continue;
            }

            ChannelRequestPolicy polput;

            for (size_t i=0; i< numberOfHandles; ++i)
            {

                status=cafe.getPolicy().getChannelRequestPolicyPut(handleULong[i], polput);


                if (status==ECAFE_INVALID_HANDLE)
                {
                    mexPrintf("SET_PUT_PREPARE: Invalid handle no = %d \n", handleULong[i]);
                    continue;
                }

                //mexPrintf("no wait for handle no = %d\n", handleULong[i]);
                polput.setMethodKind(WITH_CALLBACK_DEFAULT);
                polput.setWaitKind(NO_WAIT);
                polput.setWhenToFlushSendBuffer(FLUSH_DESIGNATED_TO_CLIENT);
                cafe.getPolicy().setChannelRequestPolicyPut(handleULong[i], polput);
            }
        }

        break;
    }
    case SET_PUT_WAIT:
    {
        size_t numberOfHandles=0;
        ChannelRequestPolicy polput;

        if (nrhs<2)
        {
            //mexPrintf("Invalid Number of argument(s) %d. Input: setNoWait, handle(s) or handleArray \n", nrhs);
            //return;
            vector<unsigned int> dhV;
            vector<string> pvV;
            cafe.getHandleHelper().getHandles(dhV,pvV);
            numberOfHandles=dhV.size();

            for (size_t i=0; i< numberOfHandles; ++i)
            {

                status=cafe.getPolicy().getChannelRequestPolicyPut(dhV[i], polput);
                if (status==ECAFE_INVALID_HANDLE)
                {
                    mexPrintf("SET_PUT_WAIT: Invalid handle no = %d \n", dhV[i]);
                    continue;
                }

                //mexPrintf("no wait for handle no = %d\n", handleULong[i]);
                //mexPrintf("wait for handle no = %d\n", handleULong[i]);
                polput.setMethodKind(WITH_CALLBACK_DEFAULT);
                polput.setWaitKind(WAIT);
                polput.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_MESSAGE);
                cafe.getPolicy().setChannelRequestPolicyPut(dhV[i], polput);
            }

            break;
        }


        for (size_t j=1; j<nrhs; ++j)
        {

            numberOfHandles=mxGetNumberOfElements (prhs[j]);

            neWithinAllCells=0; // filled by checkHandleArray

            //What is dataType?
            handleULong = checkHandleArray(prhs[j], true); //true: do open handle if channel existing

            if (handleULong==NULL)
            {
                mexPrintf("SET_PUT_WAIT: Ignoring NULL value in input %d of %d\n", (j+1), nrhs);
                continue;
            }


            if (mxIsChar(prhs[j]) )
            {
                numberOfHandles=1;
                //mexPrintf("char numberOfHandles=%d \n",numberOfHandles);
            }
            else if (mxIsCell(prhs[j]) )
            {
                numberOfHandles=neWithinAllCells;
                //mexPrintf("cell numberOfHandles=%d \n",numberOfHandles);
            }
            if(handleULong[0]==0)
            {
                mexPrintf("Ignoring input argument %d of %d \n", (j+1),nrhs);
                continue;
            }


            for (size_t i=0; i< numberOfHandles; ++i)
            {

                status=cafe.getPolicy().getChannelRequestPolicyPut(handleULong[i], polput);
                if (status==ECAFE_INVALID_HANDLE)
                {
                    mexPrintf("invalid handle no = %d\n", handleULong[i]);
                    continue;
                }

                //mexPrintf("wait for handle no = %d\n", handleULong[i]);
                polput.setMethodKind(WITH_CALLBACK_DEFAULT);
                polput.setWaitKind(WAIT);
                polput.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_MESSAGE);
                cafe.getPolicy().setChannelRequestPolicyPut(handleULong[i], polput);
            }
        }


        break;
    }

    case SET_PUT_NOWAIT:
    {
        size_t numberOfHandles=0;
        ChannelRequestPolicy polput;

        if (nrhs<2)
        {
            //mexPrintf("Invalid Number of argument(s) %d. Input: setNoWait, handle(s) or handleArray \n", nrhs);
            //return;
            vector<unsigned int> dhV;
            vector<string> pvV;
            cafe.getHandleHelper().getHandles(dhV,pvV);
            numberOfHandles=dhV.size();

            for (size_t i=0; i< numberOfHandles; ++i)
            {

                status=cafe.getPolicy().getChannelRequestPolicyPut(dhV[i], polput);
                if (status==ECAFE_INVALID_HANDLE)
                {
                    mexPrintf("SET_PUT_NOWAIT: Invalid handle no = %d \n", dhV[i]);
                    continue;
                }

                //mexPrintf("no wait for handle no = %d\n", handleULong[i]);
                //mexPrintf("wait for handle no = %d\n", handleULong[i]);
                polput.setMethodKind(WITH_CALLBACK_DEFAULT);
                polput.setWaitKind(NO_WAIT);
                polput.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_MESSAGE);
                cafe.getPolicy().setChannelRequestPolicyPut(dhV[i], polput);
            }

            break;
        }


        for (size_t j=1; j<nrhs; ++j)
        {

            numberOfHandles=mxGetNumberOfElements (prhs[j]);

            neWithinAllCells=0; // filled by checkHandleArray

            //What is dataType?
            handleULong = checkHandleArray(prhs[j], true); //true: do open handle if channel existing

            if (handleULong==NULL)
            {
                mexPrintf("SET_PUT_WAIT: Ignoring NULL value in input %d of %d\n", (j+1), nrhs);
                continue;
            }


            if (mxIsChar(prhs[j]) )
            {
                numberOfHandles=1;
                //mexPrintf("char numberOfHandles=%d \n",numberOfHandles);
            }
            else if (mxIsCell(prhs[j]) )
            {
                numberOfHandles=neWithinAllCells;
                //mexPrintf("cell numberOfHandles=%d \n",numberOfHandles);
            }
            if(handleULong[0]==0)
            {
                mexPrintf("Ignoring input argument %d of %d \n", (j+1),nrhs);
                continue;
            }

            ChannelRequestPolicy polput;
            for (size_t i=0; i< numberOfHandles; ++i)
            {

                status=cafe.getPolicy().getChannelRequestPolicyPut(handleULong[i], polput);
                if (status==ECAFE_INVALID_HANDLE)
                {
                    mexPrintf("invalid handle no = %d\n", handleULong[i]);
                    continue;
                }

                //mexPrintf("wait for handle no = %d\n", handleULong[i]);
                polput.setMethodKind(WITH_CALLBACK_DEFAULT);
                polput.setWaitKind(NO_WAIT);
                polput.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_MESSAGE);
                cafe.getPolicy().setChannelRequestPolicyPut(handleULong[i], polput);
            }
        }


        break;
    }


    case INIT_GET_SYNC_WITHOUT_CALLBACK:
    {
        cafe.channelRequestPolicyMasterGet.setPolicy(FLUSH_AFTER_EACH_MESSAGE, WAIT, WITHOUT_CALLBACK);

        vector<unsigned int> dhV;
        vector<string> pvV;
        cafe.getHandleHelper().getHandles(dhV,pvV);
        unsigned int numberOfHandles=dhV.size();

        if (numberOfHandles==0)
        {
            return;
        }

        ChannelRequestPolicy polget;
        polget.setPolicy(FLUSH_AFTER_EACH_MESSAGE, WAIT, WITHOUT_CALLBACK);

        for (size_t i=0; i< numberOfHandles; ++i)
        {
            cafe.getPolicy().setChannelRequestPolicyGet(dhV[i], polget);
        }

        break;
    }
    case INIT_GET_SYNC_WITH_CALLBACK_DEFAULT:
    {
        cafe.channelRequestPolicyMasterGet.setPolicy(FLUSH_AFTER_EACH_MESSAGE, WAIT, WITH_CALLBACK_DEFAULT);

        vector<unsigned int> dhV;
        vector<string> pvV;
        cafe.getHandleHelper().getHandles(dhV,pvV);
        unsigned int numberOfHandles=dhV.size();

        if (numberOfHandles==0)
        {
            return;
        }

        ChannelRequestPolicy polget;
        polget.setPolicy(FLUSH_AFTER_EACH_MESSAGE, WAIT, WITH_CALLBACK_DEFAULT);

        for (size_t i=0; i< numberOfHandles; ++i)
        {
            cafe.getPolicy().setChannelRequestPolicyGet(dhV[i], polget);
        }

        break;
    }

    case OPEN_NOWAIT:
        //cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_DESIGNATED_TO_CLIENT);
        //cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);
        cafe.openPrepare();

        break;

    case OPEN_WAIT:
        cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_CHANNEL_CREATION);
        cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);

        break;


    case OPEN_NOW:
        //cafe.channelOpenPolicy.flushSendBufferNow();
        //cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_CHANNEL_CREATION);
        //cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);
        cafe.openNow();

        break;

    //change open method to with pend_event - for single opens that wish to pend
    case OPEN_WAIT_WITH_TIME:
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, timeout \n", nrhs);
            return;
        }


        if (nrhs>1)
        {
            if (!mxIsNumeric(prhs[1]) )
            {
                mexPrintf("Timeout does not have a numeric value \n");
                return;
            }


            cafe.channelOpenPolicy.setTimeout(mxGetScalar(prhs[1]));
            //cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_AFTER_EACH_CHANNEL_CREATION);
            //cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);

        }

        break;


    case OPEN_WAIT_TIME_TO_DEFAULT:
        if (nrhs>1)
        {
            mexPrintf("Ignoring number of arguments %d\n", nrhs);
            mexPrintf("This method resets open wait time to default value\n");
        }


        plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
        doubleArray   = (double *) mxGetPr (plhs[0]);
        doubleArray[0]=  cafe.channelOpenPolicy.setTimeoutToDefault();


        break;


    case OPEN_NOW_AND_WAIT:

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, timeout \n", nrhs);
            return;
        }

        if (nrhs>1)
        {
            if (!mxIsNumeric(prhs[1]) )
            {
                mexPrintf("Timeout does not have a numeric value \n");
                return;
            }

            //Get Timeput
            //double tPrevious=cafe.channelOpenPolicy.getTimeout();
            //cafe.channelOpenPolicy.setTimeout(mxGetScalar(prhs[1]));
            //cafe.channelOpenPolicy.flushSendBufferNow();
            //reset
            //cafe.channelOpenPolicy.setWhenToFlushSendBuffer(FLUSH_NOW);
            //cafe.channelOpenPolicy.setFlushSendBufferKind(WITH_PEND_EVENT);

            //Set to previous
            //cafe.channelOpenPolicy.setTimeoutToDefault();
            //cafe.channelOpenPolicy.setTimeout(tPrevious);

            cafe.openNowAndWait(mxGetScalar(prhs[1]));
        }
        break;


    case GET_OPEN_WAIT_TIME:
        if (nrhs>1)
        {
            mexPrintf("Ignoring number of arguments %d\n", nrhs);
            mexPrintf("This method returns the current open wait time value\n");
        }

        plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
        doubleArray   = (double *) mxGetPr (plhs[0]);
        doubleArray[0]=  cafe.channelOpenPolicy.getTimeout();
        break;

    case WITH_EXCEPTIONS:
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, bool \n", nrhs);
            return;
        }

        if( !mxIsLogical(prhs[1]) )
        {
            mexPrintf("2nd input argument must be of Matlab type Logical, i.e.true/false \n" );
            return;
        }


        //withExceptions = mxGetLogicals (prhs[1]);
        withExceptions = mxIsLogicalScalarTrue  (prhs[1]);

        break;


    case HAVE_EXCEPTIONS:

        plhs[0] = mxCreateNumericMatrix(1,1,mxLOGICAL_CLASS,mxREAL);
        logicalData = (mxLogical *) mxGetData (plhs[0]);
        logicalData[0]=withExceptions;

        break;

    case SEND_NOW:
        ca_flush_io();
        break;


    case OPEN_ARRAY:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message Array, pv  Output: handle Array \n", nrhs);
            return;
        }

        if( !mxIsCell(prhs[1]) )
        {
            mexPrintf("Process Variable names must be in a cell. \n" );
            return;
        }

        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);

        if(numberOfHandles==0)
        {
            mexPrintf("ERROR: MISSING INPUT \n");
            return;
        }

        vector<string>  vstr;
        vstr.reserve(numberOfHandles);

        for (size_t i=0; i< numberOfHandles; ++i)
        {

            mxArray * newCell = mxGetCell(prhs[1],i);

            if (mxIsChar(newCell) )
            {

                //message pvname
                xData = (mxArray *) newCell;

                pvLength = PVNAME_MAX_SIZE+1;
                mxGetString(xData, pv, pvLength);
                vstr.push_back(pv);
            }
            else
            {

                plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
                ulongArray = (uint32_T *) mxGetData (plhs[0]);
                ulongArray[0]=0;
                mexPrintf("ERROR READING INPUT, ELEMENT %d of %d IS NOT A mxCHAR \n", i, numberOfHandles);
                return;
            }
        }


        vector<uint32_T> vuint32=NS_MOCHA::openArray(vstr);


        plhs[0] = mxCreateNumericMatrix(1,vuint32.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData (plhs[0]);
        for (size_t i=0; i<vuint32.size(); ++i)
        {
            ulongArray[i]=vuint32[i];
        }

        break;

    }

    case OPEN:
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, pv  Output: handle \n", nrhs);
            return;
        }

        if( !mxIsChar(prhs[1]) )
        {
            mexPrintf("Process Variable name must be in char format. \n" );
            return;
        }


        //message pvname for open
        xData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, pv, pvLength);
       
        handle=NS_MOCHA::open(pv);
       

        //return handle
        //plhs[0] = mxCreateDoubleScalar(handle);

        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData (plhs[0]);
        ulongArray[0]=handle;


        break;


    case CLOSE:

        if (nrhs<2)
        {
            if(isCAInitialized)
            {
                //#ifdef HAVE_PYTHON_H
                //	Py_Finalize();
                //#endif

                cafe.terminate();
                isCAInitialized=false;
                collectionsLoaded=false;
                groupsLoaded=false;

                if (!mexIsLocked())
                {
                    mexUnlock();
                }

            }
        }
        else if (nrhs<3)
        {


            handle=checkForHandle(prhs[1]);
            if(handle==0)
            {
                return;
            }

            status=cafe.close(handle);

            if (status!=ICAFE_NORMAL)
            {
                NS_MOCHA::printStatus(status);
            }
        }
        else
        {
            mexPrintf("Invalid Number of argument %d. Input:  'close', handle \n", nrhs);
            mexPrintf("or Input: 'close' to close all handles and exit mocha\n");
            return;
        }
        break;


    case CLOSE_ARRAY:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'close', [handleArray] \n", nrhs);
            return;
        }

        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);

        neWithinAllCells=0; // filled by checkHandleArray

        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);//(uint64_T *)  mxGetData ( prhs[1]);

        if (handleULong==NULL)
        {
            mexPrintf("CLOSE_ARRAY: Ignoring NULL value in input \n" );
            mexPrintf("Input Invalid. Should be:  'closeArray', handleArray \n");
            return;
        }

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
            //mexPrintf("char  numberOfHandles %d",numberOfHandles);
        }
        else if (mxIsCell(prhs[1]) )
        {
            numberOfHandles=neWithinAllCells;
            //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
        }

        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be:  'closeArray', handleArray \n");
            return;
        }


        //status=cafe.closeHandles(handleULong, numberOfHandles);
        status=cafe.closeChannels(handleULong, numberOfHandles);

        if (status!=ICAFE_NORMAL)
        {
            NS_MOCHA::printStatus(status);
        }


        break;

    }

    case OPEN_GROUP:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, groupName  Output: groupHandle\n", nrhs);
            return;
        }

        //message pvname for open
        xData = (mxArray *)  prhs[1];

        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(xData, groupName, pvLength);

        ghandle=NS_MOCHA::groupOpen(groupName);

        // return handle

        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData (plhs[0]);
        ulongArray[0]=ghandle;
        break;
    }
    case CLOSE_GROUP:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: message, groupHandle/groupName \n", nrhs);
            return;
        }

        ghandle=checkForGroupHandleNoOpen(prhs[1]);

        if(ghandle==0)
        {
            return;
        }

        status=cafe.groupClose(ghandle);

        if (status!=ICAFE_NORMAL)
        {
            NS_MOCHA::printStatus(status);
        }
        break;
    }

    case GET_GROUP:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid no. of arguments %d. Input: message, ghandle/gname \n", nrhs);
            return;
        }

        //overall groupStatus
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        int32_T * groupStatus;
        groupStatus = (int32_T *) mxGetData (plhs[1]);

        plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);

        ghandle=checkForGroupHandle(prhs[1]);
        if(ghandle==0)
        {

            //fill with data
            mexPrintf("Group entered as input argument has not been defined!\n");
            doubleArray[0]=numeric_limits<double>::signaling_NaN();

            groupStatus[0]=ECAFE_UNKNOWN_GROUP;

            return;
        }

        mdtIndex=MOCHA_NATIVE;

        if (nrhs>2)
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument must be a char \n");
                return;
            }
            mdtIndex=getMdtIndex(prhs[2]);
        }


        int32_T mdtIndexOriginal=mdtIndex;

        PVGroup pvgroup;

        status=cafe.groupAttach(ghandle, pvgroup);

        PVDataHolder * du;
        unsigned int NPV=0;
        bool isGroupTimeout=false;

        if (status!=ECAFE_UNKNOWN_GROUP && status != ECAFE_EMPTY_GROUP)
        {


            if (cacheFlag)
            {
                status=cafe.groupGetCache(ghandle, pvgroup);
            }
            else
            {
                status=cafe.groupGet(ghandle, pvgroup);
            }


            // statusGroup != ICAFE_SUCCESS when one or more channels accesses fail
            if ( pvgroup.getStatusGroup() !=  ICAFE_SUCCESS )
            {
                for (mwSignedIndex i=0; i<pvgroup.getNPV(); ++i)
                {
                    if (pvgroup.getPVData(i).getStatus() != ICAFE_SUCCESS )
                    {
                        pvgroup.getPVData(i).print();
                    }
                }
            }


            if (!cafe.getCafeStatus().csc.isTimeout(status) )   // OK
            {

                du = pvgroup.getPVData();
                NPV= pvgroup.getNPV();
            }
            else     //timeout
            {
                mexPrintf("Info: Timeout in CAFE Group; reverting to getPVArray method\n");
                vector<unsigned int> dhV = cafe.getHandlesFromWithinGroupV(ghandle);
                du = new PVDataHolder[dhV.size()];
                if (cacheFlag)
                {
                    status=cafe.getPVArray(dhV, du);
                }
                else
                {
                    //status=cafe.getPVArrayCache(dhV, du);
                    status=cafe.getCachePVArray(dhV, du);
                }
                NPV=dhV.size();
                isGroupTimeout=true;

            }
        }
        else
        {

            mexPrintf("status/handle %d / %d  \n", status, ghandle);
            mexPrintf("Returning NaN in place of struct  \n");

            NS_MOCHA::printStatus(status);

            doubleArray[0]=numeric_limits<double>::signaling_NaN();

            groupStatus[0]=status;

            return;
        }


        groupStatus[0]=status;

        if (extendedPV)
        {
            plhs[0]=mxCreateStructMatrix(1,NPV, nfieldsExtra, fnamesExtra);
        }
        else
        {
            plhs[0]=mxCreateStructMatrix(1,NPV, nfields, fnames);
        }



        for (mwSignedIndex j=0; j <NPV; ++j)
        {

            mdtIndex= mdtIndexOriginal;

            unsigned  int handleLocal =
                cafe.getHandleHelper().getHandleFromPVWithinGroup
                (du[j].getPVName(),pvgroup.getGroupHandle());


            //Match INPUT to CAFE DATATYPE
            if (mdtIndexOriginal==MOCHA_NATIVE)
            {
                mdtIndex=getMdtIndexFromCafeDataType(du[j].getDataType());

                //unsigned  int handleLocal =
                //       cafe.getHandleHelper().getHandleFromPVWithinGroup
                //     (du[j].getPVName(),pvgroup.getGroupHandle());

                if (cafe.getHandleHelper().isEnum(handleLocal))
                {
                    mdtIndex=MOCHA_STRING;
                }

            }

            fillPVStruct(mdtIndex, du[j]);
            if (extendedPV)
            {
                fillPVStructExtra(handleLocal, du[j]);
            }

            if (du[j].getStatus() != ICAFE_NORMAL)
            {
                fout0=getValueIfError(mdtIndex);
            }


            if (extendedPV)
            {
                mxSetFieldByNumber(plhs[0], j, 0, fout10);
                mxSetFieldByNumber(plhs[0], j, 1, fout5);
                mxSetFieldByNumber(plhs[0], j, 2, fout0);
                mxSetFieldByNumber(plhs[0], j, 3, fout1);
                mxSetFieldByNumber(plhs[0], j, 4, fout6);
                mxSetFieldByNumber(plhs[0], j, 5, fout2);
                mxSetFieldByNumber(plhs[0], j, 6, fout7);
                mxSetFieldByNumber(plhs[0], j, 7, fout3);
                mxSetFieldByNumber(plhs[0], j, 8, fout8);
                mxSetFieldByNumber(plhs[0], j, 9, fout4);
                mxSetFieldByNumber(plhs[0], j,10, fout9);
            }
            else
            {
                mxSetFieldByNumber(plhs[0], j, 0, fout0);
                mxSetFieldByNumber(plhs[0], j, 1, fout1);
                mxSetFieldByNumber(plhs[0], j, 2, fout2);
                mxSetFieldByNumber(plhs[0], j, 3, fout3);
                mxSetFieldByNumber(plhs[0], j, 4, fout4);
            }



        } //for


        if(isGroupTimeout)
        {
            delete [] du;
        }


        break;
    }



    case GET_ME_STRUCT:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid no. of arguments %d. Input: message, ME.message \n", nrhs);
            return;
        }


        //ischar?
        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("2nd input argument must be of type char \n");
            return;
        }


        mData = (mxArray *)  prhs[1];
        MELength = ME_MESSAGE_MAX_SIZE+1;
        mxGetString(mData, MEmess, MELength);


        plhs[0]=mxCreateStructMatrix(1, 1, nfieldsME, fnamesME);


        fillMEStruct( (string)MEmess);


        mxSetFieldByNumber(plhs[0], 0, 0, fout0);
        mxSetFieldByNumber(plhs[0], 0, 1, fout1);
        mxSetFieldByNumber(plhs[0], 0, 2, fout2);
        mxSetFieldByNumber(plhs[0], 0, 3, fout3);
        mxSetFieldByNumber(plhs[0], 0, 4, fout4);

        break;

    }


    case GET_ASYN:
    {

        //Already checked for at top of program where we deterine if GET_ARRAY_ASYN or GET_ASYN is used
        if (nrhs>2)
        {
            mexPrintf("Ignoring all input arguments after the 2nd \n");
            mexPrintf("Method is mocha ('getAsyn', <handlePV> \n");
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);

        status=cafe.get(handle);
        longStatus[0]=status;

        if (status!=ICAFE_NORMAL)
        {
            //NS_MOCHA::printStatus(status);
            cafe.printStatus(handle,status);

        }

        break;
    }


    case GET_ARRAY_ASYN:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }
        size_t numberOfHandles=0;

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else
        {
            numberOfHandles=mxGetNumberOfElements (prhs[1]);
        }

        handleULong = checkHandleArray(prhs[1],true);

        if (handleULong==NULL)
        {
            mexPrintf("GET_ARRAY_SYNC: Ignoring NULL value in input \n");
            mexPrintf("Number of arguments should be %d. with Input:  message, handle/pv \n", nrhs);
            return;
        }

        plhs[1] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS, mxREAL);
        longArray = (int32_T *)  mxGetData (plhs[1]);

        status=cafe.get( (unsigned  int *) handleULong,  numberOfHandles,
                         (int *) longArray);

        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS, mxREAL);
        longStatus = (int32_T *)  mxGetData (plhs[0]);
        longStatus[0]=(int32_T) status;

        break;
    }


    case GET_ENUM_STRINGS:
    {

        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }
        if (nh >1)
        {
            mexPrintf("Method limited to one handle/pv only. Reporting on first given");
        }

        handle=checkForHandle(prhs[1]);

        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        chtype rdt;
        status=cafe.getHandleHelper().getDataTypeRequest(handle, rdt);

        if (status!=ICAFE_NORMAL)
        {
            mexPrintf("Error for handle input= %d \n", (uint32_T) handle );
            NS_MOCHA::printStatus(status);
            plhs[0] = mxCreateCellMatrix(1,1);
            mxSetCell (plhs[0], 0, mxCreateString((const char *) "" ) );
            return;
        }

        vector<std::string> enumStrings= cafe.getHandleHelper().getEnumStrings(handle);
        unsigned int noEnumStrings=enumStrings.size();

        if (noEnumStrings==0)
        {
            status=cafe.getHandleHelper().getStatus(handle);
            if (status!=ICAFE_NORMAL)
            {
                NS_MOCHA::printStatus(status);
            }
            plhs[0] = mxCreateCellMatrix(1,1);
            mxSetCell (plhs[0], 0, mxCreateString((const char *) "" ) );
        }
        else
        {
            plhs[0] = mxCreateCellMatrix(1,noEnumStrings);
            for (mwSignedIndex i=0; i <noEnumStrings; ++i)
                mxSetCell (plhs[0], i, mxCreateString((const char *) enumStrings[i].c_str()));
        }
        break;
    }

    case GET_CTRL_STRUCT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv, matlab type (optional)\n", nrhs);
            return;
        }

        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }
        if (nh >1)
        {
            mexPrintf("Method limited to one handle/pv only. Reporting on first given");
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        mdtIndex=MOCHA_NATIVE;

        if (nrhs>2)
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument must be a char \n");
                return;
            }
            mdtIndex=getMdtIndex(prhs[2]);
        }

        fout1 = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData (fout1);
        fout2 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
        shortAlarmStatus = (int16_T *) mxGetData (fout2);
        fout3 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
        shortAlarmSeverity = (int16_T *) mxGetData (fout3);
        fout4 = mxCreateNumericMatrix(1,1, mxUINT16_CLASS,mxREAL);
        precision = (uint16_T *) mxGetData(fout4);

        fout6 = mxCreateNumericMatrix(1,1, mxUINT16_CLASS,mxREAL);
        noEnumStrings = (uint16_T *) mxGetData(fout6);
        fout8 = mxCreateDoubleMatrix(1,1,mxREAL);
        udl = (double *) mxGetPr (fout8);
        fout9 = mxCreateDoubleMatrix(1,1,mxREAL);
        ldl = (double *) mxGetPr (fout9);
        fout10 = mxCreateDoubleMatrix(1,1,mxREAL);
        ual = (double *) mxGetPr (fout10);
        fout11 = mxCreateDoubleMatrix(1,1,mxREAL);
        lal = (double *) mxGetPr (fout11);
        fout12 = mxCreateDoubleMatrix(1,1,mxREAL);
        uwl = (double *) mxGetPr (fout12);
        fout13 = mxCreateDoubleMatrix(1,1,mxREAL);
        lwl = (double *) mxGetPr (fout13);
        fout14 = mxCreateDoubleMatrix(1,1,mxREAL);
        ucl = (double *) mxGetPr (fout14);
        fout15 = mxCreateDoubleMatrix(1,1,mxREAL);
        lcl = (double *) mxGetPr (fout15);


        fout16=mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        fHandle    = (uint32_T *) mxGetData(fout16);
        fHandle[0]=handle;
        string s = cafe.getPVFromHandle(handle);
        fout17 =mxCreateString( s.c_str() );

        nelem=cafe.getHandleHelper().getNelemRequestCtrl(handle);

        // mexPrintf("nelemRequested %d", nelem);
        PVCtrlHolder du;
        du.setNelem(nelem);
        //du.setNelem(2);
        // mexPrintf("nelem %d ", du.getNelem());

        if (cacheFlag)
        {
            status=cafe.getCtrlCache(handle,du);
        }
        else
        {
            status=cafe.getCtrl(handle,du);
        }

        reportError=false;

        plhs[0]=mxCreateStructMatrix(1, 1, nfieldsCtrl, fnamesCtrl);

        //Match INPUT to CAFE DATATYPE
        if (mdtIndex==MOCHA_NATIVE)
        {
            mdtIndex=getMdtIndexFromCafeDataType(du.getDataType());
            if (cafe.getHandleHelper().isEnum(handle))
            {
                mdtIndex=MOCHA_STRING;
            }
        }

        fillCtrlStruct(mdtIndex, du);

        if (status==ICAFE_NORMAL)
        {
            fout5 = mxCreateString((char *) du.getUnits());
            ////fillCtrlStruct(mdtIndex, du);
        } //if
        else
        {
            reportError=true;
        }
        // Use shared_ptr since cafe-1.0
        //delete [] du.val;

        if (reportError)
        {
            fout0=getValueIfError(mdtIndex);
            //longStatus[0]         = (int32_T) status;
            //shortAlarmStatus[0]   = (int16_T) 0;
            //shortAlarmSeverity[0] = (int16_T) 0;
            fout5 = mxCreateString((const char *) "");
            precision[0]=0;
            noEnumStrings[0]=0;
            fout7 = mxCreateCellMatrix(1,1);
            for (mwSignedIndex i=0; i <1; ++i)
                mxSetCell (fout7, i, mxCreateString((const char *) "" ) );
            udl[0]=0;
            ldl[0]=0;
            ual[0]=0;
            lal[0]=0;
            uwl[0]=0;
            lwl[0]=0;
            ucl[0]=0;
            lcl[0]=0;
            //cafe.printStatus(handle,status);
        }

        mxSetFieldByNumber(plhs[0], 0,  0, fout16); //handle
        mxSetFieldByNumber(plhs[0], 0,  1, fout17); //pv
        mxSetFieldByNumber(plhs[0], 0,  2, fout0);
        mxSetFieldByNumber(plhs[0], 0,  3, fout1);
        mxSetFieldByNumber(plhs[0], 0,  4, fout1a);
        mxSetFieldByNumber(plhs[0], 0,  5, fout2);
        mxSetFieldByNumber(plhs[0], 0,  6, fout2a);
        mxSetFieldByNumber(plhs[0], 0,  7, fout3);
        mxSetFieldByNumber(plhs[0], 0,  8, fout3a);

        mxSetFieldByNumber(plhs[0], 0,  9, fout4);
        mxSetFieldByNumber(plhs[0], 0, 10, fout5);
        mxSetFieldByNumber(plhs[0], 0, 11, fout6);
        mxSetFieldByNumber(plhs[0], 0, 12, fout7);
        mxSetFieldByNumber(plhs[0], 0, 13, fout8);
        mxSetFieldByNumber(plhs[0], 0, 14, fout9);
        mxSetFieldByNumber(plhs[0], 0, 15, fout10);
        mxSetFieldByNumber(plhs[0], 0, 16, fout11);
        mxSetFieldByNumber(plhs[0], 0, 17, fout12);
        mxSetFieldByNumber(plhs[0], 0, 18, fout13);
        mxSetFieldByNumber(plhs[0], 0, 19, fout14);
        mxSetFieldByNumber(plhs[0], 0, 20, fout15);
        break;
    }

    case GET_STRUCT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv, matlab type (optional) \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        mdtIndex=MOCHA_NATIVE;

        if (nrhs>2)
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument must be a char \n");
                return;
            }
            mdtIndex=getMdtIndex(prhs[2]);
        }

        reportError=false;

        uint32_T * d4;
        nelem = cafe.getHandleHelper().getNelemClient(handle); //change NelemRequest to NelemClient

        // OUTPUT

        if (extendedPV)
        {
            plhs[0]=mxCreateStructMatrix(1, 1, nfieldsExtra, fnamesExtra);
        }
        else
        {
            plhs[0]=mxCreateStructMatrix(1, 1, nfields, fnames);
        }
        /*
        fout1 = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData (fout1);

        fout2 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
        shortAlarmStatus = (int16_T *) mxGetData (fout2);

        fout3 = mxCreateNumericMatrix(1,1,mxINT16_CLASS,mxREAL);
        shortAlarmSeverity = (int16_T *) mxGetData (fout3);

        fout4 = mxCreateNumericMatrix(1,7,mxUINT32_CLASS,mxREAL);
        d4    = (uint32_T *) mxGetData(fout4);




        fout9 = mxCreateNumericMatrix(1,2,mxUINT32_CLASS,mxREAL);
        d9    = (uint32_T *) mxGetData(fout9);
        */

        PVDataHolder du;

        epicsTimeStamp ets1;
        ets1.secPastEpoch=0;
        ets1.nsec=0;

        unsigned int nelemPrevious=1;
        bool nelemCacheChangeFlag=false;

        if(cacheFlag)
        {
            nelem = cafe.getHandleHelper().getNelemToRetrieveFromCache(handle);
            if (nelem > cafe.getHandleHelper().getNelemRequest(handle) )
            {
                nelemPrevious=nelem;
                nelem = min( (unsigned int) nelem, cafe.getHandleHelper().getNelemRequest(handle));
                cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,nelem);
                nelemCacheChangeFlag=true;
            }
        }

        du.setNelem(nelem);

        if (cacheFlag)
        {
            status=cafe.getCache(handle,du);
        }
        else
        {
            status=cafe.get(handle,du);
        }

        //Match INPUT to CAFE DATATYPE
        if (mdtIndex==MOCHA_NATIVE)
        {
            mdtIndex=getMdtIndexFromCafeDataType(du.getDataType());
            if (cafe.getHandleHelper().isEnum(handle))
            {
                mdtIndex=MOCHA_STRING;
            }
        }

        fillPVStruct(mdtIndex, du);

        if (extendedPV)
        {
            fillPVStructExtra(handle, du);
        }

        if (status!=ICAFE_NORMAL)
        {
            reportError=true;
        }


        if(nelemCacheChangeFlag)
        {
            cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,nelemPrevious);
        }

        // Use shared_ptr since cafe-1.0
        // delete [] du.val;

        if (reportError)
        {

            fout0=getValueIfError(mdtIndex);
            //longStatus[0]         = (int32_T) status;
            //shortAlarmStatus[0]   = (int16_T) 0;
            //shortAlarmSeverity[0] = (int16_T) 0;

            //cafe.printStatus(handle,status);
        }


        if (extendedPV)
        {
            mxSetFieldByNumber(plhs[0], 0, 0, fout10);
            mxSetFieldByNumber(plhs[0], 0, 1, fout5);
            mxSetFieldByNumber(plhs[0], 0, 2, fout0);
            mxSetFieldByNumber(plhs[0], 0, 3, fout1);
            mxSetFieldByNumber(plhs[0], 0, 4, fout6);
            mxSetFieldByNumber(plhs[0], 0, 5, fout2);
            mxSetFieldByNumber(plhs[0], 0, 6, fout7);
            mxSetFieldByNumber(plhs[0], 0, 7, fout3);
            mxSetFieldByNumber(plhs[0], 0, 8, fout8);
            mxSetFieldByNumber(plhs[0], 0, 9, fout4);
            mxSetFieldByNumber(plhs[0], 0,10, fout9);
        }
        else
        {
            mxSetFieldByNumber(plhs[0], 0, 0, fout0);
            mxSetFieldByNumber(plhs[0], 0, 1, fout1);
            mxSetFieldByNumber(plhs[0], 0, 2, fout2);
            mxSetFieldByNumber(plhs[0], 0, 3, fout3);
            mxSetFieldByNumber(plhs[0], 0, 4, fout4);
        }

        if (status!=ICAFE_NORMAL)
        {
            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }

        break;
    }

    case GET_SCALAR_ARRAY:
    {

        if (nrhs<2)
        {
            if(cacheFlag)
            {
                mexPrintf("Invalid Number of argument %d. Input: 'getScalarArrayCache', handleArray \n", nrhs);
            }
            else
            {
                mexPrintf("Invalid Number of argument %d. Input: 'getScalarArray', handleArray \n", nrhs);
            }
            return;
        }

        if (nrhs>2 && (mochaIndex==mochaIndexOriginal || cacheFlag) )
        {
            //ischar?
            if (!mxIsChar(prhs[nrhs-1]) )
            {
                mexPrintf("Last input argument must be a char \n");
                return;
            }
            mdtIndex=NS_MOCHA::getMdtIndex(prhs[nrhs-1]);
        }


        int32_T mdtIndexOriginal=mdtIndex;
        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);

        neWithinAllCells=0; // filled by checkHandleArray

        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);//(uint64_T *)  mxGetData ( prhs[1]);


        if (handleULong==NULL)
        {
            mexPrintf("GET_SCALAR_ARRAY: Ignoring NULL value in input\n");
            mexPrintf("Input Invalid. Should be:  'getStructArray(Cache)', handleArray \n");
            return;
        }

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
            //mexPrintf("char  numberOfHandles %d",numberOfHandles);
        }
        else if (mxIsCell(prhs[1]) )
        {
            numberOfHandles=neWithinAllCells;
            //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
        }


        //for (size_t i=0; i<numberOfHandles; ++i) {

        //	if(handleULong[i]==0) {
        //    mexPrintf("Invalid input (handle=0) in [element] %d of array of length %d \n",i,numberOfHandles);
        //		mexPrintf("Method format:  'getStructArray(Cache)', handleArray\n");

        //    return;
        //	}

        //}



        PVDataHolder * pvd = new PVDataHolder[numberOfHandles];

        for (size_t i=0; i<numberOfHandles; ++i)
        {
            nelem = cafe.getHandleHelper().getNelemRequest((unsigned  int) handleULong[i]);
            if(nelem<1)
            {
                nelem=1; // Should not happen
            }
            else if (nelem>1)
            {
                nelem=1; // First element only
            }
            pvd[i].setNelem(nelem);

            //Is this really needed?
            if(cacheFlag)
            {
                cafe.getHandleHelper().setNelemToRetrieveFromCache((unsigned  int) handleULong[i],nelem);
            }
        }

        if(cacheFlag)
        {
            status=cafe.getCache( (unsigned  int *) handleULong,  numberOfHandles,
                                  pvd);
        }
        else
        {

            status=cafe.get( (unsigned  int *) handleULong,  numberOfHandles,
                             pvd);

        }


        int32_T * groupStatus;

        //overall groupStatus
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        groupStatus = (int32_T *) mxGetData (plhs[1]);
        groupStatus[0]=status;

        plhs[2] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData (plhs[2]);

        //plhs[0] = mxCreateDoubleMatrix (1,numberOfHandles,mxREAL);
        //doubleArray = (double *) mxGetPr (plhs[0]);

        //If no datatype is given, assume datatype of first element!
        if (mdtIndexOriginal==MOCHA_NATIVE)
        {
            mdtIndex=NS_MOCHA::getMdtIndexFromCafeDataType(pvd[0].getDataType());
            if (cafe.getHandleHelper().isEnum(handleULong[0]))     //check this for handleULong[0]
            {
                mdtIndex=MOCHA_STRING;
            }
        }



        switch (mdtIndex)
        {
        case MOCHA_DOUBLE:
            plhs[0] = mxCreateDoubleMatrix(1,numberOfHandles,mxREAL);
            doubleArray = (double *) mxGetPr (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    doubleArray[j]=pvd[j].getAsDouble(0);
                }
                else
                {
                    doubleArray[j]=numeric_limits<double>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_FLOAT:
            plhs[0]  = mxCreateNumericMatrix(1,numberOfHandles,mxSINGLE_CLASS,  mxREAL);
            floatArray = (float *) mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    floatArray[j]=pvd[j].getAsFloat(0);
                }
                else
                {
                    floatArray[j]=numeric_limits<float>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_INT:
            plhs[0]  = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,  mxREAL);
            longArray = (int32_T *)  mxGetData (plhs[0] );
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    longArray[j]= (int32_T) pvd[j].getAsLong(0);
                }
                else
                {
                    longArray[j]=numeric_limits<int32_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_SHORT:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxINT16_CLASS,  mxREAL);
            shortArray = (int16_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    shortArray[j]=(int16_T)  pvd[j].getAsShort(0);
                }
                else
                {
                    shortArray[j]=numeric_limits<int16_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_USHORT:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxUINT16_CLASS,  mxREAL);
            ushortArray = (uint16_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {

                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    ushortArray[j]=(uint16_T) pvd[j].getAsUShort(0);
                }
                else
                {
                    ushortArray[j]=numeric_limits<uint16_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_UCHAR:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxUINT8_CLASS,  mxREAL);
            ucharArray = (uint8_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    ucharArray[j]=(uint8_T) pvd[j].getAsChar(0);
                }
                else
                {
                    ucharArray[j]=numeric_limits<uint8_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;

        case MOCHA_STRING:
        {
            plhs[0] = mxCreateCellMatrix(1,numberOfHandles);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                //mxSetCell (plhs[0], j, mxCreateString( *pvd[j].getAsDbr_string_t(0)));
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    mxSetCell (plhs[0], j, mxCreateString( pvd[j].getAsString(0).c_str()));
                }
                else
                {
                    mxSetCell (plhs[0], j, mxCreateString( numeric_limits<char *>::quiet_NaN() ));
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        }
        // Non DBR_TYPES
        case MOCHA_CHAR:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxINT8_CLASS,  mxREAL);
            charArray = (int8_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    charArray[j]=(int8_T) pvd[j].getAsShort(0);
                }
                else
                {
                    charArray[j]= numeric_limits<int8_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        case MOCHA_UINT:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxUINT32_CLASS,  mxREAL);
            ulongArray = (uint32_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    ulongArray[j]=(uint32_T) pvd[j].getAsDouble(0);
                }
                else
                {
                    ulongArray[j]= numeric_limits<uint32_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        case MOCHA_LONG:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxINT64_CLASS,  mxREAL);
            longlongArray = (int64_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    longlongArray[j]=(int64_T) pvd[j].getAsDouble(0);
                }
                else
                {
                    longlongArray[j]= numeric_limits<int64_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        case MOCHA_ULONG:
            plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxUINT64_CLASS,  mxREAL);
            ulonglongArray = (uint64_T *)  mxGetData (plhs[0]);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    ulonglongArray[j]=(uint64_T) pvd[j].getAsDouble(0);
                }
                else
                {
                    ulonglongArray[j]= numeric_limits<uint64_T>::quiet_NaN();
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        default:
        {
            plhs[0] = mxCreateCellMatrix(1,numberOfHandles);
            for (mwSignedIndex j=0; j <numberOfHandles; ++j)
            {
                if ( pvd[j].getStatus() == ICAFE_NORMAL)
                {
                    //mxSetCell (plhs[0], j, mxCreateString( *pvd[j].getAsDbr_string_t(0)));
                    mxSetCell (plhs[0], j, mxCreateString( pvd[j].getAsString(0).c_str()));
                }
                else
                {
                    mxSetCell (plhs[0], j, mxCreateString(  numeric_limits<char *>::quiet_NaN()));
                }
                longStatus[j]=pvd[j].getStatus();
            }
            break;
        }
        } //switch

        // for (mwSignedIndex j=0; j <numberOfHandles; ++j) {
        // longStatus[j]=pvd[j].getStatus();

        // Use shared_ptr since cafe-1.0
        //delete [] pvd[j].val;
        //}
        delete [] pvd;
        break;
    }


    case GET_STRUCT_ARRAY:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'getStructArray', handleArray, optional data type \n", nrhs);
            return;
        }

        if (nrhs>2 && (mochaIndex==mochaIndexOriginal || cacheFlag) )
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument (data type) must be a char \n");
                return;
            }
            mdtIndex=NS_MOCHA::getMdtIndex(prhs[2]);
        }

        int32_T mdtIndexOriginal=mdtIndex;
        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);


        neWithinAllCells=0; // filled by checkHandleArray

        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);//(uint64_T *)  mxGetData ( prhs[1]);

        if (handleULong==NULL)
        {
            mexPrintf("GET_STRUCT_ARRAY: Ignoring NULL value in input \n");
            mexPrintf("Input Invalid. Should be:  'getStructArray', handleArray \n");
            return;
        }


        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be:  'getStructArray', handleArray \n");
            return;
        }

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else if (mxIsCell(prhs[1]) )
        {
            numberOfHandles=neWithinAllCells;
            //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
        }



        PVDataHolder * pvd = new PVDataHolder[numberOfHandles];

        for (size_t i=0; i<numberOfHandles; ++i)
        {
            nelem = cafe.getHandleHelper().getNelemClient((unsigned int) handleULong[i]); //change Request to Client

            //mexPrintf("native %d",  cafe.getHandleHelper().getNelemNative((unsigned int) handleULong[i]));
            //mexPrintf("request %d", cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));
            //mexPrintf("client %d",  cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));

            //request to Client
            if(nelem<1)
            {
                nelem=1; // Should not happen
            }
            pvd[i].setNelem(nelem);

            if(cacheFlag)
            {
                cafe.getHandleHelper().setNelemToRetrieveFromCache((unsigned int) handleULong[i],nelem);
            }
        }

        if(cacheFlag)
        {
            status=cafe.getCache( (unsigned  int *) handleULong,  numberOfHandles, pvd);

        }
        else
        {

            status=cafe.get( (unsigned  int *) handleULong,  numberOfHandles, pvd);

        }

        /*
        for (size_t i=0; i<numberOfHandles; ++i) {
        mexPrintf("\n pvd.getNelem(), nelemNative %d %d", pvd[i].getNelem(), nelem);

        		mexPrintf("\n native %d", cafe.getHandleHelper().getNelemNative((unsigned int) handleULong[i]));
        		mexPrintf("request %d", cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));
        		mexPrintf("client %d", cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));
        }
        */

        int32_T * groupStatus;

        //overall groupStatus
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        groupStatus = (int32_T *) mxGetData (plhs[1]);
        groupStatus[0]=status;


        if (extendedPV)
        {
            plhs[0]=mxCreateStructMatrix(1,numberOfHandles, nfieldsExtra, fnamesExtra);
        }
        else
        {
            plhs[0]=mxCreateStructMatrix(1,numberOfHandles, nfields, fnames);
        }

        for (mwSignedIndex j=0; j <numberOfHandles; ++j)
        {
            if (mdtIndexOriginal==MOCHA_NATIVE)
            {
                mdtIndex=getMdtIndexFromCafeDataType(pvd[j].getDataType());
                if (cafe.getHandleHelper().isEnum(handleULong[j]))   //check this for handleULong
                {
                    mdtIndex=MOCHA_STRING;
                }
            }


            fillPVStruct(mdtIndex, pvd[j]);
            if (extendedPV)
            {
                fillPVStructExtra(handleULong[j], pvd[j]);
            }

            if (pvd[j].getStatus()!=ICAFE_NORMAL)
            {
                fout0=getValueIfError(mdtIndex);
                //longStatus[0]         = (int32_T) pvd[j].getStatus();
                //shortAlarmStatus[0]   = (int16_T) 0;
                //shortAlarmSeverity[0] = (int16_T) 0;
            }


            if (extendedPV)
            {
                mxSetFieldByNumber(plhs[0], j, 0, fout10);
                mxSetFieldByNumber(plhs[0], j, 1, fout5);
                mxSetFieldByNumber(plhs[0], j, 2, fout0);
                mxSetFieldByNumber(plhs[0], j, 3, fout1);
                mxSetFieldByNumber(plhs[0], j, 4, fout6);
                mxSetFieldByNumber(plhs[0], j, 5, fout2);
                mxSetFieldByNumber(plhs[0], j, 6, fout7);
                mxSetFieldByNumber(plhs[0], j, 7, fout3);
                mxSetFieldByNumber(plhs[0], j, 8, fout8);
                mxSetFieldByNumber(plhs[0], j, 9, fout4);
                mxSetFieldByNumber(plhs[0], j,10, fout9);
            }
            else
            {

                mxSetFieldByNumber(plhs[0], j, 0, fout0);
                mxSetFieldByNumber(plhs[0], j, 1, fout1);
                mxSetFieldByNumber(plhs[0], j, 2, fout2);
                mxSetFieldByNumber(plhs[0], j, 3, fout3);
                mxSetFieldByNumber(plhs[0], j, 4, fout4);
            }


            // Use shared_ptr since cafe-1.0
            //delete [] pvd[j].val;
        }
        delete [] pvd;


        break;
    }


    case GET_CELL_ARRAY:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'getCellArray', handleArray, optional data type \n", nrhs);
            return;
        }

        if (nrhs>2 && (mochaIndex==mochaIndexOriginal || cacheFlag) )
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument (data type) must be a char \n");
                return;
            }
            mdtIndex=NS_MOCHA::getMdtIndex(prhs[2]);
        }

        int32_T mdtIndexOriginal=mdtIndex;
        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);

        neWithinAllCells=0; // filled by checkHandleArray

        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);//(uint64_T *)  mxGetData ( prhs[1]);

        if (handleULong==NULL)
        {
            mexPrintf("GET_CELL_ARRAY: Ignoring NULL value in input \n");
            mexPrintf("Input Invalid. Should be:  'getCellArray', handleArray \n");
            return;
        }


        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be:  'getCellArray', handleArray \n");
            return;
        }

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else if (mxIsCell(prhs[1]) )
        {
            numberOfHandles=neWithinAllCells;
            //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
        }


        PVDataHolder * pvd = new PVDataHolder[numberOfHandles];

        for (size_t i=0; i<numberOfHandles; ++i)
        {
            nelem = cafe.getHandleHelper().getNelemClient((unsigned int) handleULong[i]); //change Request to Client

            //mexPrintf("native %d",  cafe.getHandleHelper().getNelemNative((unsigned int) handleULong[i]));
            //mexPrintf("request %d", cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));
            //mexPrintf("client %d",  cafe.getHandleHelper().getNelemRequest((unsigned int) handleULong[i]));

            //request to Client
            if(nelem<1)
            {
                nelem=1; // Should not happen
            }
            pvd[i].setNelem(nelem);
            //Is this really needed?
            if(cacheFlag)
            {
                cafe.getHandleHelper().setNelemToRetrieveFromCache((unsigned int) handleULong[i],nelem);
            }
        }

        if(cacheFlag)
        {
            status=cafe.getCache( (unsigned  int *) handleULong,  numberOfHandles, pvd);

        }
        else
        {
            status=cafe.get( (unsigned  int *) handleULong,  numberOfHandles, pvd);
        }


        int32_T * groupStatus;

        //overall groupStatus
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        groupStatus = (int32_T *) mxGetData (plhs[1]);
        groupStatus[0]=status;

        plhs[2] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData (plhs[2]);

        plhs[0]=mxCreateCellMatrix(1,numberOfHandles);

        for (mwSignedIndex j=0; j <numberOfHandles; ++j)
        {
            if (mdtIndexOriginal==MOCHA_NATIVE)
            {
                mdtIndex=getMdtIndexFromCafeDataType(pvd[j].getDataType());
                if (cafe.getHandleHelper().isEnum(handleULong[j]))   //check this for handleULong
                {
                    mdtIndex=MOCHA_STRING;
                }
            }


            fillPVCell(mdtIndex, pvd[j]);

            if (pvd[j].getStatus()!=ICAFE_NORMAL)
            {
                fout0=getValueIfError(mdtIndex);
                //longStatus[0]         = (int32_T) pvd[j].getStatus();

            }

            mxSetCell (plhs[0], j, fout0);

            longStatus[j]=pvd[j].getStatus();
            // Use shared_ptr since cafe-1.0
            //delete [] pvd[j].val;
        }
        delete [] pvd;


        break;
    }




    case GET:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);

        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        //status
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[1]);

        chtype rdt;


        //Check that we are not using a wrapper method, e.g. getInt16
        if (nrhs>2 && (mochaIndex==mochaIndexOriginal || cacheFlag) )
        {
            //ischar?
            if (!mxIsChar(prhs[2]) )
            {
                mexPrintf("3rd input argument must be a char \n");
                return;
            }
            mdtIndex=NS_MOCHA::getMdtIndex(prhs[2]);

        }
        if (mdtIndex==MOCHA_NATIVE)
        {

            status=cafe.getHandleHelper().getDataTypeRequest(handle, rdt);

            if (status!=ICAFE_NORMAL)
            {
                mexPrintf("Error for handle input= %d \n", (uint32_T) mxGetScalar(prhs[1]) );
                NS_MOCHA::printStatus(status);
                longStatus[0]=status;
                plhs[0]=getValueIfError(mdtIndex);
                return;
            }

            CAFE_DATATYPE cdt = (CAFE_DATATYPE) rdt;


            mdtIndex=getMdtIndexFromCafeDataType(cdt);


            if (cafe.getHandleHelper().isEnum(handle))
            {
                mdtIndex=MOCHA_STRING;
            }
        }

        nelem = cafe.getHandleHelper().getNelemClient(handle); //Change Request to Client


        if (cafe.getHandleHelper().getOffset(handle) > 0 )
        {

            nelem = min( cafe.getHandleHelper().getNelemClient(handle),
                         (cafe.getHandleHelper().getNelemNative(handle)-cafe.getHandleHelper().getOffset(handle)) );

        }

        if(nelem<1)
        {
            nelem=1; // This is to build output matrix
        }


        unsigned int nelemPrevious=1;
        bool nelemCacheChangeFlag=false;

        if(cacheFlag)
        {

            nelem = cafe.getHandleHelper().getNelemToRetrieveFromCache(handle) ;

            //mexPrintf("get Cache: nelem = %d \n", nelem);

            if ( 	(cafe.getHandleHelper().getOffsetLast(handle) > 0) ||
                    (cafe.getHandleHelper().getNelemToRetrieveFromCache(handle) > cafe.getHandleHelper().getNelemRequest(handle)))
            {

                //if (nelem > cafe.getHandleHelper().getNelemRequest(handle)  ) {
                nelemPrevious=cafe.getHandleHelper().getNelemToRetrieveFromCache(handle);
                nelem = min( (unsigned int) nelem, cafe.getHandleHelper().getNelemRequest(handle)-cafe.getHandleHelper().getOffsetLast(handle));

                //mexPrintf("nelem to ret from Cache %d \n",cafe.getHandleHelper().getNelemToRetrieveFromCache(handle));
                //mexPrintf("nelem to ret from Cache %d \n",nelem);
                //cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,nelem+cafe.getHandleHelper().getOffsetLast(handle));
                cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,nelem);
                nelemCacheChangeFlag=true;
            }

        }

        //special case for wf with dbr_char_t
        ChannelRegalia channelInfo;
	status=cafe.getChannelInfo(handle, channelInfo);      
        unsigned int WFWithString = 0;
        if (channelInfo.getClassNameAsString().compare("waveform") ==0) {
	    WFWithString = 1;
        }

        switch (mdtIndex)
        {
        case MOCHA_DOUBLE:
            plhs[0] = mxCreateDoubleMatrix(1,nelem,mxREAL);
            doubleArray = (double *) mxGetPr (plhs[0]);
            if(cacheFlag)
            {
                status=cafe.getCache(handle,doubleArray);
            }
            else
            {
                status=cafe.get(handle,doubleArray);
            }
            break;

        case MOCHA_FLOAT:
            plhs[0]  = mxCreateNumericMatrix(1,nelem,mxSINGLE_CLASS,  mxREAL);
            floatArray = (float *) mxGetData (plhs[0]);
            if(cacheFlag)
            {
                mexPrintf("here");
                status=cafe.getCache(handle,floatArray);
                mexPrintf("status %d", status);
            }
            else
            {
                status=cafe.get(handle,floatArray);
            }
            break;

        case MOCHA_INT:
            plhs[0]  = mxCreateNumericMatrix(1,nelem,mxINT32_CLASS,  mxREAL);
            longArray = (int32_T *)  mxGetData (plhs[0] );
            if(cacheFlag)
            {
                status=cafe.getCache(handle,longArray);
            }
            else
            {
                status=cafe.get(handle,longArray);
            }
            break;

        case MOCHA_SHORT:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
            shortArray = (int16_T *)  mxGetData (plhs[0]);
            if(cacheFlag)
            {
                status=cafe.getCache(handle,shortArray);
            }
            else
            {
                status=cafe.get(handle,shortArray);
            }
            break;

        case MOCHA_USHORT:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxUINT16_CLASS,  mxREAL);
            ushortArray = (uint16_T *)  mxGetData (plhs[0]);
            if(cacheFlag)
            {
                status=cafe.getCache(handle,ushortArray);
            }
            else
            {
                status=cafe.get(handle,ushortArray);
            }
            break;

        case MOCHA_UCHAR:

	    //special case for wf with dbr_char_t
	  /*
            if (WFWithString == 1) {

                string wfStr;

                if (cacheFlag)
                {
                   status=cafe.getWFAsStringCache(handle,wfStr);
                }
                else
                {
                   status=cafe.getWFAsString(handle,wfStr);
                }

                plhs[0] = mxCreateString( wfStr.c_str() );
            } 
            else {
	   */
                plhs[0] = mxCreateNumericMatrix(1,nelem,mxUINT8_CLASS,  mxREAL);
                ucharArray = (uint8_T *)  mxGetData (plhs[0]);
                if(cacheFlag)
                {
                    status=cafe.getCache(handle,ucharArray);
                }
                else
                {
                    status=cafe.get(handle,ucharArray);
                }
	    //}
            break;

        case MOCHA_STRING:
        {

            if (WFWithString ==1) {

                string wfStr;

                if (cacheFlag)
                {
                   status=cafe.getWFAsStringCache(handle,wfStr);
                }
                else
                {
                   status=cafe.getWFAsString(handle,wfStr);
                }

                plhs[0] = mxCreateString( wfStr.c_str() );
            } 
            else {


                dbr_string_t * stringArrayc = new dbr_string_t[nelem];

                if(cacheFlag)
                {
                    status=cafe.getCache(handle,stringArrayc);
                }
                else
                {
                    status=cafe.get(handle,stringArrayc);
                }
         

                if (status==ICAFE_NORMAL)
                {

                    if (nelem==1)
                    {
                        plhs[0]=mxCreateString( stringArrayc[0] );
                    }
                    else
                    {
                        plhs[0] = mxCreateCellMatrix(1,nelem);
                        for (mwSignedIndex i=0; i <nelem; ++i)
                        {
                            mxSetCell (plhs[0], i, mxCreateString( stringArrayc[i] ));
                        }
                    }
                 }

                delete [] stringArrayc;
            }
            break;
        }
// Non DBR_TYPES
        case MOCHA_CHAR:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
            charArray = (int8_T *)  mxGetData (plhs[0]);
            shortArray = new short [nelem];
            if(cacheFlag)
            {
                status=cafe.getCache(handle,shortArray);
            }
            else
            {
                status=cafe.get(handle,shortArray);
            }
            for (mwSignedIndex i=0; i< nelem; ++i)
            {
                charArray[i]= (int8_T) shortArray[i];
            }
            delete [] shortArray;
            break;
        case MOCHA_UINT:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxUINT32_CLASS,  mxREAL);
            ulongArray = (uint32_T *)  mxGetData (plhs[0]);
            doubleArray = new double[nelem];
            if(cacheFlag)
            {
                status=cafe.getCache(handle,doubleArray);
            }
            else
            {
                status=cafe.get(handle,doubleArray);
            }
            for (mwSignedIndex i=0; i< nelem; ++i)
            {
                ulongArray[i]= (uint32_T) doubleArray[i];
            }
            delete [] doubleArray;
            break;
        case MOCHA_LONG:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxINT64_CLASS,  mxREAL);
            longlongArray = (int64_T *)  mxGetData (plhs[0]);
            doubleArray = new double[nelem];
            if(cacheFlag)
            {
                status=cafe.getCache(handle,doubleArray);
            }
            else
            {
                status=cafe.get(handle,doubleArray);
            }
            for (mwSignedIndex i=0; i< nelem; ++i)
            {
                longlongArray[i]= (int64_T) doubleArray[i];
            }
            delete [] doubleArray;
            break;
        case MOCHA_ULONG:
            plhs[0] = mxCreateNumericMatrix(1,nelem,mxUINT64_CLASS,  mxREAL);
            ulonglongArray = (uint64_T *)  mxGetData (plhs[0]);
            doubleArray = new double[nelem];
            if(cacheFlag)
            {
                status=cafe.getCache(handle,doubleArray);
            }
            else
            {
                status=cafe.get(handle,doubleArray);
            }
            for (mwSignedIndex i=0; i< nelem; ++i)
            {
                ulonglongArray[i]= (uint64_T) doubleArray[i];
            }
            delete  [] doubleArray;
            break;
        default:
        {
            dbr_string_t * stringArrayc = new dbr_string_t[nelem];

            if(cacheFlag)
            {
                status=cafe.getCache(handle,stringArrayc);
            }
            else
            {
                status=cafe.get(handle,stringArrayc);
            }

            if (nelem==1)
            {
                plhs[0]=mxCreateString( stringArrayc[0] );
            }
            else
            {
                plhs[0] = mxCreateCellMatrix(1,nelem);
                for (mwSignedIndex i=0; i <nelem; ++i)
                {
                    mxSetCell (plhs[0], i, mxCreateString( stringArrayc[i] ));
                }
            }


            delete [] stringArrayc;
            break;
        }
        } //switch


        //if(nelemCacheChangeFlag) {
        //cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,nelemPrevious);
        //}

        longStatus[0]=status;

        if (status !=ICAFE_NORMAL)
        {
            //cafe.printStatus(handle,status);
            plhs[0]=getValueIfError(mdtIndex);

            /*
            mexPrintf("status=%s\n", cafe.getCafeStatus().code(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().info(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().msgID(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().severity(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().message(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().csc.msgIDAsString(status).c_str());
            mexPrintf("status=%s\n", cafe.getCafeStatus().csc.message(status).c_str());
            */

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }

        }

        break;
    }



    case GET_WF_AS_STRING:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        string wfStr;

        if (cacheFlag)
        {
            status=cafe.getWFAsStringCache(handle,wfStr);
        }
        else
        {
            status=cafe.getWFAsString(handle,wfStr);
        }

        //plhs[0] = mxCreateCellMatrix(1,2);
        //mxSetCell (plhs[0], 0, mxCreateString( wfStr.c_str() ));

        plhs[0] = mxCreateString( wfStr.c_str() );

        if (status!=ICAFE_NORMAL)
        {
            cafe.printStatus(handle,status);
            //NS_MOCHA::printStatus(status);

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }
        break;
    }


    case GET_ALARM:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        plhs[0] = mxCreateNumericMatrix(1,2, mxINT16_CLASS, mxREAL);
        shortArray = (short *) mxGetData(plhs[0]);

        shortArray[0]=-1;
        shortArray[1]=-1;

        status=cafe.getHandleHelper().getAlarmStatusSeverity(handle,shortArray);

        if (status!=ICAFE_NORMAL)
        {
            cafe.printStatus(handle,status);
            //NS_MOCHA::printStatus(status);

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }
        break;
    }


    case GET_ALARM_AS_STRING:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }



        string asas[2];
        asas[0]="";
        asas[1]="";

        status=cafe.getHandleHelper().getAlarmStatusSeverityAsString(handle,asas);


        plhs[0] = mxCreateCellMatrix(1,2);

        mxSetCell (plhs[0], 0, mxCreateString( asas[0].c_str() ));
        mxSetCell (plhs[0], 1, mxCreateString( asas[1].c_str() ));

        if (status!=ICAFE_NORMAL)
        {
            cafe.printStatus(handle,status);
            //NS_MOCHA::printStatus(status);

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }
        break;
    }


    case GET_STATUS:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        status=cafe.getHandleHelper().getStatus(handle);

        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,  mxREAL);

        longArray =   (int32_T *) mxGetData ( plhs[0]);
        longArray[0]= (int32_T)   status;

        string s =  cafe.getCafeStatus().csi.message(status);
        string c =  cafe.getCafeStatus().csc.message(status);

        plhs[1] = mxCreateCellMatrix(1,2);

        mxSetCell (plhs[1], 0, mxCreateString( c.c_str() ));
        mxSetCell (plhs[1], 1, mxCreateString( s.c_str() ));

        break;
    }


    case IS_STATUS_TIMEOUT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n");
            return;
        }

        plhs[0] = mxCreateLogicalScalar(cafe.getCafeStatus().csc.isTimeout((int32_T) mxGetScalar(prhs[1])));

        break;
    }




    case GET_STATUS_TEXT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n");
            return;
        }

        string s =  cafe.getCafeStatus().csc.message((int32_T) mxGetScalar(prhs[1]));

        plhs[0]=mxCreateString( s.c_str() );

        //plhs[0] = mxCreateCellMatrix(1,1);
        //mxSetCell (plhs[0], 0, mxCreateString( s.c_str()));

        break;
    }

    case GET_STATUS_INFO:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n" );
            return;
        }

        string s =  cafe.getCafeStatus().csi.message((int32_T) mxGetScalar(prhs[1]));

        plhs[0]=mxCreateString( s.c_str() );

        //plhs[0] = mxCreateCellMatrix(1,1);
        //mxSetCell (plhs[0], 0, mxCreateString( s.c_str()));

        break;
    }

    case PRINT_STATUS:
    {

        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (nrhs==2)
        {

            if (!mxIsNumeric(prhs[1]) )
            {
                mexPrintf("Status does not have a numeric value \n");
                return;
            }

            NS_MOCHA::printStatus( (int32_T) mxGetScalar(prhs[1]));
        }
        else
        {
            handle=checkForHandle(prhs[1]);
            if(handle==0)
            {
                return;
            }
            cafe.printStatus(handle,  (int32_T) mxGetScalar(prhs[2]));
        }

        break;
    }


    case ALARM_STATUS_LIST :
    {
        if (nrhs>1)
        {
            mexPrintf("Method returns enumerated value/name pairs of alarm conditions \n");
        }

        vector<string> vecS;
        vector<int> vecI;
        cgac.getStates(vecI, vecS);

        plhs[1] = mxCreateNumericMatrix(1,vecI.size(), mxINT32_CLASS,mxREAL);
        longArray=  (int32_T *) mxGetData(plhs[1]);

        //return string list
        plhs[0] = mxCreateCellMatrix(1,vecS.size());

        for (mwSignedIndex i=0; i<min(vecI.size(),vecS.size()); ++i)
        {
            longArray[i]=vecI[i];
            mxSetCell (plhs[0], i, mxCreateString( vecS[i].c_str()));

        }

        break;
    }

    case ALARM_SEVERITY_LIST :
    {
        if (nrhs>1)
        {
            mexPrintf("Method returns enumerated value/name pairs of alarm severities \n");
        }

        vector<string> vecS;
        vector<int> vecI;
        cgas.getStates(vecI, vecS);

        plhs[1] = mxCreateNumericMatrix(1,vecI.size(), mxINT32_CLASS,mxREAL);
        longArray=  (int32_T *) mxGetData(plhs[1]);

        //return string list
        plhs[0] = mxCreateCellMatrix(1,vecS.size());

        for (mwSignedIndex i=0; i<min(vecI.size(),vecS.size()); ++i)
        {
            longArray[i]=vecI[i];
            mxSetCell (plhs[0], i, mxCreateString( vecS[i].c_str()));

        }

        break;
    }


    case STATUS_LIST :
    {
        if (nrhs>1)
        {
            mexPrintf("Method returns enumerated value/name pairs of status codes \n");
        }

        vector<string> vecS;
        vector<int> vecI;
        cafe.getCafeStatus().csc.getStates(vecI, vecS);

        plhs[1] = mxCreateNumericMatrix(1,vecI.size(), mxINT32_CLASS,mxREAL);
        longArray=  (int32_T *) mxGetData(plhs[1]);

        //return string list
        plhs[0] = mxCreateCellMatrix(1,vecS.size());

        for (mwSignedIndex i=0; i<min(vecI.size(),vecS.size()); ++i)
        {
            longArray[i]=vecI[i];
            mxSetCell (plhs[0], i, mxCreateString( vecS[i].c_str()));

        }

        break;
    }


    case STATUS_AS_STRING:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n");
            return;
        }

        string s =  cafe.getCafeStatus().asString((int32_T) mxGetScalar(prhs[1]));

        plhs[0]=mxCreateString( s.c_str() );

        break;
    }

    case ALARM_STATUS_AS_STRING:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n");
            return;
        }

        string s =  cgac.asString((int32_T) mxGetScalar(prhs[1]));

        plhs[0]=mxCreateString( s.c_str() );

        break;
    }

    case ALARM_SEVERITY_AS_STRING:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, status \n", nrhs);
            return;
        }

        if (!mxIsNumeric(prhs[1]) )
        {
            mexPrintf("Status does not have a numeric value \n");
            return;
        }

        string s =  cgas.asString((int32_T) mxGetScalar(prhs[1]));

        plhs[0]=mxCreateString( s.c_str() );

        break;
    }


    case GET_EPICS_TIMESTAMP:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            return;
        }

        //uint64_T ets[2];
        epicsTimeStamp ets;

        status=cafe.getHandleHelper().getTimeStamp(handle, ets);

        plhs[0] = mxCreateNumericMatrix(1,2, mxUINT32_CLASS,mxREAL);
        uint32_T * d=  (uint32_T *) mxGetData(plhs[0]);

        d[0] = (uint32_T) 0;
        d[1] = (uint32_T) 0;


        if (status==ICAFE_NORMAL)
        {
            d[0] = (uint32_T) ets.secPastEpoch;
            d[1] = (uint32_T) ets.nsec;
        }
        else
        {
            cafe.printStatus(handle,status);

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }
        break;
    }



    case GET_TIMESTAMP:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
            return;
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            return;
        }

        //uint64_T ets[2];
        epicsTimeStamp ets;

        status=cafe.getHandleHelper().getTimeStamp(handle, ets);

        plhs[0] = mxCreateNumericMatrix(1,7, mxUINT32_CLASS,mxREAL);
        uint32_T * d=  (uint32_T *) mxGetData(plhs[0]);

        d[0] = (uint32_T) 0;
        d[1] = (uint32_T) 0;
        d[2] = (uint32_T) 0;
        d[3] = (uint32_T) 0;
        d[4] = (uint32_T) 0;
        d[5] = (uint32_T) 0;
        d[6] = (uint32_T) 0;

        if (status==ICAFE_NORMAL)
        {

            epicsTime time(ets);

            local_tm_nano_sec local = (local_tm_nano_sec) time;

            d[0] = local.ansi_tm.tm_year + 1900;
            d[1] = local.ansi_tm.tm_mon  + 1;
            d[2] = local.ansi_tm.tm_mday;
            d[3] = local.ansi_tm.tm_hour;
            d[4] = local.ansi_tm.tm_min;
            d[5] = local.ansi_tm.tm_sec;
            d[6] = (uint32_T) ets.nsec;
        }
        else
        {
            cafe.printStatus(handle,status);

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }
        break;
    }

    /*
        case GET_PULSEID_FROM_TIMESTAMP:
        {
            if (nrhs<2) {
                mexPrintf("Invalid Number of argument %d. Input:  message, handle/pv \n", nrhs);
                return;
            }

            handle=checkForHandle(prhs[1]);
            if(handle==0) {return;}

               plhs[0] = mxCreateNumericMatrix(1,1, mxUINT32_CLASS,mxREAL);
            uint32_T * pulseID=  (uint32_T *) mxGetData(plhs[0]);

            status=cafe.getPulseID(handle, pulseID[0]);

        //status
            plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[1]);
            longStatus[0]=status;

            if (status!=ICAFE_NORMAL) {
                cafe.printStatus(handle,status);

    							if (withExceptions) {
    								mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
    							}
            }

            break;
        }


        case GET_PULSEID_FROM_TIMESTAMP_GROUP:
        {


            if (nrhs<2) {
                mexPrintf("Invalid no. of arguments %d. Input: message, ghandle/gname \n", nrhs);
                return;
            }

            ghandle=checkForGroupHandle(prhs[1]);
            if(ghandle==0) {
             return;
                 }


            PVGroup pvg;
             vector<unsigned int> vi;

            status=cafe.groupAttach(ghandle, pvg);

            if (status!=ECAFE_UNKNOWN_GROUP && status != ECAFE_EMPTY_GROUP) {

        	status=cafe.groupGetCache(ghandle, pvg);

        	vi=cafe.getPulseIDFromTS(pvg);

        }

               plhs[0] = mxCreateNumericMatrix(1,vi.size(), mxUINT32_CLASS,mxREAL);
            ulongArray=  (uint32_T *) mxGetData(plhs[0]);

            for (mwSignedIndex i=0; i<vi.size(); ++i) {
        	       ulongArray[i]=vi[i];
        }

        //status
            plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[1]);
            longStatus[0]=status;

            if (status!=ICAFE_NORMAL) {
                cafe.printStatus(handle,status);

    							if (withExceptions) {
    								mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
    							}
            }

            break;
        }
    */
    case SET_GROUP:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'setGroup', handle/group name, data array \n", nrhs);
            return;
        }

        ghandle=checkForGroupHandle(prhs[1]);
        if(ghandle==0)
        {
            return;
        }

        size_t numberOfInputElements=(nrhs-2);

        PVGroup pvgroup;

        status=cafe.groupAttach(ghandle, pvgroup);

        if (status!=ECAFE_UNKNOWN_GROUP && status != ECAFE_EMPTY_GROUP)
        {

            if (pvgroup.getNPV() != numberOfInputElements)
            {
                mexPrintf("Group has %d members while input has %d data elements\n",
                          pvgroup.getNPV(), numberOfInputElements);
                return;
            }

            PVDataHolder * du = pvgroup.getPVData();

            //fill pvGroup
            for (mwSignedIndex k=2; k<nrhs; ++k)
            {
                mxClassID mxid =mxGetClassID( prhs[k]);

                nelem=mxGetNumberOfElements (prhs[k]);

                //Have to repeat this everywhere!
                if(nelem==0)
                {
                    mexPrintf("Empty Data Array (element %d) in Input \n", (k-2+1));
                    return;
                }

                mwSignedIndex j=k-2;

                if (nelem < du[j].getNelem())   //changed from du[j].getNelemNative() which was removed
                {
                    du[j].setNelem(nelem);
                }

                mxArray * fout;
                switch(mxid)
                {
                case mxDOUBLE_CLASS:
                    inputDouble = (double *) mxGetPr (prhs[k]);
                    du[j].set(inputDouble);
                    break;
                case mxSINGLE_CLASS:
                    inputFloat =  (float *) mxGetData (prhs[k]);
                    du[j].set(inputFloat);
                    break;
                case mxLOGICAL_CLASS:
                    inputBool =   (bool *) mxGetData (prhs[k]);
                    fout = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
                    inputShort = (short *)  mxGetData ( fout );
                    for (mwSignedIndex i=0; i<nelem; ++i) inputShort[i]= (short) inputBool[i];
                    du[j].set(inputShort);
                    break;
                case mxINT8_CLASS:
                    inputChar =   (int8_T *) mxGetData (prhs[k]);
                    fout = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
                    inputShort = (short *)  mxGetData ( fout );
                    for (mwSignedIndex i=0; i<nelem; ++i) inputShort[i]= (short) inputChar[i];
                    du[j].set(inputShort);
                    break;
                case mxUINT8_CLASS:
                    inputUChar =  (unsigned char *)  mxGetData (prhs[k]);
                    du[j].set(inputUChar);
                    break;
                case mxINT16_CLASS:
                    inputShort =  (short *) mxGetData (prhs[k]);
                    du[j].set(inputShort);
                    break;
                case mxUINT16_CLASS:
                    inputUShort =  (unsigned short *)  mxGetData (prhs[k]);
                    du[j].set(inputUShort);
                    break;
                case mxINT32_CLASS:
                    inputLong =   (int32_T *) mxGetData (prhs[k]);
                    du[j].set(inputLong);
                    break;
                case mxUINT32_CLASS:
                    inputULong = (uint32_T *)  mxGetData (prhs[k]);
                    fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                    inputDouble = (double *)  mxGetData ( fout );
                    for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputULong[i];
                    du[j].set(inputDouble);
                    break;
                case mxINT64_CLASS:
                    inputLongLong = (int64_T *)  mxGetData (prhs[k]);
                    fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                    inputDouble = (double *)  mxGetData ( fout );
                    for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputLongLong[i];
                    du[j].set(inputDouble);
                    break;
                case mxUINT64_CLASS:
                    inputULongLong = (uint64_T *)  mxGetData (prhs[k]);
                    fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                    inputDouble = (double *)  mxGetData ( fout );
                    for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputULongLong[i];
                    du[j].set(inputDouble);
                    break;
                // Series of Strings go in a CELL


                case mxCELL_CLASS:
                {
                    neWithinAllCells=0;
                    size_t ne=mxGetNumberOfElements (prhs[k]);

                    if(ne==0)
                    {
                        mexPrintf("Empty Data Array (element %d) in Input \n", (k-2+1));
                    }


                    scanCell(prhs[k]);

                    inputStringCell = new dbr_string_t[neWithinAllCells];

                    if(ne!=neWithinAllCells)
                    {
                        du[j].setNelem(neWithinAllCells);
                        //  mexPrintf("Method cannot deal with this cell!");
                        // return;
                    }

                    cellToString(prhs[k]);


                    du[j].set(inputStringCell);
                    delete [] inputStringCell;
                    break;

                }
                // Single String
                case mxCHAR_CLASS:

                    dbr_string_t  inputString;;
                    mxGetString(prhs[k], inputString, sizeof(dbr_string_t));

                    du[j].setNelem(1);
                    du[j].set(inputString);

                    break;

                default:

                    break;
                } //switch

            } //for
            pvgroup.setPVData(du);

            status=cafe.groupSet(ghandle, pvgroup);

            PVDataHolder * pvd = pvgroup.getPVData();

            // statusGroup != ICAFE_SUCCESS when one or more channels accesses fail
            if ( pvgroup.getStatusGroup() !=  ICAFE_SUCCESS )
            {
                for (mwSignedIndex i=0; i<pvgroup.getNPV(); ++i)
                {
                    if (pvd[i].getStatus() != ICAFE_SUCCESS )
                    {
                        pvd[i].print();
                    }
                }
            }

            //status
            plhs[1] = mxCreateNumericMatrix(1,pvgroup.getNPV(),mxINT32_CLASS,mxREAL);
            longArray = (int32_T *) mxGetData ( plhs[1]);
            for (mwSignedIndex i=0; i<pvgroup.getNPV(); ++i)
            {
                longArray[i]=pvd[i].getStatus();
            }

            //status
            plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[0]);
            longStatus[0]=status;

        }

        else
        {
            NS_MOCHA::printStatus(status);
            plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
            doubleArray = mxGetPr ( plhs[1]);
            doubleArray[0]=numeric_limits<double>::signaling_NaN();

            //status
            plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[0]);
            longStatus[0]=status;
        }


        break;
    }

    case SET_STRUCT_ARRAY:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'setStructArray', handle/pv array, data array \n", nrhs);
            return;
        }


        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);

        if (handleULong==NULL)
        {
            mexPrintf("SET_STRUCT_ARRAY: Ignoring NULL value in input\n");
            mexPrintf("Input Invalid. Should be: 'setStructArray', handle/pv array, data array \n");
            return;
        }


        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be: 'setStructArray', handle/pv array, data array \n");
            return;
        }

        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);
        size_t nDataInputs=nrhs-2;

        //Examine data input
        //Break up a cell or an array
        //Check no of data inputs
        //nelem=nrhs-2;

        /*
        if (nhrs=3} {
          //neWithinAllDataCells=0;


        	if (mxIsCell(prhs[k])) {

        		nDataInputs=getObjectsFromDataCell(prhs[k]);

         		mexPrintf("Running No of Elements in Cell = %d \n", nDataInputs);

        	}
        	//else {
        	//	nDataInputs=nDataInputs+1;
        	//}
        }
        */


        //if (numberOfHandles != nrhs-2) {
        if (numberOfHandles != nDataInputs)
        {
            mexPrintf("Input Invalid. Data Array Length %d does not equal No of handles %d! \n", nrhs-2,numberOfHandles);
            //mexPrintf("Input Invalid. Data Array Length  %d does not equal No of handles %d! \n", nDataInputs,numberOfHandles);
            return;
        }

        PVDataHolder * du;

        //du = new PVDataHolder[nrhs-2];
        du = new PVDataHolder[nDataInputs];

        mwSignedIndex j=0;

        for (mwSignedIndex k=2; k<nrhs; ++k)
        {

            mwSignedIndex neWithinDataCell=0;

            mxClassID mxid = mxGetClassID(prhs[k]);
            nelem = mxGetNumberOfElements(prhs[k]);

            if(nelem==0)
            {
                mexPrintf("Empty Data Array (element %d) in Input \n", (k-2+1));
                return;
            }

            mxArray * dataInput =  (mxArray *) prhs[k];

            ////
            //mwSignedIndex j=j+1;

            mwSignedIndex j=k-2;

            du[j].setNelem(nelem);

            //mexPrintf("j=%d nelem= %d\n", j, nelem);

            mxArray * fout;

            switch(mxid)
            {
            case mxDOUBLE_CLASS:
                inputDouble = (double *) mxGetPr (dataInput);
                //for (int i=0; i<nelem; ++i) {
                //   mexPrintf("%f [%d] ",inputDouble[i],i);
                //}
                //mexPrintf("\n");
                du[j].set(inputDouble);
                break;
            case mxSINGLE_CLASS:
                inputFloat =  (float *) mxGetData (dataInput);
                du[j].set(inputFloat);
                break;
            case mxLOGICAL_CLASS:
                inputBool =   (bool *) mxGetData (dataInput);
                fout = mxCreateNumericMatrix(1,nelem,mxINT8_CLASS,  mxREAL);
                inputShort = (short *)  mxGetData ( fout );
                for (mwSignedIndex i=0; i<nelem; ++i) inputShort[i]= (short) inputBool[i];
                du[j].set(inputShort);
                break;
            case mxINT8_CLASS:
                inputChar =   (int8_T *) mxGetData (dataInput);
                fout = mxCreateNumericMatrix(1,nelem,mxINT16_CLASS,  mxREAL);
                inputShort = (short *)  mxGetData ( fout );
                for (mwSignedIndex i=0; i<nelem; ++i) inputShort[i]= (short) inputChar[i];
                du[j].set(inputShort);
                break;
            case mxUINT8_CLASS:
                inputUChar =  (unsigned char *)  mxGetData (dataInput);
                du[j].set(inputUChar);
                break;
            case mxINT16_CLASS:
                inputShort =  (short *) mxGetData (dataInput);
                du[j].set(inputShort);
                break;
            case mxUINT16_CLASS:
                inputUShort =  (unsigned short *)  mxGetData (dataInput);
                du[j].set(inputUShort);
                break;
            case mxINT32_CLASS:
                inputLong =   (int32_T *) mxGetData (dataInput);
                du[j].set(inputLong);
                break;
            case mxUINT32_CLASS:
                inputULong = (uint32_T *)  mxGetData (dataInput);
                fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                inputDouble = (double *)  mxGetData ( fout );
                for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputULong[i];
                du[j].set(inputDouble);
                break;
            case mxINT64_CLASS:
                inputLongLong = (int64_T *)  mxGetData (dataInput);
                fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                inputDouble = (double *)  mxGetData ( fout );
                for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputLongLong[i];
                du[j].set(inputDouble);
                break;
            case mxUINT64_CLASS:
                inputULongLong = (uint64_T *)  mxGetData (dataInput);
                fout = mxCreateDoubleMatrix(1,nelem,mxREAL);
                inputDouble = (double *)  mxGetData ( fout );
                for (mwSignedIndex i=0; i<nelem; ++i) inputDouble[i]= (double) inputULongLong[i];
                du[j].set(inputDouble);
                break;
            // Series of Strings go in a CELL

            case mxCELL_CLASS:
            {
                neWithinAllCells=0;
                size_t ne=mxGetNumberOfElements (dataInput);

                if(ne==0)
                {
                    mexPrintf("Empty Data Array (element %d) in Input \n", (j+1));
                }

                scanCell(dataInput);

                inputStringCell = new dbr_string_t[neWithinAllCells];

                if(ne!=neWithinAllCells)
                {
                    du[j].setNelem(neWithinAllCells);

                }

                cellToString(dataInput);

                du[j].set(inputStringCell);
                delete [] inputStringCell;
                break;

            }
            // Single String
            case mxCHAR_CLASS:

                dbr_string_t  inputString;;
                mxGetString(dataInput, inputString, sizeof(dbr_string_t));

                du[j].setNelem(1);
                du[j].set(inputString);

                break;

            default:

                break;
            } //switch

            //} //for nDataInputs
        }  //for


        status=cafe.set(handleULong, numberOfHandles, du);

        //mexPrintf("nelem=%d", du[0].getNelem());
        //printStatus(status);
        //status
        plhs[1] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,mxREAL);
        longArray = (int32_T *) mxGetData ( plhs[1]);
        for (mwSignedIndex i=0; i<numberOfHandles; ++i)
        {


            longArray[i]=du[i].getStatus();
            //Use shared_ptr since cafe-1.0
            //delete [] du[i].val;
        }

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;

        delete [] du;
        break;

    }

    case SET_SCALAR_ARRAY:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. \nInput: 'setScalarArray', handle/pv array, data array \n", nrhs);
            return;
        }
        else if (nrhs>3)
        {
            mexPrintf("Ignoring arguments after the 3rd from the %d entered. \nInput: 'setScalarArray', handle/pv array, data array \n", nrhs);
        }

        //What is dataType?
        handleULong = checkHandleArray(prhs[1],true);

        if (handleULong==NULL)
        {
            mexPrintf("SET_SCALAR_ARRAY: Ignoring NULL value in input \n");
            mexPrintf("Input Invalid. Should be: 'setScalarArray', handle/pv array, data array \n");
            return;
        }


        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be: 'setScalarArray', handle/pv array, data array \n");
            return;
        }

        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);

        nelem=mxGetNumberOfElements (prhs[2]);

        if(nelem==0)
        {
            mexPrintf("Empty Data Array in Input \n");
            return;
        }

        if (numberOfHandles != nelem)
        {
            mexPrintf("Input Invalid. Data Array Length  %d does not equal No of handles %d! \n", nelem,numberOfHandles);
            return;
        }


        neWithinAllCells=0;

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else if (mxIsCell(prhs[1]) )
        {
            scanCell(prhs[1]);
            numberOfHandles=neWithinAllCells;
            //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
        }



        //statuses
        plhs[1] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,mxREAL);
        longArray = (int32_T *) mxGetData (plhs[1]);

        mxClassID mxid =mxGetClassID(prhs[2]);

        mxArray * fout;

        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputDouble, (int *) longArray);
            break;
        case mxSINGLE_CLASS:
            inputFloat =  (float *) mxGetData (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputFloat, (int *) longArray);
            break;
        case mxLOGICAL_CLASS:
            inputBool =   (bool *) mxGetData (prhs[2]);
            fout = mxCreateNumericMatrix(1,numberOfHandles,mxINT8_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<numberOfHandles; ++i) inputShort[i]= (short) inputBool[i];
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputShort, (int *) longArray);
            break;
        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData ( prhs[2]);
            fout = mxCreateNumericMatrix(1,numberOfHandles,mxINT16_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<numberOfHandles; ++i) inputShort[i]= (short) inputChar[i];
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputShort, (int *) longArray);
            break;
        case mxUINT8_CLASS:
            inputUChar =  (unsigned char *)  mxGetData (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputUChar, (int *) longArray);
            break;
        case mxINT16_CLASS:
            inputShort =  (short *) mxGetData (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputShort, (int *) longArray);
            break;
        case mxUINT16_CLASS:
            inputUShort =  (unsigned short *)  mxGetData (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputUShort, (int *) longArray);
            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (prhs[2]);
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputLong, (int *) longArray);
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,numberOfHandles,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<numberOfHandles; ++i) inputDouble[i]= (double) inputULong[i];
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputDouble, (int *) longArray);
            break;
        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,numberOfHandles,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<numberOfHandles; ++i) inputDouble[i]= (double) inputLongLong[i];
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputDouble, (int *) longArray);
            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,numberOfHandles,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<numberOfHandles; ++i) inputDouble[i]= (double) inputULongLong[i];
            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputDouble, (int *) longArray);
            break;
        // Series of Strings go in a CELL


        case mxCELL_CLASS:
        {
            neWithinAllCells=0;
            size_t ne=mxGetNumberOfElements (prhs[2]);

            if(ne==0)
            {
                mexPrintf("Empty Data Array in Input \n");
            }

            scanCell(prhs[2]);

            inputStringCell = new dbr_string_t[neWithinAllCells];

            if(ne!=neWithinAllCells)
            {
                mexPrintf("Method cannot deal with this cell! \n");
                return;
            }


            cellToString(prhs[2]);

            status=cafe.set((unsigned  int *)handleULong, (unsigned  int) numberOfHandles, inputStringCell, (int *) longArray);

            delete [] inputStringCell;

            break;

        }
        // Single String
        case mxCHAR_CLASS:

            dbr_string_t  inputString;;
            mxGetString(prhs[2], inputString, sizeof(dbr_string_t));
            status=cafe.set(handle, inputString);

            break;

        default:

            break;
        }

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;


        //mexPrintf("status = %d", status);
        //delete [] handleULong;

        break;
    }

    case SET:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'set', handle/pv, data \n", nrhs);
            return;
        }
        else if (nrhs>3)
        {
            mexPrintf("Ignoring arguments after the 3rd from the %d entered. Input: 'set', handle/pv, data \n", nrhs);
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            return;
        }


        if (!isValidClass(prhs[2])) return;

        size_t ne = mxGetNumberOfElements ( prhs[2]);

        if (ne==0)
        {
            mexPrintf("Empty Data Array in Input \n");
            return;
        }


        neWithinAllCells=0;
        iWithinAllCells=0;

        mxClassID mxid =mxGetClassID(prhs[2]);
        
       
        //This line is important for a string as the Number of Elements
        //is interpreted as the number of characters!!!
        //Hence for ne to be 1
        //Unless we have the special case of wf and DBR_CHAR
        //So let us first check if this is a waveform
             
        unsigned short isWFWithString=0;

        if (mxid==mxCHAR_CLASS)
        {
	   ChannelRegalia channelInfo;
	   status=cafe.getChannelInfo(handle, channelInfo);      
           //mexPrintf(channelInfo.getClassNameAsString().c_str());
           if (channelInfo.getClassNameAsString().compare("waveform") !=0) {
               ne=1;	       
	   }
           else {
	     isWFWithString = 1;
	   }
        }

        nelem = cafe.getHandleHelper().getNelemClient(handle);

        //handle does not exist
        if (nelem==0)
        {
            //status
            plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[0]);
            longStatus[0]=ECAFE_INVALID_HANDLE;

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(ECAFE_INVALID_HANDLE).c_str(), prepareExceptionText(handle,ECAFE_INVALID_HANDLE).c_str());
            }

            return;
        }

        if (nelem!=ne)
        {
            cafe.getHandleHelper().setNelem(handle,ne);
        }


        mxArray *fout;
        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr (prhs[2]);
            status=cafe.set(handle,inputDouble);
            break;
        case mxSINGLE_CLASS:
            inputFloat =  (float *) mxGetData (prhs[2]);
            status=cafe.set(handle,inputFloat);
            break;
        case mxLOGICAL_CLASS:
            inputBool =   (bool *) mxGetData (prhs[2]);
            fout = mxCreateNumericMatrix(1,ne,mxINT8_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputShort[i]= (short) inputBool[i];
            status=cafe.set(handle,inputShort);
            break;
        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData ( prhs[2]);
            fout = mxCreateNumericMatrix(1,ne,mxINT16_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputShort[i]= (short) inputChar[i];
            status=cafe.set(handle,inputShort);
            break;
        case mxUINT8_CLASS:
            inputUChar =  (unsigned char *)  mxGetData (prhs[2]);
            status=cafe.set(handle,inputUChar);
            break;
        case mxINT16_CLASS:
            inputShort =  (short *) mxGetData (prhs[2]);
            status=cafe.set(handle,inputShort);
            break;
        case mxUINT16_CLASS:
            inputUShort =  (unsigned short *)  mxGetData (prhs[2]);
            status=cafe.set(handle,inputUShort);
            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (prhs[2]);
            status=cafe.set(handle,inputLong);
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (prhs[2]);

            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputULong[i];
            status=cafe.set(handle,inputDouble);
            break;

        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputLongLong[i];
            status=cafe.set(handle,inputDouble);
            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputULongLong[i];
            status=cafe.set(handle,inputDouble);
            break;
        // Series of Strings go in a CELL


        case mxCELL_CLASS:
        {
            ////neWithinAllCells=0;
            size_t ne=mxGetNumberOfElements (prhs[2]);

            if(ne==0)
            {
                mexPrintf("Empty Data Array in Input \n");
            }


            scanCell(prhs[2]);

            //Is it a waveform??
            //If neWithinAllCells =1 and wf and class is mx_char

            inputStringCell = new dbr_string_t[neWithinAllCells];

            cellToString(prhs[2]);

            if(neWithinAllCells!=ne)
            {
                cafe.getHandleHelper().setNelem(handle,neWithinAllCells);
            }

            status=cafe.set(handle, inputStringCell);
            delete [] inputStringCell;

            break;
        }
        // Single String
        case mxCHAR_CLASS:
          char *buf;
          
          size_t buflen;
          buflen = mxGetN(prhs[2]) + 1;
          /*
	  buf = (char *) mxMalloc(buflen);

          long mexStatus;

          mexStatus = mxGetString(prhs[2], buf, (mwSize) buflen+10);
	  if (mexStatus != 0) {
            mexWarnMsgIdAndTxt( "MATLAB:mxmalloc:mxGetString", 
                           "Failed to copy input string into allocated memory.");
            status = ECAFE_BADSTR;
          }
	  */
          buf=mxArrayToString(prhs[2]);
         
	  if (isWFWithString == 1) {
            //std::string tempStr = inputString;
	    dbr_char_t * wfChar =  new dbr_char_t [buflen+1];
            //dbr_char_t * setStrValues =  (dbr_char_t *) mxMalloc(buflen+40); // new dbr_char_t  [buflen];
	    std::string wfStr = buf; 
            unsigned short iCount = 0;
	    for (std::string::size_type i=0; i < wfStr.size(); ++i) {
              char c =  wfStr[i];
             
              char cNext;
              if (c == '\\' && i <  (wfStr.size()-1)) {
		
	         cNext = wfStr[i+1]; 
                 bool recognizedFlag = true; 
                 wfChar[iCount] = NS_MOCHA::getEscapeSequence(cNext, recognizedFlag);
                 //mexPrintf ("wf %d",  wfChar[iCount]);
                 /*
		 switch (cNext) {
		    case '0':
		     wfChar[iCount] = '\0'; 
                     break;
		   case '\'':
		     wfChar[iCount] = '\''; 
                     break;
                   case '"':
		     wfChar[iCount] = '\"'; 
                     break;
		   case '?':
		     wfChar[iCount] = '\?'; 
                     break;
		   case '\\':
		     wfChar[iCount] = '\\'; 
                     break;
                   case 'a':
		     wfChar[iCount] = '\a'; 
                     break;
		   case 'b':
		     wfChar[iCount] = '\b'; 
                     break;
                   case 'f':
		     wfChar[iCount] = '\f'; 
                     break;
		   case 'n':
		     wfChar[iCount] = '\n'; 
                     break;
                   case 'r':
		     wfChar[iCount] = '\r'; 
                     break;
		   case 't':
                     mexPrintf ("wf %c",  wfChar[iCount]);
		     wfChar[iCount] = '\t'; 
                     mexPrintf ("wf %c",  wfChar[iCount]);
                     break;
                   case 'v':
		     wfChar[iCount] = '\v'; 
                     break;
		   default:
                     // Unrecognized escape sequence; print slash and subsequent letter
		     wfChar[iCount] = '\\'; 
                     --i;
		     break;
                 }
                 */
		 //if (wfChar[iCount] !=  '\\') {
		 if (recognizedFlag) {
                    ++i;
                    // mexPrintf ("wfplus i  %d %d",  wfChar[iCount], i);
		 }	        
              } 
              else {
		wfChar[iCount] = c; 
              }     
              ++iCount; 
	      //setStrValues[i] = buf[i];  
	    }

            

            cafe.getHandleHelper().setNelem(handle, iCount);
	    //std::string tb = buf;
            //strcpy(  (char*)(setStrValues), (const char*) buf); //"Hello \nWorld\0" );
            //setStrValues =(dbr_char_t *) tb.c_str(); //"Hello\tWorld\nA\0"; //(dbr_char_t *) buf; //
	    status = cafe.set(handle, wfChar); // ) (dbr_char_t *) buf);
           
            delete [] wfChar;
            // mxFree(setStrValues);
            
          }
          else {           
            status=cafe.set(handle, buf);
          }
           
         
          mxFree(buf);
 
          break;

        default:
	  mexPrintf("mxClassID=%s (enum %d) is not supported; set not instigated!\n",
                    mxGetClassName(prhs[2]), mxid);
          mexPrintf("mxClassID=%s (enum %d) is supported; i.e., use single instead of double quotes!\n",
                    "char", mxCHAR_CLASS); 

	  status = ECAFE_INVALID_SWITCH_CASE;
          break;
        }

       
        if(nelem!=ne) cafe.getHandleHelper().setNelem(handle,nelem); //back to old value

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;

        if (status != ICAFE_NORMAL)
        {
            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }


        break;
    }


    case SET_AND_GET:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'set', handle/pv, data \n", nrhs);
            return;
        }
        else if (nrhs>3)
        {
            mexPrintf("Ignoring arguments after the 3rd from the %d entered. Input: 'set', handle/pv, data \n", nrhs);
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            return;
        }


        if (!isValidClass(prhs[2])) return;

        size_t ne = mxGetNumberOfElements ( prhs[2]);

        if(ne==0)
        {
            mexPrintf("Empty Data Array in Input \n");
            return;
        }

        neWithinAllCells=0;
        iWithinAllCells=0;

        mxClassID mxid =mxGetClassID( prhs[2]);

        //This line is important for a string as the Number of Elements
        //is interpreted as the number of characters!!!
        //Hence for ne to be 1
        if (mxid==mxCHAR_CLASS)
        {
            ne=1;
        }

        nelem = cafe.getHandleHelper().getNelemClient(handle);


        //handle does not exist
        if(nelem==0)
        {


            plhs[0] = mxCreateDoubleMatrix(1,nelem,mxREAL);
            doubleArray = (double *) mxGetPr (plhs[0]);
            doubleArray[0]=numeric_limits<double>::signaling_NaN();

            //status
            plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
            longStatus = (int32_T *) mxGetData ( plhs[1]);
            longStatus[0]=ECAFE_INVALID_HANDLE;

            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(ECAFE_INVALID_HANDLE).c_str(), prepareExceptionText(handle,ECAFE_INVALID_HANDLE).c_str());
            }

            return;
        }

        if(nelem!=ne)
        {
            cafe.getHandleHelper().setNelem(handle,ne);
        }

        size_t neOne=1; //Only deal with first element for the waveform!!

        mxArray *fout;
        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr (prhs[2]);

            plhs[0] = mxCreateDoubleMatrix(1,ne,mxREAL);
            doubleArray = (double *) mxGetPr (plhs[0]);

            status=cafe.setAndGet(handle,inputDouble[0], doubleArray[0]);
            break;
        case mxSINGLE_CLASS:
            inputFloat =  (float *) mxGetData (prhs[2]);

            plhs[0]  = mxCreateNumericMatrix(1,ne,mxSINGLE_CLASS,  mxREAL);
            floatArray = (float *) mxGetData (plhs[0]);

            status=cafe.setAndGet(handle,inputFloat[0], floatArray[0]);
            break;
        case mxLOGICAL_CLASS:
            inputBool =   (bool *) mxGetData (prhs[2]);
            fout = mxCreateNumericMatrix(1,ne,mxINT8_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputShort[i]= (short) inputBool[i];


            plhs[0] = mxCreateNumericMatrix(1,neOne,mxLOGICAL_CLASS,mxREAL);
            logicalData = (mxLogical *) mxGetData (plhs[0]);

            shortArray = new short [ne];

            status=cafe.setAndGet(handle,inputShort[0], shortArray[0]);

            //mexPrintf(" Value =  %d" , shortArray[0] );
            //mexPrintf(" Value =  %d" , (mxLogical) shortArray[0] );

            //for (mwSignedIndex i=0; i< neOne; ++i) { logicalData[i]= mxCreateLogicalScalar( (bool) shortArray[i]); }
            logicalData[0]=  (mxLogical) shortArray[0];
            //mexPrintf(" Value =  %d" , logicalData[0] );

            delete [] shortArray;

            break;

        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData ( prhs[2]);
            fout = mxCreateNumericMatrix(1,ne,mxINT16_CLASS,  mxREAL);
            inputShort = (short *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputShort[i]= (short) inputChar[i];

            plhs[0] = mxCreateNumericMatrix(1,ne,mxINT8_CLASS,  mxREAL);
            charArray = (int8_T *)  mxGetData (plhs[0]);
            shortArray = new short [ne];

            status=cafe.setAndGet(handle,inputShort[0],shortArray[0]);

            for (mwSignedIndex i=0; i< neOne; ++i)
            {
                charArray[i]= (int8_T) shortArray[i];
            }
            delete [] shortArray;

            break;

        case mxUINT8_CLASS:
            inputUChar =  (unsigned char *)  mxGetData (prhs[2]);
            plhs[0] = mxCreateNumericMatrix(1,ne,mxUINT8_CLASS,  mxREAL);
            ucharArray = (uint8_T *)  mxGetData (plhs[0]);

            status=cafe.setAndGet(handle,inputUChar[0], ucharArray[0]);
            break;
        case mxINT16_CLASS:
            inputShort =  (short *) mxGetData (prhs[2]);
            plhs[0] = mxCreateNumericMatrix(1,ne,mxINT16_CLASS,  mxREAL);
            shortArray = (int16_T *)  mxGetData (plhs[0]);
            status=cafe.setAndGet(handle,inputShort[0],shortArray[0]);
            break;
        case mxUINT16_CLASS:
            inputUShort =  (unsigned short *)  mxGetData (prhs[2]);
            plhs[0] = mxCreateNumericMatrix(1,ne,mxUINT16_CLASS,  mxREAL);
            ushortArray = (uint16_T *)  mxGetData (plhs[0]);
            status=cafe.setAndGet(handle,inputUShort[0],ushortArray[0]);
            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (prhs[2]);
            plhs[0]  = mxCreateNumericMatrix(1,ne,mxINT32_CLASS,  mxREAL);
            longArray = (int32_T *)  mxGetData (plhs[0] );
            status=cafe.setAndGet(handle,inputLong[0], longArray[0]);
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (prhs[2]);

            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputULong[i];

            plhs[0] = mxCreateNumericMatrix(1,ne,mxUINT32_CLASS,  mxREAL);
            ulongArray = (uint32_T *)  mxGetData (plhs[0]);
            doubleArray = new double[ne];

            status=cafe.setAndGet(handle,inputDouble[0], doubleArray[0]);
            for (mwSignedIndex i=0; i< neOne; ++i)
            {
                ulongArray[i]= (uint32_T) doubleArray[i];
            }
            delete [] doubleArray;

            break;

        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputLongLong[i];

            plhs[0] = mxCreateNumericMatrix(1,ne,mxINT64_CLASS,  mxREAL);
            longlongArray = (int64_T *)  mxGetData (plhs[0]);
            doubleArray = new double[ne];

            status=cafe.setAndGet(handle,inputDouble[0], doubleArray[0]);
            for (mwSignedIndex i=0; i< neOne; ++i)
            {
                longlongArray[i]= (int64_T) doubleArray[i];
            }
            delete [] doubleArray;

            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (prhs[2]);
            fout = mxCreateDoubleMatrix(1,ne,mxREAL);
            inputDouble = (double *)  mxGetData ( fout );
            for (mwSignedIndex i=0; i<ne; ++i) inputDouble[i]= (double) inputULongLong[i];

            plhs[0] = mxCreateNumericMatrix(1,ne,mxUINT64_CLASS,  mxREAL);
            ulonglongArray = (uint64_T *)  mxGetData (plhs[0]);
            doubleArray = new double[ne];


            status=cafe.setAndGet(handle,inputDouble[0], doubleArray[0]);
            for (mwSignedIndex i=0; i< neOne; ++i)
            {
                ulonglongArray[i]= (uint64_T) doubleArray[i];
            }
            delete  [] doubleArray;

            break;
        // Series of Strings go in a CELL


        case mxCELL_CLASS:
        {
            ////neWithinAllCells=0;
            size_t ne=mxGetNumberOfElements (prhs[2]);

            if(ne==0)
            {
                mexPrintf("Empty Data Array in Input \n");
            }


            scanCell(prhs[2]);

            inputStringCell = new dbr_string_t[neWithinAllCells];

            cellToString(prhs[2]);

            if(neWithinAllCells!=ne)
            {
                cafe.getHandleHelper().setNelem(handle,neWithinAllCells);
            }


            dbr_string_t * stringArrayc = new dbr_string_t[neWithinAllCells];

            status=cafe.setAndGet(handle, inputStringCell[0], stringArrayc[0]);

            plhs[0] = mxCreateCellMatrix(1,ne);
            for (mwSignedIndex i=0; i <neOne; ++i)
            {
                mxSetCell (plhs[0], i, mxCreateString( stringArrayc[i] ));
            }

            delete [] stringArrayc;

            delete [] inputStringCell;

            break;
        }
        // Single String
        case mxCHAR_CLASS:
            dbr_string_t  inputString;
            dbr_string_t  outputString;;
            mxGetString(prhs[2], inputString, sizeof(dbr_string_t));

            status=cafe.setAndGet(handle, inputString, outputString);

            plhs[0]=mxCreateString( outputString);

            break;

        default:

            break;
        }

        if(nelem!=ne) cafe.getHandleHelper().setNelem(handle,nelem); //back to old value

        //status
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[1]);
        longStatus[0]=status;

        if (status != ICAFE_NORMAL)
        {
            if (withExceptions)
            {
                mexErrMsgIdAndTxt(prepareExceptionID(status).c_str(), prepareExceptionText(handle,status).c_str());
            }
        }


        break;
    }


    case SET_AND_MATCH:
    {
        if (nrhs<7)
        {
            mexPrintf("Invalid Number of argument %d.\n", nrhs);
            mexPrintf("Input: 'setAndmatch', handle/pv[], values[], readback handle/pv[], tolerance, timeout, printFlag \n");
            return;
        }

        if (mxIsEmpty(prhs[1]))
        {
            mexPrintf("SET_AND_MATCH: 2nd input argument is empty!\n");
            return;
        }

        else if (mxIsEmpty(prhs[2]))
        {
            mexPrintf("SET_AND_MATCH: 3rd input argument is empty!\n");
            return;
        }

        else if (mxIsEmpty(prhs[3]))
        {
            mexPrintf("SET_AND_MATCH: 4th input argument is empty!\n");
            return;
        }


        //input handles
        handleULong = checkHandleArray(prhs[1],true);

        if (handleULong==NULL)
        {
            mexPrintf("SET_AND_MATCH: Ignoring NULL value in input for handles\n");
            mexPrintf("Input: 'setAndmatch', handle/pv[], values[], readback handle/pv[], tolerance, timeout, printFlag \n");
            return;
        }

        if(handleULong[0]==0)
        {
            mexPrintf("SET_AND_MATCH: 0 is an invalid handle \n");
            mexPrintf("Input: 'setAndmatch', handle/pv[], values[], readback handle/pv[], tolerance, timeout, printFlag \n");
            return;
        }
        //readback handles
        handleULongReadback = checkHandleArray(prhs[3],true);

        if (handleULongReadback==NULL)
        {
            mexPrintf("SET_AND_MATCH: Ignoring NULL value in input for readback handles\n");
            mexPrintf("Input: 'setAndmatch', handle/pv[], values[], readback handle/pv[], tolerance, timeout, printFlag \n");
            return;
        }

        if(handleULongReadback[0]==0)
        {
            mexPrintf("SET_AND_MATCH: 0 is an invalid readback handle \n");
            mexPrintf("Input: 'setAndmatch', handle/pv[], values[], readback handle/pv[], tolerance, timeout, printFlag \n");
            return;
        }


        //input handles
        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);



        neWithinAllCells=0;

        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else if (mxIsCell(prhs[1]) )
        {
            scanCell(prhs[1]);
            numberOfHandles=neWithinAllCells;
        }
        //readback handles
        size_t numberOfHandlesReadback=mxGetNumberOfElements (prhs[3]);



        neWithinAllCells=0;

        if (mxIsChar(prhs[3]) )
        {
            numberOfHandlesReadback=1;
        }
        else if (mxIsCell(prhs[3]) )
        {
            scanCell(prhs[3]);
            numberOfHandlesReadback=neWithinAllCells;
        }


        if (numberOfHandles!=numberOfHandlesReadback)
        {
            mexPrintf("SET_AND_MATCH: Number of input handles/pvs %d does not equal the number of readback handles %d \n",numberOfHandles,numberOfHandlesReadback);
            return;
        }

        //mexPrintf(" nh %d nhrb %d \n", numberOfHandles, numberOfHandlesReadback);

        if (!isValidClass(prhs[2]))
        {
            return;
        }

        size_t ne = mxGetNumberOfElements ( prhs[2]);

        if(ne==0)
        {
            mexPrintf("SET_AND_MATCH: Empty Data Array in Input \n");
            return;
        }


        mxClassID mxid =mxGetClassID( prhs[2]);
        if (mxid==mxLOGICAL_CLASS)
        {
            mexPrintf("SET_AND_MATCH: Data Array in Input of MATLAB type mxLOGICAL is not supported for this method!\n");
        }

        //This line is important for a string as the Number of Elements
        //is interpreted as the number of characters!!!
        //Hence for ne to be 1
        if (mxid==mxCHAR_CLASS)
        {
            ne=1;
        }

        if (ne != numberOfHandles)
        {
            mexPrintf("SET_AND_MATCH: Number of input values %d does not equal the number of handles %d \n",ne,numberOfHandles);
            return;
        }

        vector<unsigned int> hV;
        hV.clear();
        hV.reserve(numberOfHandles);
        vector<unsigned int> hrbV;
        hrbV.clear();
        hrbV.reserve(numberOfHandles);
        for (size_t i=0; i<numberOfHandles; ++i)
        {
            hV.push_back  (handleULong[i]);
            hrbV.push_back(handleULongReadback[i] );
        }

        double timeoutLocal= (double) mxGetScalar(prhs[5]);
        bool printFlag = mxIsLogicalScalarTrue  (prhs[6]);
        vector<dbr_double_t> doubleV;
        doubleV.clear();
        vector<dbr_float_t> floatV;
        floatV.clear();
        vector<dbr_short_t> shortV;
        shortV.clear();
        vector<dbr_enum_t> ushortV;
        ushortV.clear();
        vector<unsigned char> ucharV;
        ucharV.clear();
        vector<dbr_long_t> longV;
        longV.clear();
        vector<long long> longlongV;
        longlongV.clear();
        vector<string> stringV;
        stringV.clear();

        switch(mxid)
        {
        case mxDOUBLE_CLASS:
            inputDouble = (double *) mxGetPr (prhs[2]);
            doubleV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                doubleV.push_back(inputDouble[i]);
            }
            status=cafe.setAndMatchMany(hV, doubleV, hrbV, (dbr_double_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;

        case mxSINGLE_CLASS:
            inputFloat =  (float *) mxGetData (prhs[2]);
            floatV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                floatV.push_back(inputFloat[i]);
            }
            status=cafe.setAndMatchMany(hV, floatV, hrbV, (dbr_float_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;

        case mxINT8_CLASS:
            inputChar =   (int8_T *) mxGetData (prhs[2]); //dbr_char_t is uint8;
            shortV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                shortV.push_back((dbr_short_t) inputChar[i]);
            }
            status=cafe.setAndMatchMany(hV, shortV, hrbV, (dbr_short_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        case mxUINT8_CLASS:
            inputUChar =  (unsigned char *)  mxGetData (prhs[2]);
            ucharV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                ucharV.push_back(inputUChar[i]);
            }

            status=cafe.setAndMatchMany(hV, ucharV, hrbV, (dbr_char_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);

            break;
        case mxINT16_CLASS:
            inputShort =  (short *) mxGetData (prhs[2]);
            shortV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                shortV.push_back(inputShort[i]);
            }
            status=cafe.setAndMatchMany(hV, shortV, hrbV, (dbr_short_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        case mxUINT16_CLASS:
            inputUShort =  (unsigned short *)  mxGetData (prhs[2]);
            ushortV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                ushortV.push_back(inputUShort[i]);
            }
            status=cafe.setAndMatchMany(hV, ushortV, hrbV, (dbr_short_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);

            break;
        case mxINT32_CLASS:
            inputLong =   (int32_T *) mxGetData (prhs[2]);
            longV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                longV.push_back(inputLong[i]);
            }
            status=cafe.setAndMatchMany(hV, longV, hrbV, (dbr_long_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        case mxUINT32_CLASS:
            inputULong = (uint32_T *)  mxGetData (prhs[2]);
            doubleV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                doubleV.push_back((dbr_double_t) inputULong[i]);
            }
            status=cafe.setAndMatchMany(hV, doubleV, hrbV, (dbr_double_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        case mxINT64_CLASS:
            inputLongLong = (int64_T *)  mxGetData (prhs[2]);
            doubleV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                doubleV.push_back((dbr_double_t) inputLongLong[i]);
            }
            status=cafe.setAndMatchMany(hV, doubleV, hrbV, (dbr_double_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        case mxUINT64_CLASS:
            inputULongLong = (uint64_T *)  mxGetData (prhs[2]);
            doubleV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                doubleV.push_back((dbr_double_t) inputULongLong[i]);
            }
            status=cafe.setAndMatchMany(hV, doubleV, hrbV, (dbr_double_t) mxGetScalar(prhs[4]), timeoutLocal, printFlag);
            break;
        // Series of Strings go in a CELL

        case mxCELL_CLASS:
        {
            neWithinAllCells=0;
            size_t ne=mxGetNumberOfElements (prhs[2]);

            if(ne==0)
            {
                mexPrintf("Empty Data Array in Input \n");
            }

            scanCell(prhs[2]);

            inputStringCell = new dbr_string_t[neWithinAllCells];

            if(ne!=neWithinAllCells)
            {
                mexPrintf("Method cannot deal with this cell! \n");
                return;
            }


            cellToString(prhs[2]);

            stringV.reserve(numberOfHandles);
            for (size_t i=0; i<numberOfHandles; ++i)
            {
                stringV.push_back((string) inputStringCell[i]);
            }

            dbr_string_t  toleranceString;

            //Convert tolerance to string if required
            if (mxIsNumeric(prhs[4]) )
            {
                stringstream ss;
                ss << mxGetScalar(prhs[4]);
                string strTol = ss.str();
                strcpy(toleranceString,strTol.c_str());
            }
            else
            {

                mxGetString(prhs[4], toleranceString, sizeof(dbr_string_t));
            }

            status=cafe.setAndMatchMany(hV, stringV, hrbV, (string) toleranceString, timeoutLocal, printFlag);


            delete [] inputStringCell;

            break;

        }
        // Single String
        case mxCHAR_CLASS:

            dbr_string_t  inputString;;
            mxGetString(prhs[2], inputString, sizeof(dbr_string_t));

            dbr_string_t  toleranceString;

            //Convert tolerance to string if required
            if (mxIsNumeric(prhs[4]) )
            {
                stringstream ss;
                ss << mxGetScalar(prhs[4]);

                string strTol = ss.str();
                strcpy(toleranceString,strTol.c_str());

            }
            else
            {

                mxGetString(prhs[4], toleranceString, sizeof(dbr_string_t));
            }

            stringV.push_back(inputString);

            status=cafe.setAndMatchMany(hV, stringV, hrbV, (string) toleranceString, timeoutLocal, printFlag);

            break;

        default:

            break;
        }


        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData (plhs[0]);
        longStatus[0]=status;

        break;
    }


    case MONITOR:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of argument %d. Input: 'monitor', handle/pv, monitorAction(optional) \n", nrhs);
            return;
        }

        //
        handleULong = checkHandleArray(prhs[1],true);

        if (handleULong==NULL)
        {
            mexPrintf("MONITOR: Ignoring NULL value in input \n");
            mexPrintf("Input Invalid. Should be: 'monitor', handle/pv array \n");
            return;
        }

        if(handleULong[0]==0)
        {
            mexPrintf("Input Invalid. Should be: 'monitor', handle/pv array \n");
            return;
        }


        size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);


        if (mxIsChar(prhs[1]) )
        {
            numberOfHandles=1;
        }
        else if (mxIsCell(prhs[1]) )
        {
            numberOfHandles=neWithinAllCells;
        }


        //In case of multiple actions for multiple handles

        if (nrhs>2)
        {

            for (mwSize i=2; i<nrhs; ++ i)
            {

                if ( (i-1) > numberOfHandles)
                {
                    mexPrintf("Handle does not exists for monAction: %s\n",  monAction);

                }

                if (!mxIsChar(prhs[i]) )
                {
                    mexPrintf("monitorAction does not have a string value \n" );
                    mexPrintf("Input:  'monitor', handle/pv, monitorAction(optional)\n" );
                    return;
                }
                //message
                mData = (mxArray *)  prhs[i];
                messLength = MONITOR_ACTION_MAX_SIZE;
                mxGetString(mData, monAction, messLength);

                char monActionOriginal[MONITOR_ACTION_MAX_SIZE];
                strcpy(monActionOriginal,monAction);

                //remove leadingTrailing Blanks
                const char * _monAction=monAction;

                cafe.getHandleHelper().removeLeadingAndTrailingSpaces(_monAction,monAction);
                cafe.getHandleHelper().addMonitorAction(handleULong[i-2],monAction);
                if (nrhs > 3)
                {
                    mexPrintf("Handle: %d has monAction: %s\n", handleULong[i-2], monAction);
                }
            }
        }


        int * statusArray = new  int [numberOfHandles];
        unsigned int * monitorID = new unsigned int[numberOfHandles];

        status=cafe.monitorStart(handleULong, (unsigned int) numberOfHandles,   statusArray,  monitorID);

        //monitorID
        plhs[0] = mxCreateNumericMatrix(1,numberOfHandles,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (mwSignedIndex i=0; i<numberOfHandles; ++i)
        {
            ulongArray [i]=monitorID[i];
        }


        //statuses
        plhs[2] = mxCreateNumericMatrix(1,numberOfHandles,mxINT32_CLASS,mxREAL);
        longArray = (int32_T *) mxGetData (plhs[2]);
        for (mwSignedIndex i=0; i<numberOfHandles; ++i)
        {
            longArray [i]=statusArray[i];
        }

        //status
        plhs[1] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[1]);
        longStatus[0]=status;


        delete [] statusArray;
        delete [] monitorID;

        break;

    }


    case MONITOR_STOP:
    {

        if (nrhs==1)
        {
            //mexPrintf("Input:  'monitorStop' ==> stopping all monitors\n" );
            cafe.getHandleHelper().clearMonitorAction();
            status=cafe.monitorStop();

        }

        else
        {

            handleULong = checkHandleArray(prhs[1],true);

            if (handleULong==NULL)
            {
                mexPrintf("MONITOR: Ignoring NULL value in input \n");
                mexPrintf("Input Invalid. Should be: 'monitor', handle/pv array \n");
                return;
            }

            if(handleULong[0]==0)
            {
                mexPrintf("Input Invalid. Should be: 'monitor', handle/pv array \n");
                return;
            }


            size_t numberOfHandles=mxGetNumberOfElements (prhs[1]);


            if (mxIsChar(prhs[1]) )
            {
                numberOfHandles=1;
            }
            else if (mxIsCell(prhs[1]) )
            {
                numberOfHandles=neWithinAllCells;
            }

            //handle=checkForHandle(prhs[1]);
            //if(handle==0) {return;}
            handle=handleULong[0];
            if (numberOfHandles==1 && nrhs>2)
            {

                if (!mxIsNumeric(prhs[2]) )
                {
                    mexPrintf("MonitorID does not have a numeric value \n" );
                    mexPrintf("Input:  'monitorStop', handle/pv, monitorID(optional)\n" );
                    return;
                }

                monitorID = (uint32_T) mxGetScalar(prhs[2]);
                cafe.getHandleHelper().clearMonitorAction(handle);
                status=cafe.monitorStop(handle,monitorID);

                if (status!=ICAFE_NORMAL)
                {
                    cafe.printStatus(handle,status);
                }

            }
            else if (numberOfHandles==1)
            {
                cafe.getHandleHelper().clearMonitorAction(handle);
                status=cafe.monitorStop(handle);
                if (status!=ICAFE_NORMAL)
                {
                    cafe.printStatus(handle,status);
                }

            }
            else
            {

                if (nrhs>2)
                {
                    mexPrintf("Ignoring third input argument; Stopping all monitors for given array of handles/pvs  \n" );
                    mexPrintf("Input:  'monitorStop', [handle/pv] array\n" );
                }

                int * statusArray = new  int [numberOfHandles];

                status=cafe.monitorStop(handleULong, (unsigned int) numberOfHandles,   statusArray);

                if (status!=ICAFE_NORMAL)
                {
                    for (mwSignedIndex i=0; i<numberOfHandles; ++i)
                    {
                        cafe.printStatus(handleULong[i],statusArray[i]);
                    }
                }


                delete [] statusArray;
            }



        }// else at top

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }

    case MONITOR_FLUSH_EVENT:
    {
        vector<string>al;
        al=cafe.getHandleHelper().getMonitorAction(true);

        for (int i=0; i<al.size(); ++i)
        {
            ////mexPrintf("%s \n",al.begin()[i].c_str() );
            const char * monString=al.begin()[i].c_str();
            if(monString!=NULL)
            {
                mexEvalString(monString);
            }
        }
        break;
    }


    case GET_MONITOR_HANDLES_AND_ACTIONS:
    {

        vector<unsigned int>hwm;
        vector<string>act;

        cafe.getHandleHelper().getMonitorHandlesAndActions(hwm,act);

        //return handle
        plhs[0] = mxCreateNumericMatrix(1,hwm.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData(plhs[0]);

        //return action
        plhs[1] = mxCreateCellMatrix(1,act.size());

        for (size_t i=0; i<min(hwm.size(),act.size()); ++i)
        {
            ulongArray[i]=hwm[i];
            mxSetCell (plhs[1], i, mxCreateString( act[i].c_str()));

        }
        break;
    }

    case GET_MONITOR_HANDLES:
    {
        vector<unsigned int>hwm;
        hwm=cafe.getHandleHelper().getHandlesWithMonitors();

        //return handle
        plhs[0] = mxCreateNumericMatrix(1,hwm.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData(plhs[0]);

        for (int i=0; i<hwm.size(); ++i)
        {
            ulongArray[i]=hwm[i];
        }
        break;
    }

     case GET_NO_MONITORS:
    {
		
		   
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'getNoMonitors', handle/pv \n", nrhs);
            return;
        }
        else if (nrhs>2)
        {
            mexPrintf("Ignoring arguments after the 2nd from the %d entered. Input: 'getNoMonitors', handle/pv \n", nrhs);
        }

        handle=checkForHandle(prhs[1]);
        if(handle==0)
        {
            return;
        }


        unsigned int nm;
        nm=cafe.getHandleHelper().getNmonitor(handle);

        //number of monitors for this handle      
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData(plhs[0]);

        ulongArray[0]=nm;
        
        break;
    }


    case PRINT_MONITORS:
    {
        cafe.getHandleHelper().printMonitors();
        break;

    }


    case PRINT_HANDLE:
    {
        if (nrhs<2)
        {
            cafe.printHandles();
        }
        else
        {
            handle=checkForHandleNoOpen(prhs[1]);
            if(handle==0)
            {
                return;
            }
            cafe.printHandle(handle);
        }
        break;
    }


    case PRINT_HANDLES:
    {
        if (nrhs<2)
        {
            cafe.printHandles();
        }
        else
        {


            for (size_t i=1; i<nrhs; ++i)
            {

                neWithinAllCells=0;


                //What is dataType?
                handleULong = checkHandleArray(prhs[i],false);

                if (handleULong==NULL)
                {
                    mexPrintf("PRINT_HANDLES: Ignoring NULL value in input %d of %d\n", (i+1), nrhs);
                    continue;

                }


                if(handleULong[0]==0)
                {
                    mexPrintf("Invalid value in input %d of %d\n", (i+1), nrhs);
                    continue;
                }



                size_t numberOfHandles=mxGetNumberOfElements (prhs[i]);



                if (mxIsChar(prhs[i]) )
                {
                    numberOfHandles=1;
                }
                else if (mxIsCell(prhs[i]) )
                {
                    numberOfHandles=neWithinAllCells;
                    //mexPrintf(" cell numberOfHandles %d",numberOfHandles);
                }


                cafe.getHandleHelper().printHandles(handleULong, numberOfHandles );

            }

        }




        break;
    }


    case PRINT_DISCONNECTED_HANDLES:
    {
        cafe.printDisconnectedHandles();
        break;
    }


    case GET_HANDLES:
    {


        if (nrhs>2)
        {
            mexPrintf("Ignoring all input arguments to mocha after the first. \n");
            mexPrintf("Method returns all handles and their PV names \n");
        }


        vector<unsigned int> dhV;
        vector<string> pvV;
        cafe.getHandleHelper().getHandles(dhV,pvV);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,dhV.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (size_t i=0; i<dhV.size(); ++i)
        {
            ulongArray[i]=dhV[i];
        }

        plhs[1] = mxCreateCellMatrix(1,pvV.size());

        for (size_t i=0; i<pvV.size(); ++i)
        {
            mxSetCell (plhs[1], i, mxCreateString( pvV[i].c_str()));
        }

        break;
    }


    case GET_HANDLE_STATES:
    {


        if (nrhs>1)
        {
            mexPrintf("Ignoring all input arguments to mocha after the first. \n");
            mexPrintf("Method returns all handles, their PV names and connection states \n");
        }


        vector<unsigned int> dhV;
        vector<string> pvV;
        vector<unsigned short> connV;
        cafe.getHandleHelper().getHandleStates(dhV,pvV,connV);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,dhV.size(),mxUINT32_CLASS,mxREAL);
        plhs[2] = mxCreateNumericMatrix(1,dhV.size(),mxUINT16_CLASS,mxREAL);

        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (size_t i=0; i<dhV.size(); ++i)
        {
            ulongArray[i]=dhV[i];
        }

        plhs[1] = mxCreateCellMatrix(1,pvV.size());

        for (size_t i=0; i<pvV.size(); ++i)
        {
            mxSetCell (plhs[1], i, mxCreateString( pvV[i].c_str()));
        }

        ushortArray = (uint16_T *) mxGetData ( plhs[2]);
        for (size_t i=0; i<connV.size(); ++i)
        {
            ushortArray[i]=connV[i];
        }


        break;
    }



    case GET_HANDLES_WITHIN_GROUP:
    {


        if (nrhs>2)
        {
            mexPrintf("Ignoring all input arguments to mocha after the first. \n");
            mexPrintf("Method returns all handles within a group \n");
        }


        ghandle=checkForGroupHandle(prhs[1]);
        if(ghandle==0)
        {

            //fill with data
            mexPrintf("Group entered as input argument has not been defined!\n");
            plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
            ulongArray = (uint32_T *) mxGetData ( plhs[0]);

            return;
        }

        vector<unsigned int> dhV = cafe.getHandlesFromWithinGroupV(ghandle);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,dhV.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (size_t i=0; i<dhV.size(); ++i)
        {
            ulongArray[i]=dhV[i];
        }


        break;
    }



    case GET_DISCONNECTED_HANDLES:
    {
        vector<unsigned int> dhV;
        vector<string> pvV;
        cafe.getHandleHelper().getDisconnectedHandles(dhV,pvV);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,dhV.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (size_t i=0; i<dhV.size(); ++i)
        {
            ulongArray[i]=dhV[i];
        }

        plhs[1] = mxCreateCellMatrix(1,pvV.size());

        for (size_t i=0; i<pvV.size(); ++i)
        {
            mxSetCell (plhs[1], i, mxCreateString( pvV[i].c_str()));
        }

        break;
    }

    case GET_CONNECTED_HANDLES:
    {
        vector<unsigned int> dhV;
        vector<string> pvV;

        cafe.getHandleHelper().getConnectedHandles(dhV,pvV);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,dhV.size(),mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        for (size_t i=0; i<dhV.size(); ++i)
        {
            ulongArray[i]=dhV[i];
        }

        plhs[1] = mxCreateCellMatrix(1,pvV.size());

        for (size_t i=0; i<pvV.size(); ++i)
        {
            mxSetCell (plhs[1], i, mxCreateString( pvV[i].c_str()));
        }

        break;
    }




    case HANDLE_FROM_PV:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'getHandleFromPV', pv \n", nrhs);
            return;
        }
        handle=checkForHandleNoOpen(prhs[1]);
        //return handle
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]=handle;
        break;
    }

    case PV_FROM_HANDLE:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'getPVFromHandle', handle \n", nrhs);
            return;
        }
        handle=checkForHandleNoOpen(prhs[1]);



        // plhs[0] = mxCreateCellMatrix(1,1);
        if (handle != 0)
        {
            const char * _pv=cafe.getHandleHelper().getPVFromHandle(handle);
            //mxSetCell (plhs[0], 0, mxCreateString(_pv));
            plhs[0]=mxCreateString(_pv);
        }
        else
        {
            //mxSetCell (plhs[0], 0, mxCreateString(""));
            plhs[0]=mxCreateString("");
        }
        break;
    }


    case IS_CONNECTED:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'isConnected', handle \n", nrhs);
            return;
        }
        handle=checkForHandleNoOpen(prhs[1]);
        //handle=checkForHandle(prhs[1]);
        if (handle==0)
        {
            plhs[0] = mxCreateLogicalScalar(0);
        }
        plhs[0] = mxCreateLogicalScalar(cafe.isChannelConnected(handle));

        break;
    }


    case ALL_CONNECTED:
    {
        plhs[0] = mxCreateLogicalScalar(cafe.allChannelsConnected());
        break;
    }


    case GET_CHANNEL_INFO:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'getChannelInfo', handle \n", nrhs);
            return;
        }

        if (mxIsChar(prhs[1]))
        {
            nh=1;
        }
        else
        {
            nh=mxGetNumberOfElements (prhs[1]);
        }
        if (nh >1)
        {
            mexPrintf("Method limited to one handle/pv only. Reporting on first given");
        }

        handle=checkForHandle(prhs[1]);   //NoOpen(prhs[1]);

        if (handle==0)
        {
            mexPrintf("Handle does not exist!\n");
            return;
        }

        ChannelRegalia channelInfo;

        status=cafe.getChannelInfo(handle, channelInfo);

        /*
        cout << channelInfo.getConnectFlag() << endl;
        cout << channelInfo.getHostName() << endl;
        cout << channelInfo.getChannelID() << endl;
        cout << channelInfo.getClassNameAsString() << endl;
        cout << channelInfo.getDataType() << endl;
        cout << channelInfo.getNelem() << endl;
        cout << channelInfo.getConnectionStateAsString() << endl;
        cout << channelInfo.getAccessRead() << endl;
        cout << channelInfo.getAccessWrite() << endl;
        */

        if (status != ICAFE_SUCCESS)
        {
            NS_MOCHA::printStatus(status);
            return;
        }


        plhs[0]=mxCreateStructMatrix(1, 1, nfieldsInfo, fnamesInfo);

//const char * fnamesInfo[9] ={"channelID","connectFlag","hostName","dataType",
//"className","accessRead", "accessWrite","nelem","connectionState"};


        //fout0 = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        //ulongArray = (uint32_T *) mxGetData (fout1);
        //ulongArray[0]=(chid) channelInfo.getChannelID();
        fout0 = mxCreateCellMatrix(1,1);
        stringstream ss;
        ss << channelInfo.getChannelID();
        string strH = ss.str();
        //mxSetCell (fout0, 0, mxCreateString( strH.c_str()));

        fout0 = mxCreateString( strH.c_str());
        fout1 = mxCreateLogicalScalar(channelInfo.getConnectFlag());

        fout2 = mxCreateString( channelInfo.getHostName());

        //fout2 = mxCreateCellMatrix(1,1);
        //fout3 = mxCreateCellMatrix(1,1);
        //fout4 = mxCreateCellMatrix(1,1);
        //fout8 = mxCreateCellMatrix(1,1);

        switch (channelInfo.getDataType())
        {
        case CAFE_DOUBLE:
            fout3 = mxCreateString("DBR_DOUBLE");
            break;
        case CAFE_FLOAT:
            fout3 = mxCreateString("DBR_FLOAT");
            break;
        case CAFE_SHORT:
            fout3 = mxCreateString("DBR_SHORT");
            break;
        case CAFE_ENUM:
            fout3 = mxCreateString("DBR_ENUM");
            break;
        case CAFE_CHAR:
            fout3 = mxCreateString("DBR_CHAR");
            break;
        case CAFE_LONG:
            fout3 = mxCreateString("DBR_LONG");
            break;
        case CAFE_STRING:
            fout3 = mxCreateString("DBR_STRING");
            break;
        default: //If not connected for instance
            fout3 = mxCreateString("DBR_UNKNOWN");
            break;
        }


        //mxSetCell (fout2, 0, mxCreateString( channelInfo.getHostName()));
        //mxSetCell (fout4, 0, mxCreateString( channelInfo.getClassName()));
        //mxSetCell (fout8, 0, mxCreateString( channelInfo.getConnectionStateAsString().c_str()));

        fout4 = mxCreateString( channelInfo.getClassNameAsString().c_str());
        fout8 = mxCreateString( channelInfo.getConnectionStateAsString().c_str());

        fout5 = mxCreateLogicalScalar(channelInfo.getAccessRead());
        fout6 = mxCreateLogicalScalar(channelInfo.getAccessWrite());

        fout7 = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);

        inputULong = (uint32_T *) mxGetData (fout7);
        inputULong[0]=channelInfo.getNelem();



        fout16=mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        fHandle    = (uint32_T *) mxGetData(fout16);
        fHandle[0]=handle;
        string s = cafe.getPVFromHandle(handle);
        fout17 =mxCreateString( s.c_str() );



        //Prepare struct



        mxSetFieldByNumber(plhs[0], 0, 0, fout16); //handle
        mxSetFieldByNumber(plhs[0], 0, 1, fout17); //pv
        mxSetFieldByNumber(plhs[0], 0, 2, fout0);
        mxSetFieldByNumber(plhs[0], 0, 3, fout1);
        mxSetFieldByNumber(plhs[0], 0, 4, fout2);
        mxSetFieldByNumber(plhs[0], 0, 5, fout3);
        mxSetFieldByNumber(plhs[0], 0, 6, fout4);
        mxSetFieldByNumber(plhs[0], 0, 7, fout5);
        mxSetFieldByNumber(plhs[0], 0, 8, fout6);
        mxSetFieldByNumber(plhs[0], 0, 9, fout7);
        mxSetFieldByNumber(plhs[0], 0,10, fout8);

        break;
    }




    case GET_READ_ACCESS:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'getReadAccess', handle \n", nrhs);
            return;
        }
        handle=checkForHandleNoOpen(prhs[1]);
        plhs[0] = mxCreateLogicalScalar(cafe.getReadAccess(handle));

        break;
    }

    case GET_WRITE_ACCESS:
    {
        if (nrhs<2)
        {
            mexPrintf(
                "Invalid Number of arguments %d. Input: 'getWriteAccess', handle \n", nrhs);
            return;
        }
        handle=checkForHandleNoOpen(prhs[1]);
        plhs[0] = mxCreateLogicalScalar(cafe.getWriteAccess(handle));

        break;
    }



    case FETCH_INDEX_OF_COLLECTION_MEMBER:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, collectionName, deviceName \n", nrhs);
            return;
        }

        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("collectionName does not have a string value \n" );
            return;
        }
        if (!mxIsChar(prhs[2]) )
        {
            mexPrintf("deviceName does not have a string value \n" );
            return;
        }

        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);

        //message pvname
        xData = (mxArray *)  prhs[2];
        mxGetString(xData, pv, pvLength);

        long idx=cafe.fetchIndexOfCollectionMember(groupName, pv);
        idx=idx+1;

        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        uint32_T * idxArray  = (uint32_T *) mxGetData ( plhs[0]);

        idxArray[0]=(uint32_T) idx; //+1 as first element is matlab array indexed by 1 not 0!!

        break;
    }

    case FETCH_INDEX_OF_GROUP_MEMBER:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, groupName, pv \n", nrhs);
            return;
        }

        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("groupName does not have a string value \n" );
            return;
        }
        if (!mxIsChar(prhs[2]) )
        {
            mexPrintf("pv does not have a string value \n");
            return;
        }

        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);

        //message pvname
        xData = (mxArray *)  prhs[2];
        mxGetString(xData, pv, pvLength);

        long idx=cafe.fetchIndexOfGroupMember(groupName, pv);
        idx=idx+1;

        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        uint32_T * idxArray  = (uint32_T *) mxGetData ( plhs[0]);

        idxArray[0]=(uint32_T) idx; //+1 as first element is matlab array indexed by 1 not 0!!

        break;
    }
    case LIST_GROUPS:
    {
        vector<string> glist;
        glist.clear();
        cafe.groupList(glist);
        plhs[0] = mxCreateCellMatrix(1,glist.size());
        for (size_t i=0; i<glist.size(); ++i)
        {
            mxSetCell (plhs[0], i, mxCreateString( glist[i].c_str()));
        }
        break;
    }

    case LIST_GROUP_MEMBERS:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'listGroupMembers', groupName \n", nrhs);
            return;
        }

        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("groupName does not have a string value \n" );
            return;
        }
        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);


        vector<string> glist;
        glist.clear();
        cafe.groupMemberList(groupName,glist);
        plhs[0] = mxCreateCellMatrix(glist.size(),1);
        for (size_t i=0; i<glist.size(); ++i)
        {
            mxSetCell (plhs[0], i, mxCreateString( glist[i].c_str()));
        }
        break;
    }

    case DEFINE_GROUP:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid number of arguments %d. Input: 'defineGroup', groupName, cell \n", nrhs);
            return;
        }
        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("groupName does not have a string value \n" );
            return;
        }
        if (!mxIsCell(prhs[2]) )
        {
            mexPrintf("Expecting a cell containing pv names that are to constitute the group \n" );
            return;
        }
        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);

        neWithinAllCells=0;

        scanCell(prhs[2]);

        inputPVStringCell = new pv_string_t[neWithinAllCells];
        iWithinAllCells=0; //Required before call to cellToPVString
        cellToPVString(prhs[2]);

        status=cafe.groupDefine (groupName, inputPVStringCell, neWithinAllCells);
        delete [] inputPVStringCell;

        if (status!=ICAFE_NORMAL)
        {
            NS_MOCHA::printStatus(status);
        }

        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;

    }

    case LOAD_SF_GROUPS:
    {


#ifdef HAVE_PYTHON_H
        if (nrhs<2)
        {
            mexPrintf("Loading Virtual Accelertaor groups with loadSFGroups('VA') \n");
            cafe.loadSFGroups("VA");
        }
        else
        {
            if (!mxIsChar(prhs[1]) )
            {
                mexPrintf("Accelerator base name does not have a string value \n" );
                return;
            }
            yData = (mxArray *)  prhs[1];
            pvLength = PVNAME_MAX_SIZE+1;
            mxGetString(yData, acceleratorBase, pvLength);
            mexPrintf("Loading groups with loadSFGroups: \n");
            cafe.loadSFGroups(acceleratorBase);
        }

#else
        mexPrintf("SwissFEL specific method.\n");
        mexPrintf("Requires pythonic version of cafe library.\n");
        mexPrintf("Not activated in this release\n");

#endif


        break;
    }


    case GET_CHANNEL_LIST:
    {

#if HAVE_PYTHON_H

        vector<string> inList;
        inList.clear();

        for (mwSignedIndex k=1; k<nrhs; ++k)
        {
            mxClassID mxid =mxGetClassID( prhs[k]);

            switch (mxid)
            {
            case mxCHAR_CLASS:
            {
                pv_string_t  pvString;;
                mxGetString(prhs[k], pvString, sizeof(pv_string_t));
                inList.push_back(pvString);
                break;
            }
            case mxCELL_CLASS:
            {

                neWithinAllCells=0;
                scanCell(prhs[1]);
                inputPVStringCell = new pv_string_t[neWithinAllCells];

                iWithinAllCells=0; //Required before call to cellToPVString
                cellToPVString(prhs[1]);

                for (size_t i=0; i<neWithinAllCells; ++i)
                {
                    inList.push_back(inputPVStringCell[i]);
                }
                delete [] inputPVStringCell;
                break;
            }
            default:
                mexPrintf ("ignoring non-char/non-cell input");
            }
        }


        vector<string> outList= cafe.generateChannelList(inList);


        plhs[0] = mxCreateCellMatrix(1,outList.size());
        for (size_t i=0; i<outList.size(); ++i)
        {
            mxSetCell (plhs[0], i, mxCreateString( outList[i].c_str()));
        }
#else

        mexPrintf("Specialized method for SwissFEL. Not activated in this release.\n");

#endif

        break;
    }


    case LIST_COLLECTIONS:
    {


        vector<string> clist;
        clist.clear();
        cafe.collectionList(clist);
        plhs[0] = mxCreateCellMatrix(1,clist.size());
        for (size_t i=0; i<clist.size(); ++i)
        {
            mxSetCell (plhs[0], i, mxCreateString( clist[i].c_str()));

        }


        break;
    }

    case LIST_COLLECTION_MEMBERS:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input: 'listCollectionMembers', collectionName \n", nrhs);
            return;
        }

        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("collectionName does not have a string value \n");
            return;
        }
        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);

        vector<string> clist;
        clist.clear();
        cafe.collectionMemberList(groupName,clist);
        plhs[0] = mxCreateCellMatrix(clist.size(),1);
        for (size_t i=0; i<clist.size(); ++i)
        {
            mxSetCell (plhs[0], i, mxCreateString( clist[i].c_str()));
        }
        break;
    }




    case LOAD_XML_COLLECTIONS:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid number of arguments %d. Input: 'loadXMLCollections', fileName \n", nrhs);
            return;
        }
        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("filename does not have a string value \n");
            return;
        }

        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);


        if(!collectionsLoaded)
        {

#ifdef HAVE_LIBQTXML
            {
                status=cafe.loadCollectionsFromXML((const char *) groupName);

                if (status == ECAFE_LOAD_COLLECTION )
                {
                    NS_MOCHA::printStatus(status);
                }
                else
                {
                    collectionsLoaded=true;
                }
            }
#else
            {
                mexPrintf("HAVE_LIBQTXML in <CAFE>/include/config.h is set to false.\n");
                mexPrintf("Please recompile the cafe library with qtxml to use this feature. \n");
            }
#endif

        }
        else
        {
            mexPrintf("Collections already loaded from XML. Collection names are:\n");

            vector<string> clist;
            clist.clear();
            cafe.collectionList(clist);

            for (size_t i=0; i<clist.size(); ++i)
            {
                mexPrintf("%s \n", clist[i].c_str());
            }
        }

        break;
    }


    case LOAD_XML_GROUPS:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid number of arguments %d. Input: 'loadXMLGroups', fileName \n", nrhs);
            return;
        }
        if (!mxIsChar(prhs[1]) )
        {
            mexPrintf("filename does not have a string value \n" );
            return;
        }

        //message groupName
        yData = (mxArray *)  prhs[1];
        pvLength = PVNAME_MAX_SIZE+1;
        mxGetString(yData, groupName, pvLength);


        if(!groupsLoaded)
        {

#ifdef HAVE_LIBQTXML
            {
                status=cafe.loadGroupsFromXML((const char *) groupName);

                if (status == ECAFE_LOAD_GROUP)
                {
                    NS_MOCHA::printStatus(status);
                }
                else
                {
                    groupsLoaded=true;
                }
            }
#else
            {
                mexPrintf("HAVE_LIBQTXML in <CAFE>/include/config.h is false.\n");
                mexPrintf("Please recompile the cafe library with qtxml to use this feature. \n");

            }
#endif

        }
        else
        {
            mexPrintf("Groups already loaded from XML. Group names are listed.\n");
            mexPrintf("mocha ('listGroupMembers', <groupName>) will list the members of the given group. \n");
            vector<string> glist;
            glist.clear();
            cafe.groupList(glist);

            for (size_t i=0; i<glist.size(); ++i)
            {
                mexPrintf("%s \n", glist[i].c_str());
            }
        }

        break;
    }



// This is SwissFEL specific
    /*
            if(!collectionsLoaded) {

                #ifdef HAVE_LIBQTXML
    	    {

                	status=cafe.loadCollectionsFromXML((const char *) "collection_swissfel.xml");

                	if (status != ECA_NORMAL) {
                    	NS_MOCHA::printStatus(status); }
                	else {collectionsLoaded=true;}
    	    }
    	    #endif
            }

            if(!groupsLoaded) {

    	    #ifdef HAVE_LIBQTXML
    	    {
                status=cafe.loadGroupsFromXML( (const char *) "group_swissfel.xml");

                if (status != ECA_NORMAL) {
                    NS_MOCHA::printStatus(status); }
                else {groupsLoaded=true; }
    	    }
    	    #endif
            }
    */


    case SET_SELF_GOVERNING_TIMEOUT:
    {


        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, true/false \n", nrhs);
            mexPrintf("or message, true/false (applied to all handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if( !mxIsLogical(prhs[2]) )
            {
                mexPrintf("3rd input argument must be of Matlab type Logical, i.e.true/false \n" );
                return;
            }

            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                status=cafe.getPolicy().setSelfGoverningTimeout(handle,mxIsLogicalScalarTrue(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_HANDLE;
                mexPrintf("handle=%d does not exist\n", handle);
            }
        }
        else
        {
            if( !mxIsLogical(prhs[1]) )
            {
                mexPrintf("If method is meant for all handles, then: \n" );
                mexPrintf("1st input argument must be of Matlab type Logical, i.e.true/false \n" );
                return;
            }
            status=cafe.getPolicy().setSelfGoverningTimeout(mxIsLogicalScalarTrue(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;

    }





    case SET_TIMEOUT_DEFAULT:
    {
        if (nrhs>2)
        {
            mexPrintf("Ignoring all input arguments after the 2nd\n");
        }

        if (nrhs>1)
        {


            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                status=cafe.getPolicy().setTimeoutToDefault(handle);
            }
            else
            {
                status=ECAFE_INVALID_HANDLE;
            }
        }
        else
        {
            status=cafe.getPolicy().setTimeoutToDefault();

        }

        plhs[0] = mxCreateDoubleMatrix(1,2,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);

        //Return default timeout
        if (status==ICAFE_NORMAL)
        {


            double ax=0,bx=0;
            if (nrhs>1 && handle!=0)
            {

                cafe.getPolicy().getTimeout(handle,ax,bx);
                doubleArray[0]=ax;
                doubleArray[1]=bx;
                break;
            }
            else
            {
                double cx=0,dx=0;
                cafe.getPolicy().getTimeoutRange(ax,bx,cx,dx);

                doubleArray[0]=min(ax,bx); //min put (min/max should be same)
                doubleArray[1]=min(cx,dx); //min get (min/max should be same)
                break;
            }
        }
        else
        {
            mexPrintf("Error in setting timeouts to default value!\n");
            doubleArray[0]=-1;
            doubleArray[1]=-1;
        }
        break;

    }

    case SET_TIMEOUT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, timeout \n", nrhs);
            mexPrintf("or message, timeout (applied to all handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n" );
                return;
            }

            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                status=cafe.getPolicy().setTimeout(handle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_HANDLE;
            }
        }
        else
        {
            status=cafe.getPolicy().setTimeout(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }

    case SET_TIMEOUT_PUT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, timeout (for put) \n", nrhs);
            mexPrintf("or message, timeout (for put, applied to all handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n" );
                return;
            }

            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                status=cafe.getPolicy().setTimeoutPut(handle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_HANDLE;
            }
        }
        else
        {
            status=cafe.getPolicy().setTimeoutPut(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }


    case SET_TIMEOUT_GET:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, timeout (for get) \n", nrhs);
            mexPrintf("or message, timeout (for get, applied to all handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n");
                return;
            }

            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                status=cafe.getPolicy().setTimeoutGet(handle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_HANDLE;
            }
        }
        else
        {
            status=cafe.getPolicy().setTimeoutGet(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }


    case GET_TIMEOUT:
    {

        plhs[0] = mxCreateDoubleMatrix(1,2,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);

        double ax=0,bx=0;
        if (nrhs>1)
        {
            handle=checkForHandleNoOpen(prhs[1]);
            if (handle != 0)
            {
                cafe.getPolicy().getTimeout(handle,ax,bx);
                doubleArray[0]=ax;
                doubleArray[1]=bx;
                break;
            }
            else
            {
                mexPrintf("handle=%d does not exist", handle);
            }
        }
        else
        {
            double cx=0,dx=0;
            cafe.getPolicy().getTimeoutRange(ax,bx,cx,dx);

            doubleArray[0]=min(ax,bx); //min put
            doubleArray[1]=min(cx,dx); //min,get
            break;
        }
        doubleArray[0]=0;
        doubleArray[1]=0;
        break;
    }


    case SET_SG_SELF_GOVERNING_TIMEOUT:
    {


        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, gHandle, true/false \n", nrhs);
            mexPrintf("or message, true/false (applied to all group handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if( !mxIsLogical(prhs[2]) )
            {
                mexPrintf("3rd input argument must be of Matlab type Logical, i.e.true/false \n" );
                return;
            }

            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                status=cafe.getPolicy().setSGSelfGoverningTimeout(ghandle,mxIsLogicalScalarTrue(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_GROUP_HANDLE;
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            if( !mxIsLogical(prhs[1]) )
            {
                mexPrintf("If method is meant for all handles, then: \n" );
                mexPrintf("1st input argument must be of Matlab type Logical, i.e.true/false \n" );
                return;
            }
            status=cafe.getPolicy().setSGSelfGoverningTimeout(mxIsLogicalScalarTrue(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;

    }




    case SET_SG_TIMEOUT_DEFAULT:
    {

        if (nrhs>2)
        {
            mexPrintf("Ignoring all input arguments after the 2nd\n");
        }

        if (nrhs>1)
        {


            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                status=cafe.getPolicy().setSGTimeoutToDefault(ghandle);
            }
            else
            {
                status=ECAFE_INVALID_GROUP_HANDLE;
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            status=cafe.getPolicy().setSGTimeoutToDefault();
        }

        plhs[0] = mxCreateDoubleMatrix(1,2,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);

        //Return default timeout
        if (status==ICAFE_NORMAL)
        {


            double ax=0,bx=0;
            if (nrhs>1 && ghandle!=0)
            {

                cafe.getPolicy().getSGTimeout(ghandle,ax,bx);
                doubleArray[0]=ax;
                doubleArray[1]=bx;
                break;
            }
            else
            {
                double cx=0,dx=0;
                cafe.getPolicy().getSGTimeoutRange(ax,bx,cx,dx);

                doubleArray[0]=min(ax,bx); //min put (min/max should be same)
                doubleArray[1]=min(cx,dx); //min get (min/max should be same)
                break;
            }
        }
        else
        {
            mexPrintf("Error in setting timeouts to default value!\n");
            doubleArray[0]=-1;
            doubleArray[1]=-1;
        }
        break;

    }

    case SET_SG_TIMEOUT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, gHandle, timeout \n", nrhs);
            mexPrintf("or message, timeout (applied to all group handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n" );
                return;
            }

            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                status=cafe.getPolicy().setSGTimeout(ghandle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_GROUP_HANDLE;
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            status=cafe.getPolicy().setSGTimeout(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }

    case SET_SG_TIMEOUT_PUT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, gHandle, timeout (for put) \n", nrhs);
            mexPrintf("or message, timeout (for put, applied to all group handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n" );
                return;
            }

            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                status=cafe.getPolicy().setSGTimeoutPut(ghandle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_GROUP_HANDLE;
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            status=cafe.getPolicy().setSGTimeoutPut(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }


    case SET_SG_TIMEOUT_GET:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, gHandle, timeout (for get) \n", nrhs);
            mexPrintf("or message, timeout (for get, applied to all group handles) \n");

            return;
        }

        if (nrhs>2)
        {
            if (!mxIsNumeric(prhs[2]) )
            {
                mexPrintf("Timeout does not have a numeric value \n");
                return;
            }

            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                status=cafe.getPolicy().setSGTimeoutGet(ghandle,mxGetScalar(prhs[2]));
            }
            else
            {
                status=ECAFE_INVALID_GROUP_HANDLE;
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            status=cafe.getPolicy().setSGTimeoutGet(mxGetScalar(prhs[1]));
        }
        //status
        plhs[0] = mxCreateNumericMatrix(1,1,mxINT32_CLASS,mxREAL);
        longStatus = (int32_T *) mxGetData ( plhs[0]);
        longStatus[0]=status;
        break;
    }


    case GET_SG_TIMEOUT:
    {

        plhs[0] = mxCreateDoubleMatrix(1,2,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);

        double ax=0,bx=0;
        if (nrhs>1)
        {
            ghandle=checkForGroupHandleNoOpen(prhs[1]);
            if (ghandle != 0)
            {
                cafe.getPolicy().getSGTimeout(ghandle,ax,bx);
                doubleArray[0]=ax;
                doubleArray[1]=bx;
                break;
            }
            else
            {
                mexPrintf("ghandle=%d does not exist\n", ghandle);
            }
        }
        else
        {
            double cx=0,dx=0;
            cafe.getPolicy().getSGTimeoutRange(ax,bx,cx,dx);

            doubleArray[0]=min(ax,bx); //min put
            doubleArray[1]=min(cx,dx); //min,get
            break;
        }
        doubleArray[0]=0;
        doubleArray[1]=0;
        break;
    }

    case SET_NELEM:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, nelem (else to native if not given) \n", nrhs);
            return;
        }

        uint32_T nDataTransfer= 0;

        if (!mxIsNumeric(prhs[2]) )
        {
            mexPrintf("nelem does not have a numeric value \n");
            return;
        }

        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            if (cacheFlag)
            {
                if (nrhs >2)
                {
                    nDataTransfer=cafe.getHandleHelper().setNelemToRetrieveFromCache(handle,mxGetScalar(prhs[2]));
                }
                else if (nrhs==2)
                {
                    nDataTransfer=cafe.getHandleHelper().setNelemToRetrieveFromCache(handle);
                }
                //mexPrintf("nDataTransfer for Cache = %d", nDataTransfer);
            }
            else
            {
                if (nrhs >2)
                {
                    nDataTransfer=cafe.getHandleHelper().setNelem(handle,mxGetScalar(prhs[2]));
                }
                else if (nrhs==2)
                {
                    nDataTransfer=cafe.getHandleHelper().setNelem(handle);
                }

            }
        }

        //nDataTransfer
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nDataTransfer;
        break;
    }

    case SET_OFFSET:
    {
        if (nrhs<3)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle, offset \n", nrhs);
            return;
        }

        uint32_T nOffset= 0;

        if (!mxIsNumeric(prhs[2]) )
        {
            mexPrintf("offset does not have a numeric value \n" );
            return;
        }

        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            nOffset=cafe.getHandleHelper().setOffset(handle,mxGetScalar(prhs[2]));
        }


        //nOffset
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nOffset;
        break;
    }

    case GET_NELEM_NATIVE:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle\n", nrhs);
            return;
        }

        uint32_T nElements= 0;


        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            nElements=cafe.getHandleHelper().getNelemNative(handle);
        }


        //nElements
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nElements;
        break;
    }

    case GET_NELEM_CLIENT:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle\n", nrhs);
            return;
        }

        uint32_T nElements= 0;


        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            nElements=cafe.getHandleHelper().getNelemClient(handle);
        }


        //nElements
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nElements;
        break;
    }


    case GET_NELEM_REQUEST:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle\n", nrhs);
            return;
        }

        uint32_T nElements= 0;

        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            if(cacheFlag)
            {
                nElements=cafe.getHandleHelper().getNelemToRetrieveFromCache(handle);
            }
            else
            {
                nElements=cafe.getHandleHelper().getNelemRequest(handle);
            }
        }


        //nElements
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nElements;
        break;
    }


    case GET_NELEM:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle\n", nrhs);
            return;
        }

        uint32_T nElements[3]; // client, native, requested
        nElements[0]=0;
        nElements[1]=0;
        nElements[2]=0;

        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            cafe.getHandleHelper().getNelem(handle, nElements[0], nElements[1], nElements[2]);
        }


        //nElements
        plhs[0] = mxCreateNumericMatrix(1,3,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData ( plhs[0]);
        ulongArray[0]= nElements[0];
        ulongArray[1]= nElements[1];
        ulongArray[2]= nElements[2];
        break;
    }


    case GET_OFFSET:
    {
        if (nrhs<2)
        {
            mexPrintf("Invalid Number of argument %d. Input:  message, handle\n", nrhs);
            return;
        }

        uint32_T nOffset= 0;

        handle=checkForHandleNoOpen(prhs[1]);
        if (handle != 0)
        {
            nOffset=cafe.getHandleHelper().getOffset(handle);
        }

        //nOffset
        plhs[0] = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
        ulongArray = (uint32_T *) mxGetData (plhs[0]);
        ulongArray[0]= nOffset;
        break;
    }




    default:
        char * _messOriginal=messOriginal;
        cafe.getHandleHelper().removeLeadingAndTrailingSpaces(_messOriginal,messOriginal);
        mexPrintf("%s %s\n","Unknown message--->", messOriginal);
        plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
        doubleArray = mxGetPr ( plhs[0]);
        doubleArray[0]=numeric_limits<double>::signaling_NaN();
        break;

    }//End Big Switch

    return;
}



