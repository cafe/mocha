/* Source of MEX-file which will call the wrapper shared library.
        We moved the previous content of the mexFunction to the wrapper library and now
        call the function in the wrapper library instead.
*/
#include "macchinetta.h"

extern void _main();

void mexFunction(int nlhs,mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    macchinettaFunction(nlhs,plhs,nrhs,prhs);
}
