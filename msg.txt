
Re: cafe-matlab version 2020b and onwards.

Important information to cafe-matlab users.

Dear Colleagues,
Starting with MATLAB 2020b, the mocha mex-file
will only work if matlab is executed from the
following bash script:

source /opt/psi/Cafe/cafe-matlab.sh -v 2020b

This script by necessity momentarily adds the 
EPICS libCom.so library to the LD_PRELOAD environment
variable before starting matlab. The library is
immediately removed from LD_PRELOAD once matlab is
executed. The setting of LD_PRELOAD was previously
handled from within the IT load module step but has
now been withdraw from this procedure and delegated
to the above script to prevent conflict with other
epics client applications running on the user's console
that would unwittingly pick up this lingering environment
variable.

An explanation as to why this is required is given below.

For those invoking the above script in their .bashrc
in order to load the cafe-matlab module for later use; 
the script can continue to be used for this purpose for
matlab versions 2020a and earlier only:

source /opt/psi/Cafe/cafe-matlab.sh -v 2020a

The above script can also be used to both load the 
cafe-matlab module and start matlab, e.g, on the
command line, for all matlab versions (old and new) 
as follows:

source /opt/psi/Cafe/cafe-matlab.sh -v 2020a -s True

The -s flag controls whether or not to start matlab
and only plays a role only for matlab 2020a or earlier.
The default value is False (and thus matlab is started
explicitly by the user). For matlab 2020b onwards
the flag is always set to True. To learn of all script
options, see:
/opt/psi/Cafe/cafe-matlab.sh -h

FYI, the mocha mex-file for MATLAB version 2020a and 
onwards uses the latest epics 7 client release:

mocha('epicsversion')

Please let me know if I can assist with anything.

Yours,
Jan

http://cafe.psi.ch/mocha.html

What's going on?
================
With input from a reply from Mathworks Technical Support.
MATLAB 2020b and subsequent versions require glibc 
version 2.18 or greater. Red Hat 7 (RHEL7), however,
is based on the glib 2.17 release.
To allow MATLAB 2020b to run on RHEL7, MATLAB includes
a partial glib 2.18 implementation that is pre-loaded
atop of RHEL7's glibc 2.17. Unfortunately, there is
a documented bug in glib versions 2.18 - 2.22 that
results in a deadlock in libraries using pthread_join
when loaded. Since EPICS libCom library relies on this 
method, any epics client mex-file will hang and freeze 
matlab. The problem can be circumvented by specifically
pre-loading the EPICS libCom library before the partial
glibc 2.18 preload. In this way, libCom is loaded with
glibc 2.17 (which does not have this glibc bug.)




To PyCafe users.

Dear Colleagues,
FYI, updated PyCafe extensions for python 3.5, 3.7, and 3.8 
have been provided using the latest epics7 client library. 
If it is of interest to stay "au courant", please add the 
following to your PYTHONPATH:

/opt/gfa/cafe/python/pycafe/cafe-1.15.0/lib/RHEL7-x86_64

The epics version can be retrieved with the following cafe method:
cafe.epics_version_string()

Please let me if you would like more information or help with anything.
Yours,
Jan

http://cafe.psi.ch/cython.html
