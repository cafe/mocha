function value = caget(channel,cached)
% caget('channel') returns current value and additional infos
% of given EPICS channel. 
% caget('channel',1) returns last value and additional infos
% of given EPICS channel. 
%
% channel can be a channel name or a handle as returned from mcaopen
%
% return value elements:
%   val:      value (scalar, vector or string)
%   sevr:     severity as a number
%   sevr_str: severity as a string
%   stat:     status as a number
%   stat_str: status as a string
%   time:     timestamp as a date vector
%   units:    units string

persistent severity_str
persistent status_str
persistent zerotime
persistent units

if ischar(channel)    
    pv = mocha('open',channel);
		
		if mocha('isConnected', channel) == false
			  mocha('openNowAndWait', 1.5)
		end		
		
    if mocha('isConnected', channel) == false
        error('EPICS channel %s not found', channel);
    end
elseif isnumeric(channel)
    pv = channel;
else
    error ('first argument must be channel name or handle');
end
if isvector(pv)
    pv=pv(1); % truncate
end

chInfo=mocha ('getChannelInfo',pv);

if nargin > 1 && cached		
    pvStruct = mocha('getPVCache',pv);
else
    pvStruct = mocha('getPV', pv);
end



pvCtrl = mocha('getCtrlCache',pv);
%pvCtrl

% initialize severity and status enums
if isempty(severity_str)
    severity_str = {'NO_ALARM';'MINOR';'MAJOR';'INVALID'};
    status_str = {'NO_ALARM';'READ';'WRITE';'HIHI';'HIGH';'LOLO';'LOW';'STATE';'COS';'COMM';'TIMEOUT';'HWLIMIT';'CALC';'SCAN';'LINK';'SOFT';'BAD_SUB';'UDF';'DISABLE';'SIMM';'READ_ACCESS';'WRITE_ACCESS'};
end


value.val = pvStruct.val;

units = pvCtrl.units;


if (pvCtrl.noEnumStrings>0) 
    value.val_str = value.val;
		value.val = mocha('getCache',pv,'int8'); 		
end


value.units=units;
value.sevr = pvStruct.alarmSeverity;
indx=max(pvStruct.alarmSeverity, 0);
value.sevr_str =  severity_str{indx+1};
value.stat = pvStruct.alarmStatus;
indx=max(pvStruct.alarmStatus, 0);
value.stat_str = status_str{ indx+1};



timestamp = mocha('getTimestamp',pv);
if (numel(timestamp) == 2)
% old mca version
% do not use mcatime here because 1. it is slow, 2. is returns UTC
% calculating zerotime only once is faster and takes localtime into account
% When daylight saving time begins or ends, restart the program!
    if isempty(zerotime)
        [status,timezone]=system('date +%z'); % get localtime offset
        timeoffs=str2double(timezone);
        houroffs=floor(timeoffs/100);
        minoffs=(timeoffs-100*houroffs);
        zerotime = datenum('1-Jan-1990') + (minoffs/60 + houroffs)/24;
    end
    time=timestamp(1)+timestamp(2)/1000000000;
    if time
        time = zerotime+time/24/3600;
    end
    value.time = datevec(time);
else
% new mca version
    timestamp(6)=timestamp(6)+timestamp(7)/1000000000;
    value.time = timestamp(1:6);
end



