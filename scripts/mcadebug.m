function mcadebug(onoff)
%MCADEBUG     - Enable/disable debugging
%
% Used only for development, not user-callable.
if onoff
    %mca(9999, 1);
		disp('Not relevant to mocha');
else
    %mca(9999);
		disp('Not relevant to mocha');
end

