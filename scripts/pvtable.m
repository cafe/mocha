function pvt = pvtable()
%
% pvt=pvtable()
% returns a table displaying all pvs and their status
%
[hpv,pvn,state] = mocha('getHandleStates');

pvt=[];
if (isempty(hpv)) 
	disp('THERE ARE NO CHANNELS!');
  return;
end	
	   
%If monitor then uses cache value
mocha('getAsyn', hpv);
mocha('sendNow'); %otherwise getPVCache will do the send!
dstruct =  mocha('getPVCache', hpv); 

%If NaN then following warning given from struct2table:
%Warning: Out of range or non-integer values truncated during
%conversion to character
%
t=struct2table(dstruct);

for n=1:length(hpv)
        s1(n).handle=hpv(n);
	s1(n).pv=pvn(n);
	s1(n).cs=state(n);
	s1(n).nmon=mocha('getNoMonitors', hpv(n));
end 

t1=struct2table(s1);

if (string(class(t.val)) == string('char'))
	t.val=string(t.val);
end

pvt=table(t1.handle,t1.pv, t.val, t.status, t1.cs, t1.nmon);
pvt.Properties.VariableNames={'h','pv', 'val', 'stat','c', 'm'};
%tNew.Properties.VariableNames{'Var1'}= 'handle';
%tNew.Properties.VariableNames{'Var2'}= 'pv';
%tNew.Properties.VariableNames{'Var3'}= 'val'; 


%2017a
%setColHeading(tNew, 1, 'handle');
%setColHeading(tNew, 2, 'pv');
%setColHeading(tNew, 3, 'val');
