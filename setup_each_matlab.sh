#!/bin/bash

# user prompt for setup step
while [[ ! $step =~ ^(0|1|2|3|4|5|6)$ ]]
do
	echo "Which setup step should be run?"
	echo "   0 - cleanup (make clean)"
	echo "   1 - local build (make)"
	echo "   2 - test local build (matlab -batch ...)"
	echo "   3 - install build to /opt/gfa/cafe/mocha/ (make install)"
	echo "   4 - deploy build to pmodules"
	echo "   5 - update pmodule info files"
	echo "   6 - test module (module load && matlab -batch ...)"
	read -p "choose step (0-6): " step
done

ca office network
module use Cafe
vrhel=${RHREL}
vcafe=1.21.0
vepics=7.0.8

for vmatlab in 2024a 2023b 2023a 2022b 2022a 2021b 2021a 2020b 2020a 2019b 2019a 2018b 2018a
do
	if [[ "$vmatlab" =~ ^(2018a|2018b|2019a|2019b|2020a|2020b|2021a|2021b)$ ]]; then
		vgcc=7.3.0
	elif [[ "$vmatlab" =~ ^(2022a|2022b|2023a|2023b|2024a)$ ]]; then
		vgcc=10.4.0
	fi
	printf "\n\n\n=======================================================================\n"
	printf "=======================================================================\n"
	echo "Versions"
	echo "  Matlab: $vmatlab"
	echo "  RHEL: $vrhel"
	echo "  gcc: $vgcc"
	echo "  Cafe: $vcafe"
	echo "  EPICS: $vepics"
	printf "=======================================================================\n"
	printf "=======================================================================\n"
	module unload cafe-matlab
	module unload matlab
	module load matlab/$vmatlab
	module unload gcc
    	module load gcc/$vgcc
	module list
	
	if [ $step -eq 0 ]; then
		echo "0 - cleanup (make clean)"
		make -f makefile_rel_1.21-gcc-$vgcc clean

	elif [ $step -eq 1 ]; then
		echo "1 - local build (make)"
		make -f makefile_rel_1.21-gcc-$vgcc

	elif [ $step -eq 2 ]; then
		echo "2 - test local build (matlab -batch ...)"
		if [[ "$vmatlab" =~ ^(2019a|2019b|2020a|2020b|2021a|2021b|2022a|2022b|2023a|2023b|2024a)$ ]]; then
			matlab -batch "cd RHEL8-x86_64/$vmatlab;disp(mocha('version'));example"
		else
			matlab -nodisplay -nosplash -nodesktop -r "cd RHEL8-x86_64/$vmatlab;disp(mocha('version'));example;exit"
		fi

	elif [ $step -eq 3 ]; then
		echo "3 - install build to /opt/gfa/cafe/mocha/ (make install)"
		make -f makefile_rel_1.21-gcc-$vgcc install

	elif [ $step -eq 4 ]; then
		echo "4 - deploy build to pmodules"
		new_dir="/opt/psi/Cafe/cafe-matlab/$vmatlab-$vrhel-gcc-$vgcc-cafe-$vcafe/libexec"
		src_dir="/opt/gfa/cafe/mocha/mocha-$vcafe-gcc-$vgcc/lib/$vrhel-x86_64/$vmatlab"

		echo "  new Dir: $new_dir"
		echo "  src Dir: $src_dir"

		mkdir -p $new_dir
		# cp mocha mex file
		cp $src_dir/* $new_dir
		# cp mca/ca scripts
		cp /afs/psi.ch/project/cafe/gitlab/CAFE/mocha/scripts/* $new_dir
		ls -la $new_dir
		rm -f /opt/psi/Cafe/cafe-matlab/$vmatlab
		ln -s $vmatlab-$vrhel-gcc-$vgcc-cafe-$vcafe /opt/psi/Cafe/cafe-matlab/$vmatlab
	elif [ $step -eq 5 ]; then
		echo "5 - update pmodule info files"
		cp modulefile_template $vmatlab
		sed -i -e "s/vmatlab/$vmatlab/g" $vmatlab
		sed -i -e "s/vrhel/$vrhel/g" $vmatlab
		sed -i -e "s/vepics/$vepics/g" $vmatlab
		sed -i -e "s/vgcc/$vgcc/g" $vmatlab
		sed -i -e "s/vcafe/$vcafe/g" $vmatlab

		cp $vmatlab /opt/psi/Cafe/modulefiles/cafe-matlab/$vmatlab
		rm $vmatlab

	elif [ $step -eq 6 ]; then
		echo "6 - test module (module load && matlab -batch ...)"
		
		echo $vmatlab	
		module help cafe-matlab/$vmatlab
		module load cafe-matlab/$vmatlab
		module list
		if [[ "$vmatlab" =~ ^(2019a|2019b|2020a|2020b|2021a|2021b|2022a|2022b|2023a|2023b|2024a)$ ]]; then
			matlab -batch "cd RHEL8-x86_64/$vmatlab;disp(mocha('version'));example"
		else
			matlab -nodisplay -nosplash -nodesktop -r "cd RHEL8-x86_64/$vmatlab;disp(mocha('version'));example;exit"
		fi
	fi
done
