mocha('openNoWait')

h1=mocha('open','ARIDI-BPM-01LE:X-AVG')
h2=mocha('open','ARIDI-BPM-01LB:X-AVG')
h3=mocha('open','ARIDI-BPM-01LE:Y-AVG')
h4=mocha('open','ARIDI-BPM-01LB:Y-AVG')
h5=mocha('open','ARIDI-BPM-01SE:X-AVG')
h6=mocha('open','ARIDI-BPM-01SE:Y-AVG')
h7=mocha('open','ARIDI-BPM-01SE:WF-INT-2')
h8=mocha('open','ARIDI-BPM-01SE:GET-ENABLE')
mocha('openNow')

hAll=[h1,h2,h3,h4,h5,h6,h7,h8];
kkl= 1.1;

clock

for mm=1:2

[s, allOK] = mocha('setScalarArray',[h5,h6],[kkl,kkl]); % all in one
s, allOK
[a]=mocha('getStructArray', [h1,h2,h7,h4]);
a
a(1).val
a(2).val
a(3).val
a(4).val

[a, allOK]=mocha('getScalarArray', [h1,h2,h7,h4]);
[r]=mocha('getCtrl',h5);
[r]=mocha('get',h1);
pause(0.05);
[r]=mocha('getCtrlCache',h5,'string');
end 
clock


for mm=1:10
[bret1]=mocha('getStructArrayCache', {'ARIDI-BPM-01LE:X-AVG','ARIDI-BPM-01LB:X-AVG','ARIDI-BPM-01SE:WF-INT-2','ARIDI-BPM-01LB:Y-AVG'});
[bret2]=mocha('getStructArrayCache',[h1,h2,h7,h4]);
pause(0.05);
end 
clock

%[ST,I]=dbstack('-completenames')
[ST,I]=dbstack()
ST.file
ST.line

pause(5)

for mm=1:10
[r]=mocha('get', [h1]);
[r]=mocha('get', [h2]);
[r]=mocha('get', [h7]);
[r]=mocha('get', [h4]);
r
end 
clock


[monID1,s]=mocha('monitor',h1);
[monID2,s]=mocha('monitor',h2);
[monID3,s]=mocha('monitor',h3);
[monID4,s]=mocha('monitor',h4);


s=mocha('defineGroup','gTest8',{'ARIDI-BPM-01LE:X-AVG','ARIDI-BPM-01LB:X-AVG','ARIDI-BPM-01LE:Y-AVG','ARIDI-BPM-01LB:Y-AVG','ARIDI-BPM-01SE:X-AVG','ARIDI-BPM-01SE:Y-AVG','ARIDI-BPM-01SE:WF-INT-2','ARIDI-BPM-01SE:GET-ENABLE'})
s=mocha('defineGroup','gTest', {'ARIDI-BPM-01LE:X-AVG','ARIDI-BPM-01LE:Y-AVG'})

g8=mocha('openGroup','gTest8');

%% pause to reduce growth in MATLAB memory
for mm =1:10
[r]=mocha('getStructArray', [h1,h2,h3,h4]); %byhandle
pause(0.05);
end

s=mocha('getAsyn',h5);
[r,s]=mocha('getCache',h5);
pause(0.1);

s=mocha('set','ARIDI-BPM-01LE:X-AVG',1.234); %bypvname
pause(0.1);
[r,s]=mocha('get', h1); %byhandle
pause(0.1);
s=mocha('set','ARIDI-BPM-01LE:X-AVG',1.234); %byhandle
pause(0.1);
[r,s]=mocha('getCache', h1); %byhandle
pause(0.1);
[r]=mocha('getStruct', h1); %byhandle
pause(0.1);
[r]=mocha('getStructArray', [h1,h2,h3,h4]); %byhandle
pause(0.1);

[r,s]=mocha('get', h1); %byhandle
pause(0.1);

%[r,s, allOK]=mocha('getScalarArray', [h1,h2,h3,h4]); %byhandle
%pause(0.1);


s=mocha('getAsyn',h5);
[r,s]=mocha('getCache',h5);


%why not combine the getNoWait and getNoWaitArray?
[s,allOK]=mocha('getAsyn',[h1,h2,h3]);
disp('s')
disp('allOK')
disp (s)
disp (allOK)

pause(10);
[r,allOK]=mocha('getStructArrayCache', [h1,h2,h3,h4]); %byhandle
r(1).val
r(2).val
r(3).val
r(4).val

%pause(0.1);
[r,s]=mocha('getCache',h5);
%pause(0.1);
s=mocha('set',h5,1.134);
pause(0.1);
s=mocha('set',h5,0.134);
pause(0.1);
s=mocha('getAsyn',h5);
[r,s]=mocha('getCache',h5);
pause(0.1);
%disp 'r should be 0.134'

for kkj =1:10
s=mocha('set',h5,mm);
[s, allOK] = mocha('setStructArray',[h5,h6],[1],[kkj]); %{'on'},[kk]);
[s, allOK] = mocha('setScalarArray',[h5,h6],[mm,mm]); % all in one
pause(0.01);
end


%tic;
%ts=tic;
for kk =1:10
mocha('isConnected',h1);
mocha('isConnected','ARIDI-BPM-01LE:X-AVG');
[s, allOK] = mocha('get',h1);
[r, allOK] = mocha('getScalarArray',[h1,h2,h3,h4,h5,h6,h7,h8]); %sequential gets
pause(0.01);
s
allOK
end
%te=toc(ts)

%tic;
%ts=tic;
for kk =1:10
[r, allOK] = mocha('getStructArray',[h1,h2,h3,h4,h5,h6,h7,h8]); %sequential gets
pause(0.01);
end
%te=toc(ts)

%tic;
%ts=tic
for kk =1:10
[r, allOK] = mocha('getGroup',g8); %sequential gets
pause(0.01);
end
%te=toc(ts)

%r(2).val
%pause(2.1);
%end;
%disp 'getStructArray h8,h7'
[r, allOK] = mocha('getStructArray',[h8,h7]);
%r.val
pause(0.01);
[s, allOK] = mocha('setScalarArray',[h5,h6],{'0.112','1.13'});
pause(0.01);



%disp(mm)
%disp 'getArrayNoWait h5,h6'
[s, allOK] = mocha('getArrayAsyn',[h5,h6]);
disp('getArrayAsyn')
pause(10);
%disp 'getScalarArrayCache h5,h6'
[r, s, allOK]=mocha('getScalarArrayCache',[h5,h6]);


pause(0.01);
%disp 'r should be 0.112'
s=mocha('set',h5,1.1345);
pause(0.01);
%disp 'getScalarArrayCache h5,h6'
[r, s, allOK]=mocha('getScalarArrayCache',[h5,h6]);

%disp 'r should be 0.112 not 1.1345'
%[s]=mocha('getAsyn',h1)
%[s]=mocha('getAsyn',h2)
%s=mocha('set',h1,int32(7))
%s=mocha('set',h1,single(8))
[a,s]=mocha('getCache',h1);
pause(0.01);
[a,s]=mocha('getCache',h2);
pause(0.01);

disp ('getArrayAsyn h5,h6')
[s, allOK]=mocha('getArrayAsyn',[h5,h6]);

pause(10);
s=mocha('set',h6,0.115);
pause(0.01);
[r,  allOK]=mocha('getStructArrayCache',[h5,h6]);

%disp 'r should be 0.1345 and 0.115'
%pause(0.1);
%disp 'getCtrl h5'
[r]=mocha('getCtrl',h5);
pause(0.01);
[r]=mocha('getCtrlCache',h5,'string');

pause(0.01);

[res, allOK] = mocha('getGroup', 'gTest');

[res, allOK] = mocha('getGroup', 'gTest');


mocha('loadXMLCollections','Collections/collection_swissfel.xml');
mocha('loadXMLGroups','Groups/group_swissfel.xml');


pause(0.01);
lgr=mocha('listgroups');

lgr

g1=mocha('openGroup','gWF');
pause(0.01);

pause(0.01);
[a,allOK]=mocha('getGroup', 'gWF'); %bygroupName
a
[b,allOK]=mocha('getGroup', g1);    %bygrouphandle
b
pause(0.01);
[a,allOK]=mocha('setGroup', g1, b.val); %bygroupName
pause(0.01);
l=mocha('listgroupmembers','gWF');

i=mocha('idxgrpmem', 'gWF', 'ARIDI-BPM-01LE:GET-ENABLE');
pause(0.01);
%end

r=mocha('monitorstop',h1, monID1); 
r=mocha('monitorstop'); % stops all monitors


%mocha('closeGroup','gTest');
